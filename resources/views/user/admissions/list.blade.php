@extends('user.layout')

@section('title', $admissions->title)
@section('description', $admissions->description)
@section('keywords', $admissions->keywords)

@section('content')
<div class="grid-box">
    <div class="title-section">
        <h1><span>{{$admissions->title}}</span></h1>
    </div>
    @if(!empty($admissionsDetail) && count($admissionsDetail))
        @foreach($admissionsDetail as $item)
        <div class="news-post article-post">
            <div class="row">
                <div class="col-sm-3">
                    <div class="post-gallery">
                        @if (!empty($item->img) && file_exists($imgPath."/admissions/thumb/".$item->img))
                            <img src="/public/images/admissions/thumb/{{$item->img}}"  alt="{{$item->caption}}" width="100%">
                        @else
                            <img src="/public/images/logo_thumb.jpg" style="width: auto !important; height: 150px">
                        @endif
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="post-content">
                        <h2><a href="/chieu-sinh/{{$item->id_admissions}}-{{$item->link}}.html">{{$item->title}}</a></h2>
                        <p>{{$item->short_desc}}</p>
                        <a href="/chieu-sinh/{{$item->id_admissions}}-{{$item->link}}.html" class="read-more-button"><i class="fa fa-arrow-circle-right"></i>Đọc thêm</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    @endif
</div>
 @if(!empty($admissionsDetail) && count($admissionsDetail))
<div class="pagination-box">
    <ul class="pagination-list">
        @if ($admissionsDetail->lastPage() == 1)
            <li><a class="active" href="#">1</a></li>
        @elseif ($admissionsDetail->lastPage() > 1)
            @if($admissionsDetail->currentPage() > 1)
                <li><a href="{{ $admissionsDetail->url($admissionsDetail->currentPage() - 1) }}">Back</a></li>
            @endif

             @for ($i = 1; $i <= $admissionsDetail->lastPage(); $i++)
                <li><a class="{{ ($admissionsDetail->currentPage() == $i) ? ' active' : '' }}" href="{{ $admissionsDetail->url($i) }}">{{$i}}</a></li>
            @endfor

            @if($admissionsDetail->currentPage() < $admissionsDetail->lastPage())
            <li><a href="{{ $admissionsDetail->url($admissionsDetail->currentPage()+1) }}">Next</a></li>
            @endif
        @endif
    </ul>

    <p>Page {{$admissionsDetail->currentPage()}} of {{$admissionsDetail->lastPage()}}</p>
</div>
@else
    <p>Hiện tại không tìm thấy dữ liệu</p>
@endif

@endsection