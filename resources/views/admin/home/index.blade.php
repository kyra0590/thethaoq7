@extends('admin.layout')
@section('title', $title)
@section('content')

<div class="row page-titles">
    <div class="col-md-12 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">

        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-success">
               <h4 class="m-b-0 text-white">{{$title}}</h4>
            </div>
            <div class="card-body">
                <form action="" method="post" enctype="multipart/form-data">
                    @csrf
                     <div class="form-body">
                        <div class="row p-t-20">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Tiêu đề</label>
                                    <input type="text" id="lastName" class="form-control" name="title" value="{{$home->title}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Mô tả ngắn</label>
                                    <textarea name="short_desc" class="form-control" rows="4" width="100%">{{$home->short_desc}}</textarea>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Meta Keywords</label>
                                    <textarea class="form-control form-control-danger" name="keywords" rows="5">{{$home->keywords}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Meta Descriptions</label>
                                    <textarea class="form-control form-control-danger" name="description" rows="5">{{$home->description}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Banner 1 (780x150)</label>
                                    <input type="file"  id="input-file-now" class="img dropify" name='img_1' data-default-file="{{ !empty($home->img_1) ? '/public/images/home/'.$home->img_1 : '' }}" data-allowed-file-extensions="jpeg jpg png gif"/>
                                </div>
                            </div>
                             <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Banner 2 (780x150)</label>
                                    <input type="file"  id="input-file-now" class="img dropify" name='img_2' data-default-file="{{ !empty($home->img_2) ? '/public/images/home/'.$home->img_2 : '' }}" data-allowed-file-extensions="jpeg jpg png gif"/>
                                </div>
                            </div>
                        </div>
                           <div class="row">
                             <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Banner 3 (780x150)</label>
                                    <input type="file"  id="input-file-now" class="img dropify" name='img_3' data-default-file="{{ !empty($home->img_3) ? '/public/images/home/'.$home->img_3 : '' }}" data-allowed-file-extensions="jpeg jpg png gif"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Lưu</button>
                    </div>
                    <input type="hidden" id='remove_img_1' name='remove_img_1' value='0'>
                    <input type="hidden" id='remove_img_2' name='remove_img_2' value='0'>
                    <input type="hidden" id='remove_img_3' name='remove_img_3' value='0'>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
<?php if($status) { ?>
 $.toast({
    heading: "Thông báo",
    text: "Cập nhật thành công",
    position: 'bottom-right',
    loaderBg:'#ff6849',
    icon: 'success',
    hideAfter: 3500,
    stack: 6
});
<?php } ?>
</script>
@endsection
