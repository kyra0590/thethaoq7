<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use App\Models\Intro;
use App\Models\IntroDetail;
use Illuminate\Support\Facades\DB;
use Image;

class IntroController extends BaseController
{
    public function index(Request $request){
        $params = $request->all();
        $isAjax = (isset($params['is_ajax'])) ? $params['is_ajax'] : false;
        $start = (!empty($params['start'])) ? $params['start'] : 0;
        $keyword = (!empty($params['keyword'])) ? $params['keyword'] : '';
        $limit =  (!empty($params['limit'])) ? $params['limit'] : 50;
        $intro = Intro::where('title', 'like', "%{$keyword}%")
            ->orderBy('order', 'asc')
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->offset($start)
            ->get();
        if($isAjax) {
            return response()->json(
                 [
                "recordsFiltered" => count($intro),
                "recordsTotal" => count($intro),
                "data" => $intro
            ], 200 );
        }
        return view('admin/intro/index')
            ->with('title', "Danh sách giới thiệu");
    }

    public function add(Request $request){
        $params = $request->all();
        $title = "Thêm giới thiệu";
        $status = false;
        if(!empty($params)) {
            $params['title'] = trim($params['title']);
            $params['created_at'] = date("Y-m-d");
            $params['order'] = !empty($params['order']) ? (int) $params['order'] : 0;
            unset($params['_token']);
            $status = Intro::insert($params);
        }
        return view('admin/intro/add')
            ->with('title', $title)
            ->with('status', $status);
    }

     public function edit(Request $request, $id){
        $title = 'Sửa giới thiệu';
        $intro = Intro::find($id);
        $status = false;
        if(!$id || empty($intro)) {
           return redirect('/admin/intro');
        }
        $params = $request->all();
        if(!empty($params)) {
            $intro->title        = trim($params['title']);
            $intro->is_showed    = $params['is_showed'];
            $intro->link         = $params['link'];
            $intro->keywords     = $params['keywords'];
            $intro->description  = $params['description'];
            $intro->order        = !empty($params['order']) ? (int) $params['order'] : 0;
            $intro->is_show_navi = !empty($params['is_show_navi']) ? (int) $params['is_show_navi'] : 0;
            $status              = $intro->save();
        }
        return view('admin/intro/edit')
            ->with('title', $title)
            ->with('intro', $intro)
            ->with('status', $status);
    }

    public function del($id){
         if($id){
            Intro::where('id', $id)->delete();
            IntroDetail::where('id_intro', $id)->update(['id_intro' => 0, 'is_showed' => 0]);
        }
        return redirect("/admin/intro");
    }

    public function detail(Request $request){
        $title   = "Danh sách tin tức giới thiệu";
        $params  = $request->all();
        $idIntro = (!empty($params['idIntro'])) ? trim($params['idIntro']) : '';
        $isAjax  = (isset($params['is_ajax'])) ? $params['is_ajax'] : false;
        $start   = (!empty($params['start'])) ? $params['start'] : 0;
        $keyword = (!empty($params['keyword'])) ? $params['keyword'] : '';
        $limit   =  (!empty($params['limit'])) ? $params['limit'] : 50;
        $introDetail = DB::table('intro_detail')
            ->selectRaw("intro_detail.id, intro_detail.title,
                intro.title as intro_title,
                intro_detail.short_desc, intro_detail.content,
                intro_detail.keywords, intro_detail.is_showed,
                intro_detail.date_show, intro_detail.link,
                intro_detail.order, intro_detail.img")
            ->join('intro', 'intro.id', '=', 'intro_detail.id_intro')
            ->where('intro_detail.title', 'like', "%{$keyword}%")
            ->orderBy('intro_detail.order', 'asc')
            ->orderBy('intro_detail.id', 'desc')
            ->limit($limit)
            ->offset($start);
        if($idIntro != "") {
            $introDetail->where('intro_detail.id_intro', $idIntro);
            $intro = Intro::find($idIntro);
            if(!empty($intro)) {
                $intro = $intro->first();
                $title .= " - ". $intro->title;
            }
        }
        $introDetail = $introDetail->get();
        if($isAjax) {
            return response()->json(
            [
                "recordsFiltered" => count($introDetail),
                "recordsTotal"    => count($introDetail),
                "data"            => $introDetail
            ], 200 );
        }
        return view('admin/intro/detail')
            ->with("introDetail", $introDetail)
            ->with('idIntro', $idIntro)
            ->with('title', $title);
    }

     public function addDetail(Request $request){
        $title   = "Thêm tin giới thiệu";
        $params  = $request->all();
        $status  = false;
        $idIntro = '';
        $listIntro = Intro::orderBy('order', 'ASC')->orderBy("id","DESC")->get()->keyBy("id")->toArray();
        if(!empty($params)) {
            $idIntro = (!empty($params['idIntro'])) ? trim($params['idIntro']) : '';
            if($idIntro != '') {
                $intro = Intro::find($idIntro)->first();
                $title .= " - ". $intro->title;
            }
            if(!empty($params['_token'])) {
                $params['title'] = trim($params['title']);
                $params['caption'] = trim($params['caption']);
                $params['created_at'] = date("Y-m-d");
                $params['order'] = (!empty($params['order'])) ? (int) $params['order'] : 0;
                $params['date_show'] = (!empty($params['date_show'])) ? $params['date_show'] : date("Y-m-d");
                unset($params['_token']);
                unset($params['idIntro']);
                if($request->hasFile('img')) {
                    $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/intro/";
                    $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                    if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                        $params['img'] = $fileName;

                        $imgThumb = Image::make($descriptionPath.$fileName);
                        $imgThumb->resize(375, 250, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($descriptionPath.'thumb/'.$fileName);
                    }
                }
                $status = IntroDetail::insert($params);
            }
        }
        return view('admin/intro/add-detail')
            ->with('title', $title)
            ->with('idIntro', $idIntro)
            ->with('listIntro', $listIntro)
            ->with('status', $status);
    }

    public function editDetail(Request $request, $id) {
        $title       = 'Sửa tin giới thiệu';
        $params      = $request->all();
        $status      = false;
        $idIntro     = '';
        $listIntro   = Intro::orderBy('order', 'ASC')->orderBy("id","DESC")->get()->keyBy("id")->toArray();
        $introDetail = IntroDetail::find($id);
        if(!$id || empty($introDetail)) {
            return redirect("/admin/intro-detail?idIntro={$introDetail->id_intro}");
        }
        if(!empty($params)) {
            $idIntro = (!empty($params['idIntro'])) ? trim($params['idIntro']) : '';
            if($idIntro != '') {
                $intro = Intro::find($idIntro)->first();
                $title .= " - ". $intro->title;
            }
            if(!empty($params['_token'])) {
                $introDetail->title       = trim($params['title']);
                $introDetail->caption     = trim($params['caption']);
                $introDetail->id_intro    = $params['id_intro'];
                $introDetail->is_showed   = $params['is_showed'];
                $introDetail->link        = $params['link'];
                $introDetail->keywords    = $params['keywords'];
                $introDetail->description = $params['description'];
                $introDetail->short_desc  = $params['short_desc'];
                $introDetail->date_show   = $params['date_show'];
                $introDetail->content     = $params['content'];
                $introDetail->order       = !empty($params['order']) ? (int) $params['order'] : 0;
                $descriptionPath    = $_SERVER['DOCUMENT_ROOT']."/public/images/intro/";
                if($request->hasFile('img')) {
                    $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                    if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                        if(!empty($introDetail->img)) {
                            if(file_exists($descriptionPath.$introDetail->img)) {
                                unlink($descriptionPath.$introDetail->img);
                            }

                            if(file_exists($descriptionPath.'thumb/'.$introDetail->img))
                            {
                                unlink($descriptionPath.'thumb/'.$introDetail->img);
                            }
                        }

                        $imgThumb = Image::make($descriptionPath.$fileName);
                        $imgThumb->resize(375, 250, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($descriptionPath.'thumb/'.$fileName);

                        $introDetail->img = $fileName;
                    }
                } else {
                    if( $params['remove_img'] && file_exists($descriptionPath.$introDetail->img)) {
                        unlink($descriptionPath.$introDetail->img);
                        if(file_exists($descriptionPath.'thumb/'.$introDetail->img))
                        {
                            unlink($descriptionPath.'thumb/'.$introDetail->img);
                        }
                        $introDetail->img = "";
                    }
                }
                $status = $introDetail->save();
            }
        }
        return view('admin/intro/edit-detail')
            ->with('title', $title)
            ->with('introDetail', $introDetail)
            ->with('idIntro', $idIntro)
            ->with('listIntro', $listIntro)
            ->with('status', $status);
    }

    public function delDetail(Request $request, $id){
        $params = $request->all();
        $idIntro = (!empty($params['idIntro'])) ? trim($params['idIntro']) : '';
        if($id){
            $introDetail = IntroDetail::find($id);
            $descriptionPath    = $_SERVER['DOCUMENT_ROOT']."/public/images/intro/";
            if(!empty($introDetail->img) && file_exists($descriptionPath.$introDetail->img)) {
                unlink($descriptionPath.$introDetail->img);
                if(file_exists($descriptionPath.'thumb/'.$introDetail->img))
                {
                    unlink($descriptionPath.'/thumb/'.$introDetail->img);
                }
            }
            $introDetail->delete();
        }
        return redirect("/admin/intro-detail?id_intro={$idIntro}");
    }
}
