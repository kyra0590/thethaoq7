@extends('user.layout')

@section('title', $setting->title)
@section('description', $setting->description)
@section('keywords', $setting->keywords)

@section('content')

<div class="contact-info-box">
    <div class="title-section">
        <h1><span>Liên hệ</span></h1>
    </div>
    <div id="map"></div>
    <p>
        <i class="fa fa-map-marker"></i> &nbsp; Địa chỉ: 504-506 Huỳnh Tấn Phát, Bình Thuận, Quận 7, Hồ Chí Minh, Vietnam <br>
        <i class="fa fa-mobile"></i> &nbsp; Điện thoại: +84 28 3781 0381 <br>
        <i class="fa fa-clock-o"></i> &nbsp; Mở cửa: 6AM–8PM
    </p>
</div>

<div class="contact-form-box">
    <div class="title-section">
        <h1><span>Gửi thông tin đến chúng tôi</span></h1>
    </div>
    <form id="contact-form" method="post">
        @csrf
        <div class="row">
            <div class="col-md-4">
                <label for="name">Họ tên*</label>
                <input id="name" name="fullname" type="text" value="{{ !empty($params['fullname']) ? $params['fullname'] : '' }}">
            </div>
            <div class="col-md-4">
                <label for="mail">E-mail*</label>
                <input id="mail" name="email" type="text" value="{{ !empty($params['email']) ? $params['email'] : '' }}">
            </div>
            <div class="col-md-4">
                <label for="website">Số Điện Thoại</label>
                <input id="phone" name="phone" type="text" value="{{ !empty($params['phone']) ? $params['phone'] : '' }}">
            </div>
            <div class="col-md-12">
                <label for="comment">Yêu cầu của bạn*</label>
                <textarea id="comment" name="content">{{ !empty($params['content']) ? $params['content'] : '' }}</textarea>
            </div>
            <div class="col-md-3">
              <span id="captcha">{!! captcha_img() !!}</span>
            </div>
            <div class="col-md-1">
              <button type="button" class="btn btn-success btn-refresh"><i class="fa fa-refresh"></i></button>
            </div>
            <div class="col-md-3">
              <input type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
            </div>
            <div class="col-md-5 text-right">
                <button type="submit" id="submit-contact">Gửi thông tin </button>
            </div>
        </div>
        @if(!empty($errors) && count($errors))
        <div class="row">
            <div class="col-md-12">
                <p class="text-warning">Vui lòng kiểm tra lại:</p>
                <ul>
                @foreach ($errors as $error)
                    <li class="text-danger">{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        </div>
        @endif

    </form>
</div>

@endsection

@section("js")
<script>
$(".btn-refresh").click(function(){
  $.ajax({
     type:'GET',
     url:'/refresh_captcha',
     success:function(data){
        $("#captcha").html(data.captcha);
     },
      error: function(data) {
        alert('Try Again.');
      }
  });

});
</script>
@endsection