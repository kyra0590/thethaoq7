<!doctype html>
<html lang="en" class="no-js">

<head>
    <title>Thể Thao Quận 7</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <?php include('css.php') ?>
</head>

<body>
    <!-- Container -->
    <div id="container">
        <!-- Header
            ================================================== -->
        <?php include('header.php') ?>
        <!-- End Header -->
        <!-- block-wrapper-section
            ================================================== -->
         <section class="block-wrapper">
            <div class="container shadow-white">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="block-content">
                            <!-- grid box -->
                            <div class="grid-box">
                                <div class="title-section">
                                    <h1><span>Akido</span></h1>
                                </div>
                                <div class="news-post article-post">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="post-gallery">
                                                <img alt="" src="https://baoquocte.vn/stores/news_dataimages/huynguyen/122019/10/23/in_article/truyen-thong-chau-a-khen-ngoi-chien-thang-lich-su-cua-bong-da-viet-nam.jpg">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="post-content">
                                                <h2><a href="chi-tiet-tin.php">Truyền thông châu Á khen ngợi chiến thắng lịch sử của bóng đá Việt Nam</a></h2>
                                                <p>TGVN. Sau khi thầy trò HLV Park Hang-seo giành chiến thắng thuyết phục 3-0 trong trận chung kết trước U22 Indonesia để đưa bóng đá Việt Nam lần đầu lên bục vinh quang SEA Games, báo chí châu Á đã dành không ít lời khen ngợi cho màn trình diễn này.</p>
                                                <a href="chi-tiet-tin.php" class="read-more-button"><i class="fa fa-arrow-circle-right"></i>Đọc thêm</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="news-post article-post">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="post-gallery">
                                                <img alt="" src="https://baoquocte.vn/stores/news_dataimages/nguyennga/122019/13/16/croped/1837_cong-phuong-st-1-1576216710959.jpg">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="post-content">
                                                <h2><a href="chi-tiet-tin.php">Công Phượng miệt mài tập luyện chờ ngày được ra sân ở Sint Truidense</a></h2>
                                                <p>TGVN. Sau khi tham dự vòng loại World Cup 2022 cùng đội tuyển Việt Nam hồi đầu tháng 11, tiền đạo Công Phượng đã trở về CLB chủ quản Sint Truidense tập luyện cùng đồng đội. Tuy nhiên từ đó đến nay anh vẫn chưa được ra sân chính thức, bất chấp CLB này thay đổi HLV.</p>
                                                <a href="chi-tiet-tin.php" class="read-more-button"><i class="fa fa-arrow-circle-right"></i>Đọc thêm</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="news-post article-post">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="post-gallery">
                                                <img alt="" src="https://baoquocte.vn/stores/news_dataimages/nguyennga/122019/13/14/1109_doanvanhaunongtrenbaohalanheerenveenvocungtuhao-1576212373231.jpg">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="post-content">
                                                <h2><a href="chi-tiet-tin.php">Văn Hậu được 'thưởng' sau khi trở về Heerenveen</a></h2>
                                                <p>TGVN. Văn Hậu đã nhận được món quà tinh thần khi CLB Heerenveen xác nhận sẽ tổ chức vinh danh tuyển thủ Việt Nam trước trận đấu với Willem II.</p>
                                                <a href="chi-tiet-tin.php" class="read-more-button"><i class="fa fa-arrow-circle-right"></i>Đọc thêm</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pagination-box">
                                <ul class="pagination-list">
                                    <li><a class="active" href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><span>...</span></li>
                                    <li><a href="#">9</a></li>
                                    <li><a href="#">Next</a></li>
                                </ul>
                                <p>Page 1 of 9</p>
                            </div>
                            </div>
                    </div>
                    <div class="col-sm-3">
                    <!-- sidebar -->
                    <?php include('sidebar.php') ?>
                    <!-- End sidebar -->
                    </div>
                </div>
            </div>
        </section>
    <!-- End block-wrapper-section -->
    <!-- footer
            ================================================== -->
    <?php include('footer.php') ?>
    <!-- End footer -->
    </div>
    <!-- End Container -->
    <?php include('script.php') ?>
</body>

</html>