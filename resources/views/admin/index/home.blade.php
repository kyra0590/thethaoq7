{{-- Load Layout User --}}
@extends('admin.layout')

{{-- set title --}}
@section('title', __('admin.edit_home'))

@section('css')
<link href="/public/assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="/public/dist/css/pages/other-pages.css" rel="stylesheet">
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">{{__('common.home')}}</h4>
    </div>
</div>

<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<!-- Row -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-success">
                <h4 class="m-b-0 text-white">{{__('admin.edit_home')}}</h4>
            </div>
            <div class="card-body">
                <form action="" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                        <div class="row">
                           <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{__('common.title')}}</label>
                                    <input type="text" id="lastName" class="form-control" placeholder="" name="title" required value="{{$home->title}}">
                                </div>
                            </div>
                        </div>
                          <div class="row">
                           <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{__('common.keywords')}}</label>
                                    <textarea class="form-control form-control-danger" name="keywords" rows="5">{{$home->keywords}}</textarea>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{__('common.description')}}</label>
                                    <textarea class="form-control form-control-danger" name="description" rows="5">{{$home->description}}</textarea>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        {{-- <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">{{__('common.content')}}</label>
                                   <textarea id="editor" name="content">{!! $home->content !!}</textarea>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{__('common.save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Row -->
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
@endsection

@section('js')
<script src="/public/assets/node_modules/moment/moment.js"></script>
<script src="/public/assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<script src="/public/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

<script src="/public/js/ckeditor/ckeditor.js"></script>
<script src="/public/js/ckeditor/ckfinder/ckfinder.js"></script>

<script src="/public/assets/node_modules/toast-master/js/jquery.toast.js"></script>
<script src="/public/assets/node_modules/dropify/dist/js/dropify.min.js"></script>

<script>
$('.dropify').dropify();
$('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false, format: 'YYYY-MM-DD'});
var editor = CKEDITOR.replace( 'editor' , {height: 400} );
CKFinder.setupCKEditor( editor );
CKEDITOR.stylesSet.add('default', [
    {name: 'Table', element: 'table', attributes: {'class': 'table table-border'}},
    {name: 'Quote', element: 'blockquote', attributes: {'class': 'quote'}},
    {name: 'Square Button', element: 'a', attributes: {'class' : 'btn btn-sm', }},
    {name: 'Circle Button', element: 'a', attributes: {'class' : 'btn btn-circle btn-sm', }},
    {name: 'Popup', element: 'a', attributes: {'class' : 'lightbox', 'data-option' : 'mainClass:inner','data-lightbox-anima': 'fade-in'}},
    ]);

<?php if($status) { ?>
 $.toast({
    heading: "{{__('common.notification')}}",
    text: "{{__('admin.edit_successfull')}}",
    position: 'top-right',
    loaderBg:'#ff6849',
    icon: 'success',
    hideAfter: 3500,
    stack: 6
});
<?php } ?>
</script>
@endsection
