@extends('admin.layout')
@section('title', $title)
@section('content')

<div class="row page-titles">
    <div class="col-md-12 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <button type="button" class="btn btn-info d-none d-lg-block m-l-15" onclick="javascript:location.href='/admin/intro'"><i class="fa fa-undo"></i> Trở lại</button>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-success">
               <h4 class="m-b-0 text-white">{{$title}}</h4>
            </div>
            <div class="card-body">
                <form action="" method="post" enctype="multipart/form-data">
                    @csrf
                     <div class="form-body">
                        <div class="row p-t-20">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Hiển thị</label>
                                    <?php $show = ["Ẩn", "Hiện"]; ?>
                                    <select class="form-control custom-select" name="is_showed">
                                        <?php foreach ($show as $key => $value) {
                                            $selected = ($intro->is_showed == $key) ? 'selected' : '';
                                        ?>
                                            <option value="{{$key}}" {{$selected}}>{{$value}}</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"> <input type="checkbox" name="is_show_navi" <?= ($intro->is_show_navi) ? 'checked' : '' ?> value="1"> Hiển thị trên điều hướng</label>
                                </div>
                            </div>
                        </div>
                        <!--/row-->
                        <div class="row">
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Tiêu đề</label>
                                    <input type="text" id="lastName" class="form-control" name="title" value="{{$intro->title}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Đường dẫn</label>
                                    <input type="text" id="link" class="form-control" name="link" value="{{$intro->link}}">
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                         <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Meta Keywords</label>
                                    <textarea class="form-control form-control-danger" name="keywords" rows="5">{{$intro->keywords}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Meta Descriptions</label>
                                    <textarea class="form-control form-control-danger" name="description" rows="5">{{$intro->description}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Lưu</button>
                        <button type="button" class="btn btn-danger" onclick="javascript:location.href='/admin/intro'"> <i class="fa fa-ban"></i> Hủy bỏ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
<?php if($status) { ?>
 $.toast({
    heading: "Thông báo",
    text: "Cập nhật thành công",
    position: 'bottom-right',
    loaderBg:'#ff6849',
    icon: 'success',
    hideAfter: 3500,
    stack: 6
});
<?php } ?>
</script>
@endsection
