@extends('user.layout')

@section('title', $info->title)
@section('description', $info->description)
@section('keywords', $info->keywords)

@section('content')
<div class="grid-box">
    <div class="single-post-box">
        <div class="title-post">
            <h1>{{$infoDetail->title}}</h1>
             <ul class="post-tags">
                <li><i class="fa fa-clock-o"></i>{{date("d M Y", strtotime($infoDetail->date_show))}}</li>
            </ul>
        </div>
        <div class="post-gallery">
            @if (!empty($infoDetail->img) && file_exists($imgPath."/info/".$infoDetail->img))
            <img src="/public/images/info/{{$infoDetail->img}}" alt="{{$infoDetail->caption}}">
            <span class="image-caption">{{$infoDetail->caption}}</span>
            @endif
        </div>
        <div class="post-content">
            {!! $infoDetail->content  !!}
        </div>
        @if(!empty($relationInfo) && count($relationInfo))
        <div class="col-md-12">
            <div class="title-section">
                <h1><span class="world">TIN LIÊN QUAN</span></h1>
            </div>
            <ul class="list-posts">
                @foreach($relationInfo as $item)
                <li style="border:none">
                    <div class="post-content">
                        <h2><a href="/thong-tin/{{$item->id_info}}-{{$item->link}}.html">»» {{$item->title}}</a></h2>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>
@endsection