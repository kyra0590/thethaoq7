@extends('admin.layout')
@section('title', $title)
@section('content')

<div class="row page-titles">
    <div class="col-md-12 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <button id="btn_update_order" type="button" class="btn btn-warning d-none d-lg-block m-l-15"><i class="fa fa-edit"></i> Cập nhật vị trí</button>
            <button type="button" class="btn btn-info d-none d-lg-block m-l-15" onclick="location.href ='/admin/partner/add'"><i
                    class="fa fa-plus-circle"></i> Thêm</button>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-success">
                <h4 class="m-b-0 text-white">{{$title}}</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive m-t-20">
                    <form action="" id="frm_data">
                        <input type="hidden" name='table_name' value="partner">
                        <table id="dataTable"
                            class="display nowrap table table-hover table-striped table-bordered"
                            cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Hiển thị</th>
                                    <th>Hình ảnh</th>
                                    <th>Tiêu đề</th>
                                    <th>Hiển thị thanh điều hướng</th>
                                    <th>Vị trí</th>
                                    <th>Xử lý</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script>
$('#dataTable').DataTable({
        "bSort" : false,
        // "order": [[ 0, "asc" ]],
        "serverSide" : true,
        "paging": true,
        "pageLength": 50,
        "ajax" : {
            url : "/admin/partner?is_ajax=true",
            type: 'POST',
            data : function(d) {
               return $.extend( {}, d, {
                    "nums" : d.start,
                    "keyword" : d.search['value'],
                    "limit" : d.length,
                });
            },
        },
        columns:[
            {
                "width" : "50px",
                "data" : 'id',
                "name"  : "id"
            },
            {
                "width" : "100px",
                "data" : 'is_showed',
                "name"  : "is_showed" ,
                 render: function ( data, type, row ) {
                    if(data) {
                        return "<p style='text-align:center'><i class='fa fa-eye'></i></p>";
                    }
                    return "<p style='text-align:center'><i class='fa fa-eye-slash'></i></p>";
                }
            },
            {
                "width" : "100px",
                "data" : 'img',
                "name"  : "img" ,
                render: function ( data, type, row ) {
                    if(data && data != "") {
                        return "<p style='text-align:center'><img style='width:50px; height:50px' src='/public/images/partner/"+data+"'/></p>";
                    }
                    return data;
                }
            },
            { "data" : 'title' , "name" : "title",
                render: function ( data, type, row ) {
                    return data;
                }
            },
            { "data" : 'is_show_navi' , "name" : "is_show_navi", "width": "100px",
                render: function ( data, type, row ) {
                    if(data) {
                        return "<p style='text-align:center'><i class='fas fa-star'></i></p>";
                    }
                    return "<p style='text-align:center'><i class='far fa-star'></i></p>";
                }
            },
            {
                "width" : "100px",
                "data" : 'order',
                "name" : "order",
                render: function ( data, type, row ) {
                    return "<input type='text' class='form-control' name='order["+row.id+"]' value='"+data+"' style='width:50px'/>";
                }
            },
            {   "width" : "100px",
                "aTargets": [4],
                "mData": null,
                render: function (data, type, full) {
                    return '<a href="/admin/partner/edit/'+data.id+'"><i class="far fa-edit"></i></a> | '+
                        '<a href="/admin/partner/del/'+ data.id +'" class="btnDelete"><i class="far fa-trash-alt"></i></a>';
                }
            }
        ]
    });

$("#btn_update_order").bind('click', function(event) {
    event.preventDefault();
    $.ajax({
        url: '/admin/update-order',
        type: 'GET',
        dataType: 'json',
        data: $("#frm_data").serialize(),
    })
    .done(function(data) {
        if(data.status) {
            $.toast({
                heading: "Thông báo",
                text: "Cập nhật vị trí thành công",
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500,
                stack: 6
            });
            location.reload();
        }
        else {
            $.toast({
                heading: "Thông báo",
                text: "Cập nhật vị trí thất bại",
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'warning',
                hideAfter: 3500,
                stack: 6
            });
        }
    });
});
</script>
@endsection
