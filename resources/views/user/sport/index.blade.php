@extends('user.layout')

@section('title', $setting->title)
@section('description', $setting->description)
@section('keywords', $setting->keywords)

@section('content')
<div class="grid-box">
    <div class="title-section">
        <h1><span>Các bộ môn</span></h1>
    </div>
    @if(!empty($sport) && count($sport))
        @foreach($sport as $item)
        <div class="news-post article-post">
            <div class="row">
                <div class="col-sm-3">
                    <div class="post-gallery">
                        @if (!empty($item->img) && file_exists($imgPath."/sport-detail/thumb/".$item->img))
                            <img src="/public/images/sport-detail/thumb/{{$item->img}}"  alt="{{$item->caption}}" width="100%">
                        @else 
                            <img src="/public/images/logo_thumb.jpg" style="width: auto !important; height: 150px">
                        @endif
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="post-content">
                        <h2><a href="/bo-mon/{{$item->id_sport}}-{{$item->link}}.html">{{$item->title}}</a></h2>
                        <p>{{$item->short_desc}}</p>
                        <a href="/bo-mon/{{$item->id_sport}}-{{$item->link}}.html" class="read-more-button"><i class="fa fa-arrow-circle-right"></i>Đọc thêm</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    @endif
</div>
@if(!empty($sport) && count($sport))
<div class="pagination-box">
    <ul class="pagination-list">
        @if ($sport->lastPage() == 1)
            <li><a class="active" href="#">1</a></li>
        @elseif ($sport->lastPage() > 1)
            @if($sport->currentPage() > 1)
                <li><a href="{{ $sport->url($sport->currentPage() - 1) }}">Back</a></li>
            @endif

             @for ($i = 1; $i <= $sport->lastPage(); $i++)
                <li><a class="{{ ($sport->currentPage() == $i) ? ' active' : '' }}" href="{{ $sport->url($i) }}">{{$i}}</a></li>
            @endfor

            @if($sport->currentPage() < $sport->lastPage())
            <li><a href="{{ $sport->url($sport->currentPage()+1) }}">Next</a></li>
            @endif
        @endif
    </ul>

    <p>Page {{$sport->currentPage()}} of {{$sport->lastPage()}}</p>
</div>
@else
    <p>Hiện tại không tìm thấy dữ liệu</p>
@endif

@endsection