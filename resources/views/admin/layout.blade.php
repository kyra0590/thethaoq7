<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Thể Thao Quận 7 - Admin - @yield('title')</title>
    <link href="/public/assets/node_modules/morrisjs/morris.css" rel="stylesheet">
    <link href="/public/dist/css/pages/dashboard3.css" rel="stylesheet">
    <meta property="og:image" content="/public/images/logo_og.jpg" />
    <link type="text/css" href="/public/assets/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css">
    <link type="text/css" href="/public/assets/node_modules/datatables.net-bs4/css/responsive.dataTables.min.css">
    <link href="/public/assets/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet">
    <link href="/public/assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
    <link href="/public/dist/css/pages/other-pages.css" rel="stylesheet">
    <link rel="stylesheet" href="/public/assets/node_modules/dropify/dist/css/dropify.min.css">
    <link href="/public/assets/node_modules/dropzone-master/dist/dropzone.css" rel="stylesheet">
    <link href="/public/assets/node_modules/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
    <link href="/public/dist/css/pages/user-card.css" rel="stylesheet">
    <link href="/public/dist/css/style.css" rel="stylesheet">
    @yield('css')
</head>

<body class="skin-default fixed-layout">
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Thể Thao Quận 7</p>
        </div>
    </div>
    <div id="main-wrapper">
        @include("admin.header");

        @include("admin.left_sidebar");
        <div class="page-wrapper">
            <div class="container-fluid">
               @yield('content')
            </div>
        </div>
        <footer class="footer">
            © 2020 Thể Thao Quận 7
        </footer>
    </div>
    <script src="/public/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
    <script src="/public/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/public/dist/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="/public/dist/js/waves.js"></script>
    <script src="/public/dist/js/sidebarmenu.js"></script>
    <script src="/public/dist/js/custom.js"></script>
    <script src="/public/assets/node_modules/moment/moment.js"></script>
    <script src="/public/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="/public/assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <script src="/public/assets/node_modules/dropify/dist/js/dropify.min.js"></script>
    <script src="/public/js/ckeditor/ckeditor.js"></script>
    <script src="/public/js/ckeditor/ckfinder/ckfinder.js"></script>
    <script src="/public/assets/node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/public/assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js"></script>
    <script src="/public/assets/node_modules/toast-master/js/jquery.toast.js"></script>
    <script src="/public/assets/node_modules/dropzone-master/dist/dropzone.js"></script>
    <script src="/public/assets/node_modules/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
    <script>
        if($('#editor').length > 0){
            var editor = CKEDITOR.replace( 'editor', {height: 400, allowedContent: true} );
            CKFinder.setupCKEditor( editor );
            CKEDITOR.stylesSet.add('default', [
                {name: 'table type 1', element: 'table', attributes: {'class': 'table'}},
                {name: 'table type 2', element: 'table', attributes:{'class': 'table table-striped'}},
                {name: 'table type 3', element: 'table', attributes:{'class': ' table-borderless'}},
                {name: 'post-content', element: 'div', attributes:{'class': 'post-content'}},
                {name: 'article-inpost', element: 'div', attributes:{'class': 'article-inpost'}},
                {name: 'post-gallery', element: 'div', attributes: {'class' : 'post-gallery'}},
                {name: 'image-content image-place', element: 'div', attributes: {'class' : 'image-content image-place'}},
                {name: 'image-caption', element: 'span', attributes:{'class': 'image-caption'}},
            ]);
        }

        if($("#mdate").length > 0) {
            $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false, format: 'YYYY-MM-DD'});
        }

        if($('.dropify').length > 0) {
            var drEvent = $('.dropify').dropify();
            drEvent.on('dropify.afterClear', function(event, element){
                if($(this).hasClass('img')) {
                    if($(this).attr('name') == "img"){
                        $("#remove_img").val(1);
                    }


                    if($(this).attr('name') == "img_1"){
                        $("#remove_img_1").val(1);
                    }

                    if($(this).attr('name') == "img_2"){
                        $("#remove_img_2").val(1);
                    }

                    if($(this).attr('name') == "img_3"){
                        $("#remove_img_3").val(1);
                    }
                }
                if($(this).hasClass('video')){
                    $("#remove_video").val(1);
                }
            });
        }

        $(document).on('click', '.btnDelete', function(event) {
            event.preventDefault();
            var cf = confirm('Bạn muốn xóa dòng này?');
            if(cf === true) {
                location.href = $(this).attr('href');
            }
        });

    </script>
    @yield('js')
</body>

</html>