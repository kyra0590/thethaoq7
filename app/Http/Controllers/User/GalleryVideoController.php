<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\User\BaseController;
use Illuminate\Http\Request;
use App\Models\GalleryVideo;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;
use File;

class GalleryVideoController extends BaseController
{
    public function index(){
         $setting = Setting::where("module", "video")->first();
        if(empty($setting)) abort(404);

        $gallery = GalleryVideo::where('is_showed', 1)
            ->orderBy('order', 'acs')
            ->orderBy('id', 'desc')
            ->paginate(10)
            ->withPath("/thu-vien-video");

        return view("user/gallery-video/index")
            ->with('setting', $setting)
            ->with('gallery', $gallery);
    }
}
