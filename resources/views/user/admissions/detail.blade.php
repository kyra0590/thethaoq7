@extends('user.layout')

@section('title', $admissions->title)
@section('description', $admissions->description)
@section('keywords', $admissions->keywords)

@section('content')
<div class="grid-box">
    <div class="single-post-box">
        <div class="title-post">
            <h1>{{$admissionsDetail->title}}</h1>
            <ul class="post-tags">
                <li><i class="fa fa-clock-o"></i>{{date("d M Y", strtotime($admissionsDetail->date_show))}}</li>
            </ul>
        </div>
        <div class="post-gallery">
            @if (!empty($admissionsDetail->img) && file_exists($imgPath."/admissions/".$admissionsDetail->img))
            <img src="/public/images/admissions/{{$admissionsDetail->img}}" alt="{{$admissionsDetail->caption}}">
            <span class="image-caption">{{$admissionsDetail->caption}}</span>
            @endif
        </div>
        <div class="post-content">
            {!! $admissionsDetail->content  !!}
        </div>
        @if(!empty($relationAdmissions) && count($relationAdmissions))
        <div class="col-md-12">
            <div class="title-section">
                <h1><span class="world">TIN LIÊN QUAN</span></h1>
            </div>
            <ul class="list-posts">
                @foreach($relationAdmissions as $item)
                <li style="border:none">
                    <div class="post-content">
                        <h2><a href="/chieu-sinh/{{$item->id_admissions}}_{{$item->link}}.html">»» {{$item->title}}</a></h2>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>
@endsection