<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\User\BaseController;
use Illuminate\Http\Request;
use App\Models\Notify;
use App\Models\Setting;

class NotifyController extends BaseController
{
    public function detail($link = "") {
    	if(!$link) {
    		abort(404);
    	}

    	$notify = Notify::where('is_showed', 1)->where('link', $link)->first();

        if(empty($notify)) abort(404);

        $lastestNotify =  Notify::where('is_showed', 1)
            ->where('id','!=', $notify->id)
            ->orderBy('order', 'acs')
            ->orderBy('id', 'desc')
            ->limit(5)
            ->get();

    	return view("user/notify/detail")
            ->with('notify', $notify)
            ->with('lastestNotify', $lastestNotify);
    }
}
