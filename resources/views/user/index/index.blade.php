@extends('user.layout')
@section('title', $title)
@section('description', $description)
@section('keywords', $keywords)
@section('content')

@if(!empty($newsHome))
<div id="hot-news" class="grid-box">
    <div class="title-section">
        <h1><span>Tin nổi bật</span></h1>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="news-post image-post2">
                <div class="news-post standard-post2">
                    <div class="post-gallery">
                        @if (!empty($newsHome[0]->img) && file_exists($imgPath."/news/thumb/".$newsHome[0]->img))
                            <img src="/public/images/news/thumb/{{$newsHome[0]->img}}" alt="{{$newsHome[0]->title}}">
                        @endif
                    </div>
                    <div class="post-title">
                        <h2><a href="/tin-tuc/{{$newsHome[0]->link}}.html">{{$newsHome[0]->title}}</a></h2>
                        <ul class="post-tags">
                            <li>{{$newsHome[0]->short_desc}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <ul class="list-posts">
                @foreach($newsHome as $key => $item)
                    @if($key > 0)
                    <li>
                        <div class="post-content">
                            <h2><a href="/tin-tuc/{{$item->link}}.html">{{$item->title}}</a></h2>
                        </div>
                        <div class="clearfix">
                            @if (!empty($newsHome[0]->img) && file_exists($imgPath."/news/thumb/".$newsHome[0]->img))
                                <img src="/public/images/news/thumb/{{$item->img}}" alt="{{$item->title}}">
                            @endif
                            <p>{{$item->short_desc}}</p>
                        </div>
                    </li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endif

@if (!empty($home->img_1) && file_exists($imgPath."/home/".$home->img_1))
<div class="advertisement">
    <img src="/public/images/home/{{$home->img_1}}" alt="banner advertisement" width="100%" style="max-height: 150px">
</div>
@endif

<div class="grid-box">
    <div class="row">
        <div class="col-md-6">
            @if(!empty($sportHome) && count($sportHome))
            <div class="title-section">
                <h1><span class="fashion">Các bộ môn</span></h1>
            </div>
            <div class="item news-post standard-post">
                <div class="post-gallery">
                    @if (!empty($sportHome[0]->img) && file_exists($imgPath."/sport-detail/thumb/".$sportHome[0]->img))
                        <img src="/public/images/sport-detail/thumb/{{$sportHome[0]->img}}" alt="{{$sportHome[0]->title}}">
                    @endif
                </div>
                <div class="post-content">
                    <h2><a href="/bo-mon/{{$sportHome[0]->id_sport}}-{{$sportHome[0]->link}}.html">{{$sportHome[0]->title}}</a></h2>
                    <p>{{$sportHome[0]->short_desc}}</p>
                </div>
            </div>
            <div class="item">
                <ul class="list-posts">
                    @foreach($sportHome as $key => $item)
                    @if($key > 0)
                    <li>
                        <div class="post-content">
                            <h2><a href="/bo-mon/{{$item->id_sport}}-{{$item->link}}.html">{{$item->title}}</a></h2>
                        </div>
                    </li>
                    @endif
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        <div class="col-md-6">
            @if(!empty($infoHome) && count($infoHome))
            <div class="title-section">
                <h1><span class="world">Thông tin</span></h1>
            </div>
            <div class="item news-post standard-post">
                <div class="post-gallery">
                    @if (!empty($infoHome[0]->img) && file_exists($imgPath."/info/thumb/".$infoHome[0]->img))
                    <img src="/public/images/info/thumb/{{$infoHome[0]->img}}" alt="{{$infoHome[0]->title}}">
                    @endif
                </div>
                <div class="post-content">
                    <h2><a href="/thong-tin/{{$infoHome[0]->id_info}}-{{$infoHome[0]->link}}.html">{{$infoHome[0]->title}}</a></h2>
                    <p>{{$infoHome[0]->short_desc}}</p>
                </div>
            </div>
            <div class="item">
                <ul class="list-posts">
                    @foreach($infoHome as $key => $item)
                        @if($key > 0)
                        <li>
                            <div class="post-content">
                                <h2><a href="/thong-tin/{{$item->id_info}}-{{$item->link}}.html">{{$item->title}}</a></h2>
                            </div>
                        </li>
                        @endif
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
    </div>
</div>

@if (!empty($home->img_2) && file_exists($imgPath."/home/".$home->img_2))
<div class="advertisement">
    <img src="/public/images/home/{{$home->img_2}}" alt="banner advertisement" width="100%" style="max-height: 150px">
</div>
@endif

<div class="grid-box">
    <div class="row">
        <div class="col-md-6">
            @if(!empty($unionHome) && count($unionHome))
            <div class="title-section">
                <h1><span class="fashion">Đảng - đoàn thể</span></h1>
            </div>
            <div class="item news-post standard-post">
                <div class="post-gallery">
                    @if (!empty($unionHome[0]->img) && file_exists($imgPath."/union/thumb/".$unionHome[0]->img))
                        <img src="/public/images/union/thumb/{{$unionHome[0]->img}}" alt="{{$unionHome[0]->title}}">
                    @endif
                </div>
                <div class="post-content">
                    <h2><a href="/dang-doan-the/{{$unionHome[0]->id_union}}-{{$unionHome[0]->link}}.html">{{$unionHome[0]->title}}</a></h2>
                    <p>{{$unionHome[0]->short_desc}}</p>
                </div>
            </div>
            <div class="item">
                <ul class="list-posts">
                    @foreach($unionHome as $key => $item)
                    @if($key > 0)
                    <li>
                        <div class="post-content">
                            <h2><a href="/dang-doan-the/{{$item->id_union}}-{{$item->link}}.html">{{$item->title}}</a></h2>
                        </div>
                    </li>
                    @endif
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        <div class="col-md-6">
            @if(!empty($admissionsHome) && count($admissionsHome))
            <div class="title-section">
                <h1><span class="world">Chiêu sinh</span></h1>
            </div>
            <div class="item news-post standard-post">
                <div class="post-gallery">
                    @if (!empty($admissionsHome[0]->img) && file_exists($imgPath."/admissions/thumb/".$admissionsHome[0]->img))
                        <img src="/public/images/admissions/thumb/{{$admissionsHome[0]->img}}" alt="{{$admissionsHome[0]->title}}">
                    @endif
                </div>
                <div class="post-content">
                    <h2><a href="/chieu-sinh/{{$admissionsHome[0]->id_admissions}}-{{$admissionsHome[0]->link}}.html">{{$admissionsHome[0]->title}}</a></h2>
                    <p>{{$admissionsHome[0]->short_desc}}</p>
                </div>
            </div>
            <div class="item">
                <ul class="list-posts">
                    @foreach($admissionsHome as $key => $item)
                    @if($key > 0)
                    <li>
                        <div class="post-content">
                            <h2><a href="/chieu-sinh/{{$item->id_admissions}}-{{$item->link}}.html">{{$item->title}}</a></h2>
                        </div>
                    </li>
                    @endif
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
    </div>
</div>

@if (!empty($home->img_3) && file_exists($imgPath."/home/".$home->img_3))
<div class="advertisement">
    <img src="/public/images/home/{{$home->img_3}}" alt="banner advertisement" width="100%" style="max-height: 150px">
</div>
@endif

@endsection