@extends('user.layout')

@section('title', $setting->title)
@section('description', $setting->description)
@section('keywords', $setting->keywords)

@section('css')
<link href="/public/css/lightgallery.css" rel="stylesheet">
<style>
    .demo-gallery {
       /* background: #152836;
        padding: 30px 10px 0px 10px;
        border-radius: 10px;*/
    }

    .demo-gallery > ul {
      margin-bottom: 0;
    }
    .demo-gallery > ul > li {
        float: left;
        margin-bottom: 20px;
        color:#fff;
        text-align: center;
    }

    .demo-gallery > ul > li a {
      border: 3px solid #FFF;
      border-radius: 3px;
      /*display: block;*/
      overflow: hidden;
      position: relative;
      float: left;
      width: 100%;
      height: 150px;
      display: table-cell;
    }
    .demo-gallery > ul > li a > img {
      -webkit-transition: -webkit-transform 0.15s ease 0s;
      -moz-transition: -moz-transform 0.15s ease 0s;
      -o-transition: -o-transform 0.15s ease 0s;
      transition: transform 0.15s ease 0s;
      -webkit-transform: scale3d(1, 1, 1);
      transform: scale3d(1, 1, 1);
      height: auto;
      width: 100%;
    }

    .demo-gallery > ul > li a:hover > img {
      -webkit-transform: scale3d(1.1, 1.1, 1.1);
      transform: scale3d(1.1, 1.1, 1.1);
    }

    .demo-gallery > ul > li a .demo-gallery-poster {
      background-color: rgba(0, 0, 0, 0.1);
      bottom: 0;
      left: 0;
      position: absolute;
      right: 0;
      top: 0;
      -webkit-transition: background-color 0.15s ease 0s;
      -o-transition: background-color 0.15s ease 0s;
      transition: background-color 0.15s ease 0s;
    }

    .demo-gallery > ul > li a:hover .demo-gallery-poster {
      background-color: rgba(0, 0, 0, 0.5);
    }

    .demo-gallery > ul > li a .demo-gallery-poster > img {
      left: 50%;
      margin-left: -10px;
      margin-top: -10px;
      opacity: 0;
      position: absolute;
      top: 50%;
      -webkit-transition: opacity 0.3s ease 0s;
      -o-transition: opacity 0.3s ease 0s;
      transition: opacity 0.3s ease 0s;
    }

    .demo-gallery > ul > li a:hover .demo-gallery-poster > img {
      opacity: 1;
    }

</style>
@section('content')
<div class="grid-box" >
    <div class="title-section">
        <h1><span class="sport">Thư viện ảnh : {{$gallery->title}}</span></h1>
    </div>
     @if(!empty($images) && count($images))
     <div class="demo-gallery">
        <ul id="lightgallery" class="list-unstyled row">
            @foreach($images as $item)
                <li class="col-xs-6 col-sm-4 col-md-3" data-src="/public/images/gallery/image/{{$gallery->id}}/{{$item}}" data-sub-html="{{$gallery->title}}">
                   <a href="#">
                        <img class="img-responsive" src="/public/images/gallery/image/{{$gallery->id}}/{{$item}}">
                    <div class="demo-gallery-poster">
                      <img src="/public/images/zoom.png">
                    </div>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
    @else
      <p>Hiện tại không tìm thấy dữ liệu</p>
    @endif
</div>
@endsection
@section('js')
    <script type="text/javascript">
    $(document).ready(function(){
        $('#lightgallery').lightGallery();
    });
    </script>
@endsection
