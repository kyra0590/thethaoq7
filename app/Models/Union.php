<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Union extends Model
{
	protected $table      = 'union';
	protected $primaryKey = 'id';
	public $timestamps    = true;
}
