<header class="clearfix fifth-style">
    <!-- Bootstrap navbar -->


    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <!-- navbar list container -->
        <div class="logo-advertisement">
            <div class="container">
                <div id="slider_top"  class="show_pc">
                    <div id="slider_nivo" class="nivoSlider">
                        <img src="images/slide1.jpg" alt="">
                        <img src="images/slide2.jpg" alt="">
                        <img src="images/slide3.jpg" alt="">
                        <img src="images/slide4.jpg" alt="">
                    </div>
                      <div class="bg_slider_top">
                        <img src="images/bg_slider_top.png" alt="">
                    </div>
                </div>
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header show_sp">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="images/logo-black.png" height="80px" alt=""></a>
                </div>
            </div>
        </div>

        <div id="slider_top">

        </div>

        <div class="nav-list-container">
            <div class="container">
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left">
                        <li><a class="home" href="index.php">Trang chủ</a>
                        </li>
                        <li class="drop"><a class="tech" href="gioi-thieu.php">Giới thiệu</a>
                            <ul class="dropdown">
                                <li><a href="gioi-thieu/1_gioi-thieu-chi-bo-trung-tam.php">Chi bộ Trung tâm</a></li>
                                <li><a href="gioi-thieu/2_gioi-thieu-trung-tam.php">trung tâm</a></li>
                                <li><a href="gioi-thieu/3_gioi-thieu-co-cau-to-chuc.php">Cơ cấu tổ chức</a></li>
                                <li><a href="gioi-thieu/4_phuong-huong-hoat-dong.php">Phương hướng hoạt động</a></li>
                            </ul>
                        </li>
                        <li><a class="travel" href="tin-tuc-su-kien.php">Tin tức - Sự kiện</a></li>
                       <!--  <li class="drop"><a class="tech" href="index.php">Câu lạc bộ</a>
                            <ul class="dropdown">
                                <li><a href="cau-lac-bo/1_cau-lac-bo-tdtt-rach-mieu.php">Câu lạc bộ TDTT Rạch Miễu</a></li>
                                <li><a href="cau-lac-bo/2_cau-lac-bo-bong-da.php">Câu lạc bộ Bóng đá</a></li>
                                <li><a href="cau-lac-bo/3_cau-lac-bo-tdtt-chi-lang.php">Câu lạc bộ TDTT Chi Lăng</a></li>
                                <li><a href="cau-lac-bo/4_cau-lac-bo-the-thao-hoc-duong.php">Câu lạc bộ Thể thao Học đường</a></li>
                                <li><a href="cau-lac-bo/5_van-phong-trung-tam.php">Văn phòng Trung tâm</a></li>
                                <li><a href="cau-lac-bo/6_khu-tdtt-truong-sa.php">Khu TDTT Trường Sa</a></li>
                            </ul>
                        </li> -->
                        <li><a class="sport" href="#">Các Bộ môn</a>
                            <div  id="slider_sports" class="megadropdown">
                                <div class="container">
                                    <div class="inner-megadropdown tech-dropdown">
                                        <div class="owl-wrapper">
                                            <h1>Danh sách các bộ môn</h1>
                                            <div class="owl-carousel" data-num="8">
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/akido.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="akido.php">Aikido</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/boxing.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Boxing</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/judo.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Judo</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/karate.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Karate</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/kickboxing.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Kickboxing</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/muay.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Muay</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/pencak_silat.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Pencak silat</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/taekwondo.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Taekwondo</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/vocotruyen.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Võ cổ truyền</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/vovinam.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Vovinam</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/wushu.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Wushu</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/boiloi.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Bơi lội</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/bongban.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Bóng bàn</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/bongchuyen.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Bóng chuyền</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/bongda.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Bóng Đá</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/bongro.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Bóng Rổ</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/caulong.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Cầu Lông</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/caumay.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Cầu mây</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/cotuong.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Cờ tướng</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/covua.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Cờ Vua</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/daygay.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Đẩy gậy</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/dienkinh.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Điền Kinh</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/keoco.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Kéo co</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/quanvot.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Quần vợt</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/theducnhipdieu.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">TD nhịp điệu</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/theducthammy.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">TD thẩm mỹ</a></h2>
                                                    </div>
                                                </div>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <img src="images/icons/xedap.jpg" alt="">
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="#">Xe đạp</a></h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="drop"><a class="fashion" href="#">Thư viện</a>
                            <ul class="dropdown">
                                <li><a href="thu-vien-video.php">Video clip</a></li>
                                <li><a href="thu-vien-hinh-anh.php">Album hình</a></li>
                            </ul>
                        </li>
                        <li class="drop"><a class="video" href="thong-tin-chuyen-nganh.php">Thông tin</a>
                            <ul class="dropdown">
                              <!--   <li><a href="thong-tin-chuyen-nganh/1_thong-tin-tuyen-sinh-he-2015.php">Thông tin tuyển sinh Hè 2015</a></li>
                                <li><a href="thong-tin-chuyen-nganh/2_thong-tin-dao-tao-nghiep-vu.php">Thông tin đào tạo nghiệp vụ</a></li> -->
                                <li><a href="thong-tin-chuyen-nganh/3_bang-gia-dich-vu-nam-2018.php">Đào tạo nghiệp vụ</a></li>
                                <li><a href="thong-tin-chuyen-nganh/4_bang-gia-dich-vu-2019.php">Tuyển sinh hè 2020</a></li>
                            </ul>
                        </li>
                        <li class="drop"><a class="food" href="doan-the.php">Đảng - đoàn thể</a>
                            <ul class="dropdown">
                                <li><a href="doan-the/1_chi-bo.php">Chi bộ</a></li>
                                <li><a href="doan-the/3_cong-doan-co-so.php">Công đoàn cơ sở</a></li>
                                <li><a href="doan-the/2_chi-doan.php">Chi Đoàn</a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="features" href="lien-he.php">Liên hệ</a>
                        </li>
                    </ul>

                </div>
                <!-- /.navbar-collapse -->
            </div>
        </div>
        <!-- End navbar list container -->
        <!-- Logo & advertisement -->

        <!-- End Logo & advertisement -->
    </nav>
    <!-- End Bootstrap navbar -->
</header>
<!-- End Header -->
<!-- big-slider-section
            ================================================== -->
<section class="big-slider">
    <div class="container">
        <div class="image-slider">
            <ul class="big-bxslider">
                <li>
                    <div class="news-post image-post2">
                        <div class="post-gallery">
                            <img src="images/hd1.jpg" alt="">
                           <!--  <div class="hover-box">
                                <div class="inner-hover">
                                    <h2><a href="#">Tranh giải vô địch PNC</a></h2>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </li>
                <li>
                    <div class="news-post image-post2">
                        <div class="post-gallery">
                            <img src="images/hd2.jpg"  alt="">
                        </div>
                    </div>
                </li>
                <li>
                    <div class="news-post image-post2">
                        <div class="post-gallery">
                            <img src="images/hd3.jpg"  alt="">
                        </div>
                    </div>
                </li>
                <li>
                    <div class="news-post image-post2">
                        <div class="post-gallery">
                            <img src="images/hd4.jpg" alt="">
                        </div>
                    </div>
                </li>
                <li>
                    <div class="news-post image-post2">
                        <div class="post-gallery">
                            <img src="images/hd5.jpg" alt="">
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</section>