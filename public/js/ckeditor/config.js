/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	// config.stylesSet.default = [
	// 	{name: 'Khung bao ảnh và mô tả', element: 'div', attributes: {'class' : 'post-gallery'}},
	//     {name: 'Mô tả ảnh', element: 'span', attributes:{'class': 'image-caption'}},
	//     {name: 'Khung bao chú thích', element: 'div', attributes:{'class': 'post-content'}},
	//     {name: 'Bảng chi tiết loại 1', element: 'table', attributes: {'class': 'table'}},
	//     {name: 'Bảng chi tiết loại 2', element: 'table', attributes:{'class': 'table table-striped'}},
	//     {name: 'Bảng chi tiết loại 3', element: 'table', attributes:{'class': ' table-borderless'}}
	// ];
};
