<?php

return [
    'clients' => 'Clients',
    'projects' => 'Project',
    'countries' => 'Country',
    'mission' => 'Mission',
    'mission_message' => 'Become a prosperous organization for customers, employees, shareholders and other stakeholders around the globe',
    'contact_us' => 'Contact',
    'vision' => 'Vision',
    'vision_message' => 'Become a prosperous organization for customers, employees, shareholders and other stakeholders around the globe',
    'core_value' => 'Core Value',
    'core_value_message' => 'Creative - Transparent - Drastic to the end. <br><br>
    <div class="hc-component hc-cmp-steps box-steps">
        <div class="step-item">
            <span>1</span>
            <div class="content"><h3>Creative</h3>
            <p>Thể Thao Quận 7 accepts positive and even negative feedbacks to create results for customers. Thể Thao Quận 7 is always looking for new and useful solutions for the activities of each person, department and company. Thể Thao Quận 7 encourages two forms of creativity:
            <ul>
                <li>Empirical creativity: A form of creativity that is new and useful at the department level</li>
                <li>Breakthrough creation: A new and useful form of innovation at the corporate level or above.</li>
            </ul>
            </p></div>
        </div>
         <div class="step-item">
            <span>2</span>
            <div class="content"><h3>Transparent</h3>
            <p>Thể Thao Quận 7 is always clear and specific in all its activities. Thể Thao Quận 7 recognizes two types of transparency:
            <ul>
                <li>Internal Transparency: Being transparent with colleagues, superiors and subordinates.</li>
                <li>External transparency: Being transparent with customers, suppliers and other partners.</li>
            </ul>
            </p></div>
        </div>
         <div class="step-item">
            <span>3</span>
            <div class="content"><h3>Drastic to the end</h3>
            <p>Thể Thao Quận 7 will never give up without reaching its goal. Thể Thao Quận 7 is willing to accept changes in execution methods if the results reflect the method\'s nonconformity. Thể Thao Quận 7 acts quickly to achieve results, promptly detects errors and redo them to get the desired results.</p></div>
        </div>
    </div>',
    'message_ceo' => 'CEO MESSAGE',
    'message_ceo_content' => 'We would like to start with our sincere and profound thanks to the customers who have been and will be choosing Thể Thao Quận 7 in the future. We understand very well that we in the past were an enterprise from a country not yet a market economy in its full sense, but not really professional business culture. However, we currently own a young, enthusiastic and talented staff. In addition, we also understand that entrepreneurs must be dreamers big enough and determined to make their dreams come true despite the fact that they are very small, as evidenced by many stories. The success of the automobile garage has created global change.<br><br>
        Therefore, we would like to introduce you to a young company with a dream of global change through a miraculous mindset that helps customers realize their dreams through experience of services and products. our products.<br>
        <ul>
        <li>We are not merely consulting but providing solutions to customers to choose the services and products to help you satisfy after many years without a professional management tool.</li>
        <li>We look forward to meeting our companions, changing ourselves from within the business and fulfilling our great dreams through real-world experience</li>
        <li>We provide you with a valuable solution that makes a difference from inside to help you become a total change and fulfill your dreams. That\'s called Thể Thao Quận 7!</li>
        </ul>
        Lastly, wish you all the best for your successful transformation process to become a global company, fulfilling your dream of building an entrepreneurial era.<br>
        <br>
        Best regards! ',
    'about_us_title_1' => 'About us',
    'abount_us_content_1' => "
        Established in Vietnam: Dec 2014 <br>
        Net assets 150 billion VND <br>
        Employees: 150 <br>
        Business producer: 15 <br>
        Listing: OTC <br>
        Number of project: 45 <br>
        Number of companies by model: 40 <br>
        Established in Singapore: Sep 2019",

    'contact_title_2' => 'Information Contact',
    'contact_subtitle_2' => 'Thể Thao Quận 7',
    'contact_title_1' => 'Request to us',
    'contact_policy' => 'You accept term and privacy policy',
    'working_process' => 'Working process',
    'working_process_title_1' => 'Survey and design',
    'working_process_title_2' => 'Investment decision',
    'working_process_title_3' => 'Project implementation',
    'working_process_content_1' => 'Assessing the current state of the operating enterprise, <br>
    focusing on the current situation of cash flow, <br>
    products and human resource.<br> Develop an investment strategy',
    'working_process_content_2' => 'Agree on investment plans and related contents',
    'working_process_content_3' => 'Contribute resources to implement the project as planned: <br>
    - Capital <br>
    - Human resources<br>
    - Intellectual property',
    'field' => 'Field',
    'field_title_1' => 'Invesment',
    'field_title_2' => 'Management',
    'field_title_3' => 'Consulting',
    'field_content_1' => '
        - Private company investment <br>
        - Growth fund',
    'field_content_2' => '- Group of companies <br>
        - Subsidiaries',
    'field_content_3' => '- Management advice <br>
        - M&A advisory <br>
        - Support market expansion to Asia <br>
        - Senior human resource training',
];
