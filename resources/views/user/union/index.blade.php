@extends('user.layout')

@section('title', $setting->title)
@section('description', $setting->description)
@section('keywords', $setting->keywords)

@section('content')
<div class="grid-box">
    <div class="title-section">
        <h1><span>Đảng - Đoàn Thể</span></h1>
    </div>
    @if(!empty($union) && count($union))
        @foreach($union as $item)
        <div class="news-post article-post">
            <div class="row">
                <div class="col-sm-3">
                    <div class="post-gallery">
                        @if (!empty($item->img) && file_exists($imgPath."/union/thumb/".$item->img))
                            <img src="/public/images/union/thumb/{{$item->img}}"  alt="{{$item->caption}}" width="100%">
                        @else
                            <img src="/public/images/logo_thumb.jpg" style="width: auto !important; height: 150px">
                        @endif
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="post-content">
                        <h2><a href="/dang-doan-the/{{$item->id_union}}-{{$item->link}}.html">{{$item->title}}</a></h2>
                        <p>{{$item->short_desc}}</p>
                        <a href="/dang-doan-the/{{$item->id_union}}-{{$item->link}}.html" class="read-more-button"><i class="fa fa-arrow-circle-right"></i>Đọc thêm</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    @endif
</div>
@if(!empty($union) && count($union))
<div class="pagination-box">
    <ul class="pagination-list">
        @if ($union->lastPage() == 1)
            <li><a class="active" href="#">1</a></li>
        @elseif ($union->lastPage() > 1)
            @if($union->currentPage() > 1)
                <li><a href="{{ $union->url($union->currentPage() - 1) }}">Back</a></li>
            @endif

             @for ($i = 1; $i <= $union->lastPage(); $i++)
                <li><a class="{{ ($union->currentPage() == $i) ? ' active' : '' }}" href="{{ $union->url($i) }}">{{$i}}</a></li>
            @endfor

            @if($union->currentPage() < $union->lastPage())
            <li><a href="{{ $union->url($union->currentPage()+1) }}">Next</a></li>
            @endif
        @endif
    </ul>

    <p>Page {{$union->currentPage()}} of {{$union->lastPage()}}</p>
</div>
@else
    <p>Hiện tại không tìm thấy dữ liệu</p>
@endif

@endsection