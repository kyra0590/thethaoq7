<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class IndexController extends BaseController
{
    public function index(){
        return view('admin/index/index')
            ->with('title', "Quản Trị Thể Thao Quận 7");
    }

    public function login(Request $request){
        $params = $request->all();
        $status = true;
        if(!empty($params)) {
            $credentials = [
                'username' => trim($params['username']),
                'password' => $params['password'],
                'permission' => 1,
                'block' => 0
            ];
            $remember = isset($params['remember']) ? (bool) $params['remember'] : false;
            if(Auth::attempt($credentials, $remember)) {
                return redirect()->intended('admin');
            }
            $status = false;
        }

        return view('admin/index/login')
            ->with('status', $status);
    }

    public function logout(){
        Auth::logout();
        return redirect()->intended('admin/login');
    }

    public function updateOrder(Request $request){
    	$params = $request->all();
    	$status = false;
    	if(!empty($params) && !empty($params['table_name'])) {
    		$tableName = $params['table_name'];
    		$listOrder = $params['order'];
    		foreach ($listOrder as $key => $value) {
    			DB::table($tableName)->where('id', $key)->update(['order' => $value]);
    		}
    		$status = true;
    	}
    	return response()->json(['status' => $status]);
    }

     public function delFile(Request $request){
        $params = $request->all();
        $status = false;
        if(!empty($params) && !empty($params['dir'])) {
            $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/".$params['dir'];
            if(file_exists($descriptionPath)) {
                unlink($descriptionPath);
                $status = true;
            }
        }
        return response()->json(['status' => $status], ($status) ? 200 : 400);
    }
}
