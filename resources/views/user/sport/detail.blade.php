@extends('user.layout')

@section('title', $sport->title)
@section('description', $sport->description)
@section('keywords', $sport->keywords)

@section('content')
<div class="grid-box">
    <div class="single-post-box">
        <div class="title-post">
            <h1>{{$sportDetail->title}}</h1>
            <ul class="post-tags">
                <li><i class="fa fa-clock-o"></i>{{date("d M Y", strtotime($sportDetail->date_show))}}</li>
            </ul>
        </div>
        <div class="post-gallery">
            @if (!empty($sportDetail->img) && file_exists($imgPath."/sport-detail/".$sportDetail->img))
            <img src="/public/images/sport-detail/{{$sportDetail->img}}" alt="{{$sportDetail->caption}}">
            <span class="image-caption">{{$sportDetail->caption}}</span>
            @endif
        </div>
        <div class="post-content">
            {!! $sportDetail->content  !!}
        </div>
        @if(!empty($relationSport) && count($relationSport))
        <div class="col-md-12">
            <div class="title-section">
                <h1><span class="world">TIN LIÊN QUAN</span></h1>
            </div>
            <ul class="list-posts">
                @foreach($relationSport as $item)
                <li style="border:none">
                    <div class="post-content">
                        <h2><a href="/bo-mon/{{$item->id_sport}}-{{$item->link}}.html">»» {{$item->title}}</a></h2>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>
@endsection