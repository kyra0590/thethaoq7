@extends('user.layout')

@section('title', $setting->title)
@section('description', $setting->description)
@section('keywords', $setting->keywords)

@section('content')
<div class="grid-box">
    <div class="title-section">
        <h1><span>Giới thiệu</span></h1>
    </div>
    @if(!empty($intro) && count($intro))
        @foreach($intro as $item)
        <div class="news-post article-post">
            <div class="row">
                <div class="col-sm-3">
                    <div class="post-gallery">
                        @if (!empty($item->img) && file_exists($imgPath."/intro/thumb/".$item->img))
                            <img src="/public/images/intro/thumb/{{$item->img}}"  alt="{{$item->caption}}" width="100%">
                        @else 
                            <img src="/public/images/logo_thumb.jpg" style="width: auto !important; height: 150px">
                        @endif
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="post-content">
                        <h2><a href="/gioi-thieu/{{$item->id_intro}}-{{$item->link}}.html">{{$item->title}}</a></h2>
                        <p>{{$item->short_desc}}</p>
                        <a href="/gioi-thieu/{{$item->id_intro}}-{{$item->link}}.html" class="read-more-button"><i class="fa fa-arrow-circle-right"></i>Đọc thêm</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    @endif
</div>
@if(!empty($intro) && count($intro))
<div class="pagination-box">
    <ul class="pagination-list">
        @if ($intro->lastPage() == 1)
            <li><a class="active" href="#">1</a></li>
        @elseif ($intro->lastPage() > 1)
            @if($intro->currentPage() > 1)
                <li><a href="{{ $intro->url($intro->currentPage() - 1) }}">Back</a></li>
            @endif

             @for ($i = 1; $i <= $intro->lastPage(); $i++)
                <li><a class="{{ ($intro->currentPage() == $i) ? ' active' : '' }}" href="{{ $intro->url($i) }}">{{$i}}</a></li>
            @endfor

            @if($intro->currentPage() < $intro->lastPage())
            <li><a href="{{ $intro->url($intro->currentPage()+1) }}">Next</a></li>
            @endif
        @endif
    </ul>

    <p>Page {{$intro->currentPage()}} of {{$intro->lastPage()}}</p>
</div>
@else
    <p>Hiện tại không tìm thấy dữ liệu</p>
@endif

@endsection