<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\User\BaseController;
use Illuminate\Http\Request;
use App\Models\Sport;
use App\Models\SportDetail;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;

class SportController extends BaseController
{
    public function index(){
        $setting = Setting::where("module", "sport")->first();
        if(empty($setting)) abort(404);

        $sport = DB::table('sport_detail')
            ->selectRaw("sport_detail.id, sport_detail.title, sport.title as sport_title,
                sport_detail.short_desc, sport_detail.content,
                sport_detail.keywords, sport_detail.is_showed,
                sport_detail.date_show, sport_detail.link,
                sport_detail.order, sport_detail.img,
                sport.link as sportLink, sport_detail.id_sport,
                sport_detail.caption")
            ->join('sport', 'sport.id', '=', 'sport_detail.id_sport')
            ->where('sport.is_showed', 1)
            ->where('sport_detail.is_showed', 1)
            ->orderBy('sport_detail.order', 'asc')
            ->orderBy('sport_detail.id', 'desc')
            ->paginate(10)
            ->withPath("/bo-mon");

        return view("user/sport/index")
            ->with('setting', $setting)
            ->with('sport', $sport);
    }

    public function list($id = "", $link = "") {
    	if(!$id && !$link) {
    		abort(404);
    	}

    	$sport = Sport::where('is_showed', 1)
            ->where('id', $id)
            ->where('link', $link)->first();

        if(empty($sport)) abort(404);

        $sportDetail = DB::table('sport_detail')
            ->selectRaw("sport_detail.id, sport_detail.title, sport.title as sport_title,
                sport_detail.short_desc, sport_detail.content,
                sport_detail.keywords, sport_detail.is_showed,
                sport_detail.date_show, sport_detail.link,
                sport_detail.order, sport_detail.img,
                sport.link as sportLink, sport_detail.id_sport,
                sport_detail.caption")
            ->join('sport', 'sport.id', '=', 'sport_detail.id_sport')
            ->where('sport.id', $id)
            ->where('sport.is_showed', 1)
            ->where('sport_detail.is_showed', 1)
            ->orderBy('sport_detail.order', 'asc')
            ->orderBy('sport_detail.id', 'desc')
            ->paginate(10)
            ->withPath("/bo-mon");

    	return view("user/sport/list")
            ->with('sport', $sport)
            ->with('sportDetail', $sportDetail);
    }

     public function detail($id = "", $link = "") {
        if(!$id && !$link) {
            abort(404);
        }

        $sport = Sport::where('is_showed', 1)->where('id', $id)->first();

        if(empty($sport)) abort(404);

        $sportDetail = SportDetail::where('is_showed', 1)
            ->where('id_sport', $id)
            ->where('link', $link)
            ->first();

        if(empty($sportDetail)) abort(404);

        $relationSport =  SportDetail::where('is_showed', 1)
            ->where('id_sport', $id)
            ->where('link', "!=", $link)
            ->limit(5)
            ->get();

        return view("user/sport/detail")
            ->with('sport', $sport)
            ->with('sportDetail', $sportDetail)
            ->with('relationSport', $relationSport);
    }
}
