<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notify extends Model
{
	protected $table      = 'notify';
	protected $primaryKey = 'id';
	public $timestamps    = true;
}
