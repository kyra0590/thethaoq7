<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use App\Models\SliderTop;
use Image;

class SliderTopController extends BaseController
{
    public function index(Request $request){
        $params  = $request->all();
        $isAjax  = (isset($params['is_ajax'])) ? $params['is_ajax'] : false;
        $start   = (!empty($params['start'])) ? $params['start'] : 0;
        $keyword = (!empty($params['keyword'])) ? $params['keyword'] : '';
        $limit   =  (!empty($params['limit'])) ? $params['limit'] : 50;
        $slider    = SliderTop::where('title', 'like', "%{$keyword}%")
            ->orderBy('order', 'asc')
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->offset($start)
            ->get();
        if($isAjax) {
            return response()->json(
                 [
                    "recordsFiltered" => count($slider),
                    "recordsTotal" => count($slider),
                    "data" => $slider
                 ], 200
            );
        }
        return view('admin/slider-top/index')
            ->with('title', "Danh sách slider top");
    }

    public function add(Request $request){
        $params = $request->all();
        $title = "Thêm slider top";
        $status = false;
        if(!empty($params)) {
            $params['title'] = trim($params['title']);
            $params['created_at'] = date("Y-m-d");
            $params['order'] = !empty($params['order']) ? (int) $params['order'] : 0;
            unset($params['_token']);
            if($request->hasFile('img')) {
                $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/slider-top/";
                $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                    $params['img'] = $fileName;

                    $imgThumb = Image::make($descriptionPath.$fileName);
                    $imgThumb->resize(1000, 200, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($descriptionPath.$fileName);
                }
            }
            $status = SliderTop::insert($params);
        }
        return view('admin/slider-top/add')
            ->with('title', $title)
            ->with('status', $status);
    }

     public function edit(Request $request, $id){
        $title  = 'Sửa slider top';
        $slider = SliderTop::find($id);
        $status = false;
        if(!$id || empty($slider)) {
           return redirect('/admin/slider');
        }
        $params = $request->all();
        if(!empty($params)) {
            $slider->title     = trim($params['title']);
            $slider->is_showed = $params['is_showed'];
            $slider->order     = !empty($params['order']) ? (int) $params['order'] : 0;
            $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/slider-top/";
            if($request->hasFile('img')) {
                $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                    if(!empty($slider->img) && file_exists($descriptionPath.$slider->img)) {
                        unlink($descriptionPath.$slider->img);
                    }

                    $imgThumb = Image::make($descriptionPath.$fileName);
                    $imgThumb->resize(1000, 200, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($descriptionPath.$fileName);

                    $slider->img = $fileName;
                }
            } else {
                if( $params['remove_img'] && file_exists($descriptionPath.$slider->img)) {
                    unlink($descriptionPath.$slider->img);
                    $slider->img = "";
                }
            }
            $status = $slider->save();
        }
        return view('admin/slider-top/edit')
            ->with('title', $title)
            ->with('slider', $slider)
            ->with('status', $status);
    }

    public function del($id){
         if($id){
            SliderTop::where('id', $id)->delete();
            $descriptionPath    = $_SERVER['DOCUMENT_ROOT']."/public/images/slider-top/";
            if(!empty($slider->img) && file_exists($descriptionPath.$slider->img)) {
                unlink($descriptionPath.$slider->img);
            }
        }
        return redirect("/admin/slider");
    }

}
