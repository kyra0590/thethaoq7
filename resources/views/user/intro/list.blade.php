@extends('user.layout')

@section('title', $intro->title)
@section('description', $intro->description)
@section('keywords', $intro->keywords)

@section('content')
<div class="grid-box">
    <div class="title-section">
        <h1><span>{{$intro->title}}</span></h1>
    </div>
    @if(!empty($introDetail) && count($introDetail))
        @foreach($introDetail as $item)
        <div class="news-post article-post">
            <div class="row">
                <div class="col-sm-3">
                    <div class="post-gallery">
                        @if (!empty($item->img) && file_exists($imgPath."/intro/thumb/".$item->img))
                            <img src="/public/images/intro/thumb/{{$item->img}}"  alt="{{$item->caption}}" width="100%">
                        @else 
                            <img src="/public/images/logo_thumb.jpg" style="width: auto !important; height: 150px">
                        @endif
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="post-content">
                        <h2><a href="/gioi-thieu/{{$item->id_intro}}-{{$item->link}}.html">{{$item->title}}</a></h2>
                        <p>{{$item->short_desc}}</p>
                        <a href="/gioi-thieu/{{$item->id_intro}}-{{$item->link}}.html" class="read-more-button"><i class="fa fa-arrow-circle-right"></i>Đọc thêm</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    @endif
</div>
 @if(!empty($introDetail) && count($introDetail))
<div class="pagination-box">
    <ul class="pagination-list">
        @if ($introDetail->lastPage() == 1)
            <li><a class="active" href="#">1</a></li>
        @elseif ($introDetail->lastPage() > 1)
            @if($introDetail->currentPage() > 1)
                <li><a href="{{ $introDetail->url($introDetail->currentPage() - 1) }}">Back</a></li>
            @endif

             @for ($i = 1; $i <= $introDetail->lastPage(); $i++)
                <li><a class="{{ ($introDetail->currentPage() == $i) ? ' active' : '' }}" href="{{ $introDetail->url($i) }}">{{$i}}</a></li>
            @endfor

            @if($introDetail->currentPage() < $introDetail->lastPage())
            <li><a href="{{ $introDetail->url($introDetail->currentPage()+1) }}">Next</a></li>
            @endif
        @endif
    </ul>

    <p>Page {{$introDetail->currentPage()}} of {{$introDetail->lastPage()}}</p>
</div>
@else
    <p>Hiện tại không tìm thấy dữ liệu</p>
@endif

@endsection