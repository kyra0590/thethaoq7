<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web'], 'prefix' => 'admin'], function(){
	Route::any('/login', 'Admin\IndexController@login');
	Route::any('/logout', 'Admin\IndexController@logout');
});

Route::group(['middleware' => ['auth.admin', 'web'], 'prefix' => 'admin'], function () {
	Route::get('/', 'Admin\IndexController@index');
	Route::any('/update-order', 'Admin\IndexController@updateOrder');
	Route::any('/del-file', 'Admin\IndexController@delFile');
	// Home
	Route::any('/home', 'Admin\HomeController@index');

	// Setting
	Route::any('/setting/add', 'Admin\SettingController@add');
	Route::any('/setting/edit/{id}', 'Admin\SettingController@edit')->where('id', '[0-9]+');
	Route::get('/setting/del/{id}', 'Admin\SettingController@del')->where('id', '[0-9]+');
	Route::any('/setting', 'Admin\SettingController@index');

	// Sport
	Route::any('/sport', 'Admin\SportController@index');
	Route::any('/sport/add', 'Admin\SportController@add');
	Route::any('/sport/edit/{id}', 'Admin\SportController@edit')->where('id', '[0-9]+');
	Route::any('/sport/del/{id}', 'Admin\SportController@del')->where('id', '[0-9]+');
	// Sport Detail
	Route::any('/sport-detail', 'Admin\SportController@detail');
	Route::any('/sport-detail/add', 'Admin\SportController@addDetail');
	Route::any('/sport-detail/edit/{id}', 'Admin\SportController@editDetail')->where('id', '[0-9]+');
	Route::any('/sport-detail/del/{id}', 'Admin\SportController@delDetail')->where('id', '[0-9]+');
	// Slider
	Route::any('/slider/add', 'Admin\SliderController@add');
	Route::any('/slider/edit/{id}', 'Admin\SliderController@edit')->where('id', '[0-9]+');
	Route::get('/slider/del/{id}', 'Admin\SliderController@del')->where('id', '[0-9]+');
	Route::any('/slider', 'Admin\SliderController@index');
	// Slider top
	Route::any('/slider-top/add', 'Admin\SliderTopController@add');
	Route::any('/slider-top/edit/{id}', 'Admin\SliderTopController@edit')->where('id', '[0-9]+');
	Route::get('/slider-top/del/{id}', 'Admin\SliderTopController@del')->where('id', '[0-9]+');
	Route::any('/slider-top', 'Admin\SliderTopController@index');

	// Intro
	Route::any('/intro', 'Admin\IntroController@index');
	Route::any('/intro/add', 'Admin\IntroController@add');
	Route::any('/intro/edit/{id}', 'Admin\IntroController@edit')->where('id', '[0-9]+');
	Route::any('/intro/del/{id}', 'Admin\IntroController@del')->where('id', '[0-9]+');
	// Intro Detail
	Route::any('/intro-detail', 'Admin\IntroController@detail');
	Route::any('/intro-detail/add', 'Admin\IntroController@addDetail');
	Route::any('/intro-detail/edit/{id}', 'Admin\IntroController@editDetail')->where('id', '[0-9]+');
	Route::any('/intro-detail/del/{id}', 'Admin\IntroController@delDetail')->where('id', '[0-9]+');

	// News
	Route::any('/news/add', 'Admin\NewsController@add');
	Route::any('/news/edit/{id}', 'Admin\NewsController@edit')->where('id', '[0-9]+');
	Route::get('/news/del/{id}', 'Admin\NewsController@del')->where('id', '[0-9]+');
	Route::any('/news', 'Admin\NewsController@index');

	// Info
	Route::any('/info', 'Admin\InfoController@index');
	Route::any('/info/add', 'Admin\InfoController@add');
	Route::any('/info/edit/{id}', 'Admin\InfoController@edit')->where('id', '[0-9]+');
	Route::any('/info/del/{id}', 'Admin\InfoController@del')->where('id', '[0-9]+');
	// Info Detail
	Route::any('/info-detail', 'Admin\InfoController@detail');
	Route::any('/info-detail/add', 'Admin\InfoController@addDetail');
	Route::any('/info-detail/edit/{id}', 'Admin\InfoController@editDetail')->where('id', '[0-9]+');
	Route::any('/info-detail/del/{id}', 'Admin\InfoController@delDetail')->where('id', '[0-9]+');

	// Union
	Route::any('/union', 'Admin\UnionController@index');
	Route::any('/union/add', 'Admin\UnionController@add');
	Route::any('/union/edit/{id}', 'Admin\UnionController@edit')->where('id', '[0-9]+');
	Route::any('/union/del/{id}', 'Admin\UnionController@del')->where('id', '[0-9]+');
	// Union Detail
	Route::any('/union-detail', 'Admin\UnionController@detail');
	Route::any('/union-detail/add', 'Admin\UnionController@addDetail');
	Route::any('/union-detail/edit/{id}', 'Admin\UnionController@editDetail')->where('id', '[0-9]+');
	Route::any('/union-detail/del/{id}', 'Admin\UnionController@delDetail')->where('id', '[0-9]+');

	// Admissions
	Route::any('/admissions', 'Admin\AdmissionsController@index');
	Route::any('/admissions/add', 'Admin\AdmissionsController@add');
	Route::any('/admissions/edit/{id}', 'Admin\AdmissionsController@edit')->where('id', '[0-9]+');
	Route::any('/admissions/del/{id}', 'Admin\AdmissionsController@del')->where('id', '[0-9]+');
	// Admissions Detail
	Route::any('/admissions-detail', 'Admin\AdmissionsController@detail');
	Route::any('/admissions-detail/add', 'Admin\AdmissionsController@addDetail');
	Route::any('/admissions-detail/edit/{id}', 'Admin\AdmissionsController@editDetail')->where('id', '[0-9]+');
	Route::any('/admissions-detail/del/{id}', 'Admin\AdmissionsController@delDetail')->where('id', '[0-9]+');

	// Gallery Image
	Route::any('/gallery-image/add', 'Admin\GalleryImageController@add');
	Route::any('/gallery-image/edit/{id}', 'Admin\GalleryImageController@edit')->where('id', '[0-9]+');
	Route::get('/gallery-image/del/{id}', 'Admin\GalleryImageController@del')->where('id', '[0-9]+');
	Route::any('/gallery-image', 'Admin\GalleryImageController@index');
	Route::any('/gallery-image/detail/{id}', 'Admin\GalleryImageController@detail')->where('id', '[0-9]+');
	Route::any('/gallery-image/upload/{id}', 'Admin\GalleryImageController@upload')->where('id', '[0-9]+');
	Route::any('/gallery-image/list/{id}', 'Admin\GalleryImageController@list')->where('id', '[0-9]+');
	// Gallery Video
	Route::any('/gallery-video/add', 'Admin\GalleryVideoController@add');
	Route::any('/gallery-video/edit/{id}', 'Admin\GalleryVideoController@edit')->where('id', '[0-9]+');
	Route::get('/gallery-video/del/{id}', 'Admin\GalleryVideoController@del')->where('id', '[0-9]+');
	Route::any('/gallery-video', 'Admin\GalleryVideoController@index');
	// notify
	Route::any('/notify/add', 'Admin\NotifyController@add');
	Route::any('/notify/edit/{id}', 'Admin\NotifyController@edit')->where('id', '[0-9]+');
	Route::get('/notify/del/{id}', 'Admin\NotifyController@del')->where('id', '[0-9]+');
	Route::any('/notify', 'Admin\NotifyController@index');
	// partner
	Route::any('/partner/add', 'Admin\PartnerController@add');
	Route::any('/partner/edit/{id}', 'Admin\PartnerController@edit')->where('id', '[0-9]+');
	Route::get('/partner/del/{id}', 'Admin\PartnerController@del')->where('id', '[0-9]+');
	Route::any('/partner', 'Admin\PartnerController@index');
	// relation
	Route::any('/relation/add', 'Admin\RelationController@add');
	Route::any('/relation/edit/{id}', 'Admin\RelationController@edit')->where('id', '[0-9]+');
	Route::get('/relation/del/{id}', 'Admin\RelationController@del')->where('id', '[0-9]+');
	Route::any('/relation', 'Admin\RelationController@index');

	Route::any('/contact', 'Admin\ContactController@index');
	Route::get('/contact/del/{id}', 'Admin\ContactController@del')->where('id', '[0-9]+');

	//user
	Route::any('/user/add', 'Admin\UserController@add');
	Route::any('/user/edit/{id}', 'Admin\UserController@edit')->where('id', '[0-9]+');
	Route::get('/user/del/{id}', 'Admin\UserController@del')->where('id', '[0-9]+');
	Route::any('/user', 'Admin\UserController@index');

});

Route::get('/', 'User\IndexController@index');

// News
Route::group(['prefix' => 'tin-tuc'], function () {
	Route::get('/', 'User\NewsController@index');
	Route::get('/{link}.html', 'User\NewsController@detail')->where('link', '.*');
});

Route::get('/tim-kiem', 'User\NewsController@search');


// Notify
Route::group(['prefix' => 'thong-bao'], function () {
	// Route::get('/', 'User\NewsController@index');
	Route::get('/{link}.html', 'User\NotifyController@detail')->where('link', '.*');
});

// Intro
Route::group(['prefix' => 'gioi-thieu'], function () {
	Route::get('/', 'User\IntroController@index');
	Route::get('/{id}/{link}.html', 'User\IntroController@list')->where('id', '[0-9]+')->where('link', '.*');
	Route::get('/{id}-{link}.html', 'User\IntroController@detail')->where('id', '[0-9]+')->where('link', '.*');
});

// Gallery Image
Route::group(['prefix' => 'thu-vien-anh'], function () {
	Route::get('/', 'User\GalleryImageController@index');
	Route::get('/{link}.html', 'User\GalleryImageController@detail')->where('link', '.*');
});

// Gallery Video
Route::group(['prefix' => 'thu-vien-video'], function () {
	Route::get('/', 'User\GalleryVideoController@index');
	Route::get('/{link}.html', 'User\GalleryVideoController@detail')->where('link', '.*');
});

// Sport
Route::group(['prefix' => 'bo-mon'], function () {
	Route::get('/', 'User\SportController@index');
	Route::get('/{id}/{link}.html', 'User\SportController@list')->where('id', '[0-9]+')->where('link', '.*');
	Route::get('/{id}-{link}.html', 'User\SportController@detail')->where('id', '[0-9]+')->where('link', '.*');
});

// Info
Route::group(['prefix' => 'thong-tin'], function () {
	Route::get('/', 'User\InfoController@index');
	Route::get('/{id}/{link}.html', 'User\InfoController@list')->where('id', '[0-9]+')->where('link', '.*');
	Route::get('/{id}-{link}.html', 'User\InfoController@detail')->where('id', '[0-9]+')->where('link', '.*');
});

// Union
Route::group(['prefix' => 'dang-doan-the'], function () {
	Route::get('/', 'User\UnionController@index');
	Route::get('/{id}/{link}.html', 'User\UnionController@list')->where('id', '[0-9]+')->where('link', '.*');
	Route::get('/{id}-{link}.html', 'User\UnionController@detail')->where('id', '[0-9]+')->where('link', '.*');
});

// Admissions
Route::group(['prefix' => 'chieu-sinh'], function () {
	Route::get('/', 'User\AdmissionsController@index');
	Route::get('/{id}/{link}.html', 'User\AdmissionsController@list')->where('id', '[0-9]+')->where('link', '.*');
	Route::get('/{id}-{link}.html', 'User\AdmissionsController@detail')->where('id', '[0-9]+')->where('link', '.*');
});

Route::any('/lien-he', 'User\ContactController@index');
Route::get('/thanks', 'User\ContactController@thanks');

Route::get('refresh_captcha', 'User\IndexController@refreshCaptcha')->name('refresh_captcha');

Route::get('/mail/send', 'MailController@send');