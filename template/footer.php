<footer>
    <div class="container">
        <div class="footer-widgets-part">
            <div class="row">
                <div class="col-md-4">
                    <div class="widget text-widget">
                        <h1>TRUNG TÂM THỂ DỤC THỂ THAO QUẬN 7</h1>
                        <p><i class="fa fa-map-marker"></i> &nbsp; Địa chỉ: 504 Huỳnh Tấn Phát, Phường Bình Thuận, Quận 7 <br>
                            <i class="fa fa-mobile"></i> &nbsp; Điện thoại: +84 28 3781 0381 <br>
                            <i class="fa fa-clock-o"></i> &nbsp; Mở cửa: 6AM–8PM  <br>
                            <i class="fa fa-envelope"></i> &nbsp; Email: thethaoquan7@gmail.com <br>
                            <i class="fa fa-facebook-square"></i> &nbsp; Facebook: https://www.facebook.com/thethaophunhuan
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="widget categories-widget">
                        <h1>Danh sách các môn</h1>
                        <ul class="category-list">
                            <li><a href="akido.php">Aikido</a></li>
                            <li><a href="#">Boxing</a></li>
                            <li><a href="#">Judo</a></li>
                            <li><a href="#">Karate</a></li>
                            <li><a href="#">Kickboxing</a></li>
                            <li><a href="#">Muay</a></li>
                            <li><a href="#">Pencak silat</a></li>
                            <li><a href="#">Taekwondo</a></li>
                            <li><a href="#">Võ cổ truyền</a></li>
                            <li><a href="#">Vovinam</a></li>
                            <li><a href="#">Wushu</a></li>
                            <li><a href="#">Bơi lội</a></li>
                            <li><a href="#">Bóng bàn</a></li>
                            <li><a href="#">Bóng chuyền</a></li>
                            <li><a href="#">Bóng Đá</a></li>
                            <li><a href="#">Bóng Rổ</a></li>
                            <li><a href="#">Cầu Lông</a></li>
                            <li><a href="#">Cầu mây</a></li>
                            <li><a href="#">Cờ tướng</a></li>
                            <li><a href="#">Cờ Vua</a></li>
                            <li><a href="#">Đẩy gậy</a></li>
                            <li><a href="#">Điền Kinh</a></li>
                            <li><a href="#">Kéo co</a></li>
                            <li><a href="#">Quần vợt</a></li>
                            <li><a href="#">TD nhịp điệu</a></li>
                            <li><a href="#">TD thẩm mỹ</a></li>
                            <li><a href="#">Xe đạp</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="widget flickr-widget">
                        <h1>Chia sẻ qua facebook</h1>
                       <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fntddnq7%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=229115950959968" width="100%" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="false" allow="encrypted-media"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-last-line">
            <div class="row">
                <div class="col-md-6">
                    <p>&copy; COPYRIGHT 2019 thethaoquan7.com.vn</p>
                </div>
                <div class="col-md-6">
                    <nav class="footer-nav">
                        <ul>
                            <li><a href="index.php">Trang chủ</a></li>
                            <li><a href="gioi-thieu.php">Giới thiệu</a></li>
                            <li><a href="lien-he.php">Liên hệ</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</footer>