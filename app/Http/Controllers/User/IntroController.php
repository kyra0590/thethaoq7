<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\User\BaseController;
use Illuminate\Http\Request;
use App\Models\Intro;
use App\Models\IntroDetail;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;

class IntroController extends BaseController
{
    public function index(){
        $setting = Setting::where("module", "intro")->first();
        if(empty($setting)) abort(404);

        $intro = DB::table('intro_detail')
            ->selectRaw("intro_detail.id, intro_detail.title, intro.title as intro_title,
                intro_detail.short_desc, intro_detail.content,
                intro_detail.keywords, intro_detail.is_showed,
                intro_detail.date_show, intro_detail.link,
                intro_detail.order, intro_detail.img,
                intro.link as introLink, intro_detail.id_intro,
                intro_detail.caption")
            ->join('intro', 'intro.id', '=', 'intro_detail.id_intro')
            ->where('intro.is_showed', 1)
            ->where('intro_detail.is_showed', 1)
            ->orderBy('intro_detail.order', 'asc')
            ->orderBy('intro_detail.id', 'desc')
            ->paginate(10)
            ->withPath("/gioi-thieu");

        return view("user/intro/index")
            ->with('setting', $setting)
            ->with('intro', $intro);
    }

    public function list($id = "", $link = "") {
    	if(!$link) {
    		abort(404);
    	}

    	$intro = Intro::where('is_showed', 1)
            ->where('id', $id)
            ->where('link', $link)->first();

        if(empty($intro)) abort(404);

        $introDetail = DB::table('intro_detail')
            ->selectRaw("intro_detail.id, intro_detail.title, intro.title as intro_title,
                intro_detail.short_desc, intro_detail.content,
                intro_detail.keywords, intro_detail.is_showed,
                intro_detail.date_show, intro_detail.link,
                intro_detail.order, intro_detail.img,
                intro.link as introLink, intro_detail.id_intro,
                intro_detail.caption")
            ->join('intro', 'intro.id', '=', 'intro_detail.id_intro')
            ->where('intro.id', $id)
            ->where('intro.is_showed', 1)
            ->where('intro_detail.is_showed', 1)
            ->orderBy('intro_detail.order', 'asc')
            ->orderBy('intro_detail.id', 'desc')
            ->paginate(10)
            ->withPath("/gioi-thieu");

    	return view("user/intro/list")
            ->with('intro', $intro)
            ->with('introDetail', $introDetail);
    }

     public function detail($id = "", $link = "") {
        if(!$id && !$link) {
            abort(404);
        }

        $intro = Intro::where('is_showed', 1)->where('id', $id)->first();

        if(empty($intro)) abort(404);

        $introDetail = IntroDetail::where('is_showed', 1)
            ->where('id_intro', $id)
            ->where('link', $link)
            ->first();

        if(empty($introDetail)) abort(404);

        $relationIntro =  IntroDetail::where('is_showed', 1)
            ->where('id_intro', $id)
            ->where('link', "!=", $link)
            ->limit(5)
            ->get();

        return view("user/intro/detail")
            ->with('intro', $intro)
            ->with('introDetail', $introDetail)
            ->with('relationIntro', $relationIntro);
    }
}
