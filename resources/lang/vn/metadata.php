<?php
return [
	'about_us'        => 'Về chúng tôi',
	'about_us_kw'     => 'keywords about us',
	'about_us_des'    => 'description about us',
	'message_ceo'     => 'Thông điệp CEO',
	'message_ceo_kw'  => 'kw mesasge ceo',
	'message_ceo_des' => 'des message ceo',
	'contact'         => 'Liên hệ',
	'contact_kw'      => 'kw Liên hệ',
	'contact_des'     => 'des Liên hệ',
	'search_news'     => 'Tìm kiếm tin',
	'search_news_kw'  => 'kw tìm kiếm tin',
	'search_news_des' => 'des tìm kiếm tin',
];
