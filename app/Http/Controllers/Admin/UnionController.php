<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use App\Models\Union;
use App\Models\UnionDetail;
use Illuminate\Support\Facades\DB;
use Image;

class UnionController extends BaseController
{
    public function index(Request $request){
        $params = $request->all();
        $isAjax = (isset($params['is_ajax'])) ? $params['is_ajax'] : false;
        $start = (!empty($params['start'])) ? $params['start'] : 0;
        $keyword = (!empty($params['keyword'])) ? $params['keyword'] : '';
        $limit =  (!empty($params['limit'])) ? $params['limit'] : 50;
        $union = Union::where('title', 'like', "%{$keyword}%")
            ->orderBy('order', 'asc')
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->offset($start)
            ->get();
        if($isAjax) {
            return response()->json(
                 [
                "recordsFiltered" => count($union),
                "recordsTotal" => count($union),
                "data" => $union
            ], 200

                );
        }
        return view('admin/union/index')
            ->with('title', "Danh sách Đảng - Đoàn thể");
    }

    public function add(Request $request){
        $params = $request->all();
        $title = "Thêm Đảng - Đoàn thể";
        $status = false;
        if(!empty($params)) {
            $params['title'] = trim($params['title']);
            $params['created_at'] = date("Y-m-d");
            $params['order'] = !empty($params['order']) ? (int) $params['order'] : 0;
            unset($params['_token']);
            $status = Union::insert($params);
        }
        return view('admin/union/add')
            ->with('title', $title)
            ->with('status', $status);
    }

     public function edit(Request $request, $id){
        $title = 'Sửa Đảng - Đoàn thể';
        $union = Union::find($id);
        $status = false;
        if(!$id || empty($union)) {
           return redirect('/admin/union');
        }
        $params = $request->all();
        if(!empty($params)) {
            $union->title        = trim($params['title']);
            $union->is_showed    = $params['is_showed'];
            $union->link         = $params['link'];
            $union->keywords     = $params['keywords'];
            $union->description  = $params['description'];
            $union->order        = !empty($params['order']) ? (int) $params['order'] : 0;
            $union->is_show_navi = !empty($params['is_show_navi']) ? (int) $params['is_show_navi'] : 0;
            $status              = $union->save();
        }
        return view('admin/union/edit')
            ->with('title', $title)
            ->with('union', $union)
            ->with('status', $status);
    }

    public function del($id){
         if($id){
            Union::where('id', $id)->delete();
            UnionDetail::where('id_union', $id)->update(['id_union' => 0, 'is_showed' => 0]);
        }
        return redirect("/admin/union");
    }

    public function detail(Request $request){
        $title   = "Danh sách tin tức Đảng - Đoàn thể";
        $params  = $request->all();
        $idUnion = (!empty($params['idUnion'])) ? trim($params['idUnion']) : '';
        $isAjax  = (isset($params['is_ajax'])) ? $params['is_ajax'] : false;
        $start   = (!empty($params['start'])) ? $params['start'] : 0;
        $keyword = (!empty($params['keyword'])) ? $params['keyword'] : '';
        $limit   =  (!empty($params['limit'])) ? $params['limit'] : 50;
        $unionDetail = DB::table('union_detail')
            ->selectRaw("union_detail.id, union_detail.title,
                union.title as union_title,
                union_detail.short_desc, union_detail.content,
                union_detail.keywords, union_detail.is_showed,
                union_detail.date_show, union_detail.link,
                union_detail.order, union_detail.img,
                union_detail.is_new")
            ->join('union', 'union.id', '=', 'union_detail.id_union')
            ->where('union_detail.title', 'like', "%{$keyword}%")
            ->orderBy('union_detail.order', 'asc')
            ->orderBy('union_detail.id', 'desc')
            ->limit($limit)
            ->offset($start);
        if($idUnion != "") {
            $unionDetail->where('union_detail.id_union', $idUnion);
            $union = Union::find($idUnion);
            if(!empty($union)) {
                $union = $union->first();
                $title .= " - ". $union->title;
            }
        }
        $unionDetail = $unionDetail->get();
        if($isAjax) {
            return response()->json(
            [
                "recordsFiltered" => count($unionDetail),
                "recordsTotal"    => count($unionDetail),
                "data"            => $unionDetail
            ], 200 );
        }
        return view('admin/union/detail')
            ->with("unionDetail", $unionDetail)
            ->with('idUnion', $idUnion)
            ->with('title', $title);
    }

     public function addDetail(Request $request){
        $title   = "Thêm tin Đảng - Đoàn thể";
        $params  = $request->all();
        $status  = false;
        $idUnion = '';
        $listUnion = Union::orderBy('order', 'ASC')->orderBy("id","DESC")->get()->keyBy("id")->toArray();
        if(!empty($params)) {
            $idUnion = (!empty($params['idUnion'])) ? trim($params['idUnion']) : '';
            if($idUnion != '') {
                $union = Union::find($idUnion)->first();
                $title .= " - ". $union->title;
            }
            if(!empty($params['_token'])) {
                $params['title'] = trim($params['title']);
                $params['caption'] = trim($params['caption']);
                $params['created_at'] = date("Y-m-d");
                $params['order'] = (!empty($params['order'])) ? (int) $params['order'] : 0;
                $params['is_new'] = (!empty($params['is_new'])) ? (int) $params['is_new'] : 0;
                $params['date_show'] = (!empty($params['date_show'])) ? $params['date_show'] : date("Y-m-d");
                unset($params['_token']);
                if($request->hasFile('img')) {
                    $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/union/";
                    $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                    if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                        $params['img'] = $fileName;

                        $imgThumb = Image::make($descriptionPath.$fileName);
                        $imgThumb->resize(375, 250, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($descriptionPath.'thumb/'.$fileName);
                    }
                }
                $status = UnionDetail::insert($params);
            }
        }
        return view('admin/union/add-detail')
            ->with('title', $title)
            ->with('idUnion', $idUnion)
            ->with('listUnion', $listUnion)
            ->with('status', $status);
    }

    public function editDetail(Request $request, $id) {
        $title       = 'Sửa tin Đảng - Đoàn thể';
        $params      = $request->all();
        $status      = false;
        $idUnion     = '';
        $listUnion   = Union::orderBy('order', 'ASC')->orderBy("id","DESC")->get()->keyBy("id")->toArray();
        $unionDetail = UnionDetail::find($id);
        if(!$id || empty($unionDetail)) {
            return redirect("/admin/union-detail?idUnion={$unionDetail->id_union}");
        }
        if(!empty($params)) {
            $idUnion = (!empty($params['idUnion'])) ? trim($params['idUnion']) : '';
            if($idUnion != '') {
                $union = Union::find($idUnion)->first();
                $title .= " - ". $union->title;
            }
            if(!empty($params['_token'])) {
                $unionDetail->title       = trim($params['title']);
                $unionDetail->caption     = trim($params['caption']);
                $unionDetail->id_union    = $params['id_union'];
                $unionDetail->is_showed   = $params['is_showed'];
                $unionDetail->link        = $params['link'];
                $unionDetail->keywords    = $params['keywords'];
                $unionDetail->description = $params['description'];
                $unionDetail->date_show   = $params['date_show'];
                $unionDetail->content     = $params['content'];
                $unionDetail->short_desc     = $params['short_desc'];
                $unionDetail->order       = !empty($params['order']) ? (int) $params['order'] : 0;
                $unionDetail->is_new       = !empty($params['is_new']) ? (int) $params['is_new'] : 0;
                $descriptionPath    = $_SERVER['DOCUMENT_ROOT']."/public/images/union/";
                if($request->hasFile('img')) {
                    $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                    if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                        if(!empty($unionDetail->img)) {
                            if(file_exists($descriptionPath.$unionDetail->img)) {
                                unlink($descriptionPath.$unionDetail->img);
                            }

                            if(file_exists($descriptionPath.'thumb/'.$unionDetail->img))
                            {
                                unlink($descriptionPath.'thumb/'.$unionDetail->img);
                            }
                        }

                        $imgThumb = Image::make($descriptionPath.$fileName);
                        $imgThumb->resize(375, 250, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($descriptionPath.'thumb/'.$fileName);

                        $unionDetail->img = $fileName;
                    }
                } else {
                    if( $params['remove_img'] && file_exists($descriptionPath.$unionDetail->img)) {
                        unlink($descriptionPath.$unionDetail->img);
                        if(file_exists($descriptionPath.'thumb/'.$unionDetail->img))
                        {
                            unlink($descriptionPath.'thumb/'.$unionDetail->img);
                        }
                        $unionDetail->img = "";
                    }
                }
                $status = $unionDetail->save();
            }
        }
        return view('admin/union/edit-detail')
            ->with('title', $title)
            ->with('unionDetail', $unionDetail)
            ->with('idUnion', $idUnion)
            ->with('listUnion', $listUnion)
            ->with('status', $status);
    }

    public function delDetail(Request $request, $id){
        $params = $request->all();
        $idUnion = (!empty($params['idUnion'])) ? trim($params['idUnion']) : '';
        if($id){
            $unionDetail = UnionDetail::find($id);
            $descriptionPath    = $_SERVER['DOCUMENT_ROOT']."/public/images/union/";
            if(!empty($unionDetail->img) && file_exists($descriptionPath.$unionDetail->img)) {
                unlink($descriptionPath.$unionDetail->img);
                if(file_exists($descriptionPath.'thumb/'.$unionDetail->img))
                {
                    unlink($descriptionPath.'/thumb/'.$unionDetail->img);
                }
            }
            $unionDetail->delete();
        }
        return redirect("/admin/union-detail?idUnion={$idUnion}");
    }
}
