@extends('user.layout')

@section('title', $info->title)
@section('description', $info->description)
@section('keywords', $info->keywords)

@section('content')
<div class="grid-box">
    <div class="title-section">
        <h1><span>{{$info->title}}</span></h1>
    </div>
    @if(!empty($infoDetail) && count($infoDetail))
        @foreach($infoDetail as $item)
        <div class="news-post article-post">
            <div class="row">
                <div class="col-sm-3">
                    <div class="post-gallery">
                        @if (!empty($item->img) && file_exists($imgPath."/info/thumb/".$item->img))
                            <img src="/public/images/info/thumb/{{$item->img}}"  alt="{{$item->caption}}" width="100%">
                        @else
                            <img src="/public/images/logo_thumb.jpg" style="width: auto !important; height: 150px">
                        @endif
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="post-content">
                        <h2><a href="/thong-tin/{{$item->id_info}}-{{$item->link}}.html">{{$item->title}}</a></h2>
                        <p>{{$item->short_desc}}</p>
                        <a href="/thong-tin/{{$item->id_info}}-{{$item->link}}.html" class="read-more-button"><i class="fa fa-arrow-circle-right"></i>Đọc thêm</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    @endif
</div>
 @if(!empty($infoDetail) && count($infoDetail))
<div class="pagination-box">
    <ul class="pagination-list">
        @if ($infoDetail->lastPage() == 1)
            <li><a class="active" href="#">1</a></li>
        @elseif ($infoDetail->lastPage() > 1)
            @if($infoDetail->currentPage() > 1)
                <li><a href="{{ $infoDetail->url($infoDetail->currentPage() - 1) }}">Back</a></li>
            @endif

             @for ($i = 1; $i <= $infoDetail->lastPage(); $i++)
                <li><a class="{{ ($infoDetail->currentPage() == $i) ? ' active' : '' }}" href="{{ $infoDetail->url($i) }}">{{$i}}</a></li>
            @endfor

            @if($infoDetail->currentPage() < $infoDetail->lastPage())
            <li><a href="{{ $infoDetail->url($infoDetail->currentPage()+1) }}">Next</a></li>
            @endif
        @endif
    </ul>

    <p>Page {{$infoDetail->currentPage()}} of {{$infoDetail->lastPage()}}</p>
</div>
@else
    <p>Hiện tại không tìm thấy dữ liệu</p>
@endif

@endsection