<?php
return [
  'about_us' => 'About us',
  'about_us_kw' => 'keywords about us',
  'about_us_des' => 'description about us',
  'message_ceo' => 'Message CEO',
  'message_ceo_kw' => 'kw mesasge ceo',
  'message_ceo_des' => 'des message ceo',
  'contact' => 'Contact',
  'contact_kw' => 'kw contact',
  'contact_des' => 'des contact',
  'search_news'     => 'Search news',
	'search_news_kw'  => 'kw search news',
	'search_news_des' => 'des search news',
];
