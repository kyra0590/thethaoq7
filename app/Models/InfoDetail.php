<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InfoDetail extends Model
{
	protected $table      = 'info_detail';
	protected $primaryKey = 'id';
	public $timestamps    = true;
}
