<!doctype html>
<html lang="en" class="no-js">

<head>
    <title>Thể Thao Quận 7</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <?php include('css.php') ?>

</head>

<body>
    <!-- Container -->
    <div id="container">
        <!-- Header
            ================================================== -->
        <?php include('header.php') ?>
        <!-- End Header -->
        <!-- block-wrapper-section
            ================================================== -->
        <section class="block-wrapper">
            <div class="container shadow-white">
                <div class="row">
                    <div class="col-sm-9">
                        <!-- block content -->
                        <div class="block-content">
                            <!-- contact info box -->
                            <div class="contact-info-box">
                                <div class="title-section">
                                    <h1><span>Liên hệ</span></h1>
                                </div>
                                <div id="map"></div>
                                <p>
                                    <i class="fa fa-map-marker"></i> &nbsp; Địa chỉ: 504-506 Huỳnh Tấn Phát, Bình Thuận, Quận 7, Hồ Chí Minh, Vietnam <br>
                                    <i class="fa fa-mobile"></i> &nbsp; Điện thoại: +84 28 3781 0381 <br>
                                    <i class="fa fa-clock-o"></i> &nbsp; Mở cửa: 6AM–8PM
                                </p>
                            </div>
                            <!-- End contact info box -->
                            <!-- contact form box -->
                            <div class="contact-form-box">
                                <div class="title-section">
                                    <h1><span>Gửi thông tin đến chúng tôi</span></h1>
                                </div>
                                <form id="contact-form">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="name">Họ tên*</label>
                                            <input id="name" name="name" type="text">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="mail">E-mail*</label>
                                            <input id="mail" name="mail" type="text">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="website">Số Điện Thoại</label>
                                            <input id="phone" name="phone" type="text">
                                        </div>
                                    </div>
                                    <label for="comment">Yêu cầu của bạn*</label>
                                    <textarea id="comment" name="comment"></textarea>
                                    <button type="submit" id="submit-contact">
                                        Gửi thông tin
                                    </button>
                                </form>
                            </div>
                            <!-- End contact form box -->
                        </div>
                        <!-- End block content -->
                    </div>
                    <div class="col-sm-3">
                        <!-- sidebar -->
                        <?php include('sidebar.php') ?>
                    </div>
                </div>
                <!-- End sidebar -->
            </div>
        </section>
    <!-- End block-wrapper-section -->
    <!-- footer
            ================================================== -->
    <?php include('footer.php') ?>
    <!-- End footer -->
    </div>
    <!-- End Container -->
    <?php include('script.php') ?>
</body>

</html>