<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UnionDetail extends Model
{
	protected $table      = 'union_detail';
	protected $primaryKey = 'id';
	public $timestamps    = true;
}
