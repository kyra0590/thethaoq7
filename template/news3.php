<!doctype html>
<html lang="en" class="no-js">

<head>
    <title>Thể Thao Quận 7</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <?php include('css.php') ?>
</head>

<body>
    <!-- Container -->
    <div id="container">
        <!-- Header
            ================================================== -->
        <?php include('header.php') ?>
        <!-- End Header -->
        <!-- block-wrapper-section
            ================================================== -->
        <section class="block-wrapper shadow-white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="block-content">
                            <!-- grid box -->
                            <div class="grid-box">
                                <div class="title-section">
                                    <h1><span>Chi tiết tin</span></h1>
                                </div>
                                <div class="single-post-box">
                                    <div class="title-post">
                                        <h1>TỔ CHỨC THÀNH CÔNG HỘI THAO NGÀNH GIÁO DỤC QUẬN 7</h1>
                                    </div>
                                    <div class="post-gallery">
                                        <img src="images/news3.jpg" alt="">
                                        <span class="image-caption">Ảnh: TỔ CHỨC THÀNH CÔNG HỘI THAO NGÀNH GIÁO DỤC QUẬN 7</span>
                                    </div>
                                    <div class="post-content">
                                        <p>Nhân dịp kỷ niệm 37 năm ngày Nhà giáo Việt Nam 20/11/1982 – 20/11/2019, Liên đoàn lao động Quận 7 phối hợp cùng Phòng giáo dục Quận 7, Trung tâm TDTT Quận 7 tổ chức hội thao ngành giáo dục cho cán bộ, giáo viên, nhân viên ngành giáo dục trên địa bàn Quận 7.</p>
                                        <p>Đến tham dự ngày Hội có sự hiện diện của Ông Ngô Xuân Đông – Trưởng phòng giáo dục Quận 7, Ông Nguyễn Văn Tuyền – UVTV Liên đoàn lao động Thành phố, Chủ tịch Liên đoàn lao động Quận 7, Ông Trần Văn Ly – Giám đốc trung tâm thể dục thể thao Quận 7 cùng hơn 1.500 cán bộ, công đoàn viên ngành giáo dục, các trường Mầm non, tiểu học, THCS trên địa bàn Quận 7. Đến tham dự ngày hội cán bộ và công đoàn viên ngành giáo dục được thi đấu các môn trò chơi dân gian tạo không khí sôi nổi, sảng khoái sau những tiết học.</p>
                                        <p style="text-align: right;"><b>TT. TDTT Q7.</b></p>
                                    </div>
                                    <!--  <div class="article-inpost">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="image-content">
                                                    <div class="image-place">
                                                        <img src="https://media1.nguoiduatin.vn/thumb_x600x600/media/ha-thi-linh/2019/12/10/u22-viet-nam-vo-dich-seagame-30-20.jpg" alt="">
                                                        <div class="hover-image">
                                                            <a class="zoom" href="https://media1.nguoiduatin.vn/thumb_x600x600/media/ha-thi-linh/2019/12/10/u22-viet-nam-vo-dich-seagame-30-20.jpg">
                                                                <i class="fa fa-arrows-alt"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <span class="image-caption">Cras eget sem nec dui volutpat ultrices.</span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="text-content">
                                                    <h2>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. </h2>
                                                    <p>Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscing risus a sem. Nullam quis massa sit amet nibh viverra malesuada. </p>
                                                    <p>Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu. Ut scelerisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. </p>
                                                    <p>Nunc iaculis mi in ante. Vivamus nibh feugiat est.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="col-md-12">
                                        <div class="title-section">
                                            <h1><span class="world">TIN LIÊN QUAN</span></h1>
                                        </div>
                                        <ul class="list-posts">
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» Bế mạc giải bóng đá HKPĐ khối THCS 6&7 năm 2019: quận 7 đăng quang ngôi vô địch</a></h2>
                                                </div>
                                            </li>
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» Gần 4.000 người tham gia cuộc đi bộ đồng hành vì người nghèo</a></h2>
                                                </div>
                                            </li>
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» ĐẠI HỘI ĐOÀN VIÊN CHI ĐOÀN TRUNG TÂM TDTT NHIỆM KỲ 2019 - 2022</a></h2>
                                                </div>
                                            </li>
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» Về Cao Lãnh - Đồng Tháp thăm mộ cụ Phó bảng Nguyễn Sinh Sắc</a></h2>
                                                </div>
                                            </li>
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» GIẢI KARATE HỘI KHỎE PHÙ ĐỔNG quận 7 NĂM HỌC 2019 - 2020</a></h2>
                                                </div>
                                            </li>
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» ĐỘI BÓNG U13 quận 7 vô địch Giải bóng đá thiếu niên U13 Yamaha Cup 2019</a></h2>
                                                </div>
                                            </li>
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» quận 7 KHAI MẠC HỘI KHỎE PHÙ ĐỔNG NĂM HỌC 2019 – 2020</a></h2>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <!-- sidebar -->
                        <?php include('sidebar.php') ?>
                        <!-- End sidebar -->
                    </div>
                </div>
            </div>
        </section>
        <!-- End block-wrapper-section -->
        <!-- footer
            ================================================== -->
        <?php include('footer.php') ?>
        <!-- End footer -->
    </div>
    <!-- End Container -->
    <?php include('script.php') ?>
</body>

</html>