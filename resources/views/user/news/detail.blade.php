@extends('user.layout')

@section('title', $news->title)
@section('description', $news->description)
@section('keywords', $news->keywords)

@section('content')
<div class="grid-box">
    <div class="single-post-box">
        <div class="title-post">
            <h1>{{$news->title}}</h1>
             <ul class="post-tags">
                <li><i class="fa fa-clock-o"></i>{{date("d M Y", strtotime($news->date_show))}}</li>
            </ul>
        </div>
        <div class="post-gallery">
            @if (!empty($news->img) && file_exists($imgPath."/news/".$news->img))
            <img src="/public/images/news/{{$news->img}}" alt="{{$news->caption}}">
            <span class="image-caption">{{$news->caption}}</span>
            @endif
        </div>
        <div class="post-content">
            {!! $news->content  !!}
        </div>
        @if(!empty($lastestNews) && count($lastestNews))
        <div class="col-md-12">
            <div class="title-section">
                <h4><span class="world">TIN NỔI BẬT</span></h4>
            </div>
            <ul class="list-posts">
                @foreach($lastestNews as $item)
                <li style="border:none">
                    <div class="post-content">
                        <h2><a href="/tin-tuc/{{$item->link}}.html">»» {{$item->title}}</a></h2>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>
@endsection