<!doctype html>
<html lang="en" class="no-js">

<head>
    <title>Thể Thao Quận 7</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,900,400italic" rel="stylesheet" type="text/css">
    <?php include('css.php') ?>
</head>

<body>
    <!-- Container -->
    <div id="container">
        <!-- Header
            ================================================== -->
        <?php include('header.php') ?>
        <!-- End Header -->
        <!-- block-wrapper-section
            ================================================== -->
        <section class="block-wrapper shadow-white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="block-content">
                            <!-- grid box -->
                            <div class="grid-box">
                                <div class="title-section">
                                    <h1><span>Chi tiết tin</span></h1>
                                </div>
                                <div class="single-post-box">
                                    <div class="title-post">
                                        <h1>QUẬN ỦY – HĐND – UBND QUẬN 7 KHEN THƯỞNG VẬN ĐỘNG VIÊN ĐẠT THÀNH TÍCH XUẤT SẮC TẠI SEAGAMES 30</h1>
                                    </div>
                                    <div class="post-gallery">
                                        <img src="images/news1.jpg" alt="">
                                        <span class="image-caption">Ảnh: QUẬN ỦY – HĐND – UBND QUẬN 7 KHEN THƯỞNG VẬN ĐỘNG VIÊN ĐẠT THÀNH TÍCH XUẤT SẮC TẠI SEAGAMES 30</span>
                                    </div>
                                    <div class="post-content">
                                        <blockquote>
                                            <p>Sáng 23/12, trong buổi chào cờ đầu tuần, UBND Quận 7 đã tổ chức khen thưởng lực lượng huấn luyện viên, vận động viên của Quận đã thi đấu đạt thành tích xuất sắc tại SEA Games 30 trong thành phần đoàn Thể thao Việt Nam.</p>
                                        </blockquote>
                                        <p>Đóng góp 7 VĐV trong thành phần đoàn Thể thao Việt Nam tham dự SEA Games 30 ở 4 bộ môn gồm Kickboxing, Taekwondo, Judo, Muay Thái và Đua thuyền, ngành Thể thao Quận 7 đã góp công mang về 2 HCV, 1 HCB và 4 HCĐ. Với những thành tích đó, UBND Quận đã trao tặng giấy khen và tiền thưởng cho các 3 huấn luyện viên và 7 vận động viên xuất sắc.</p>

                                        <p>Được khen thưởng đợt này là các vận động viên: Huỳnh Văn Tuấn (Huy chương vàng môn Kickboxing); Trần Hồ Duy (Huy chương vàng môn Taekwondo); Nguyễn Hoàng Dương (Huy chương bạc môn Judo); Huỳnh Hà Hữu Hiếu (Huy chương đồng môn Muay Thái); Trần Thị Lụa (Huy chương đồng môn Muay Thái); Phan Mạnh Linh (Huy chương đồng môn Đua thuyền); Nguyễn Mậu Trường (Huy chương đồng môn Đua thuyền). Ngoài ra, còn có 3 huấn luyện viên các bộ môn Taekwondo, Muay Thái và Đua thuyền được nhận giấy khen của UBND Quận 7.
                                        <p style="text-align: right;"><b>TT. TDTT Q7.</b></p>
                                    </div>
                                    <!--  <div class="article-inpost">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="image-content">
                                                    <div class="image-place">
                                                        <img src="https://media1.nguoiduatin.vn/thumb_x600x600/media/ha-thi-linh/2019/12/10/u22-viet-nam-vo-dich-seagame-30-20.jpg" alt="">
                                                        <div class="hover-image">
                                                            <a class="zoom" href="https://media1.nguoiduatin.vn/thumb_x600x600/media/ha-thi-linh/2019/12/10/u22-viet-nam-vo-dich-seagame-30-20.jpg">
                                                                <i class="fa fa-arrows-alt"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <span class="image-caption">Cras eget sem nec dui volutpat ultrices.</span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="text-content">
                                                    <h2>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. </h2>
                                                    <p>Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscing risus a sem. Nullam quis massa sit amet nibh viverra malesuada. </p>
                                                    <p>Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu. Ut scelerisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. </p>
                                                    <p>Nunc iaculis mi in ante. Vivamus nibh feugiat est.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="col-md-12">
                                        <div class="title-section">
                                            <h1><span class="world">TIN NỔI BẬT</span></h1>
                                        </div>
                                        <ul class="list-posts">
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» Bế mạc giải bóng đá HKPĐ khối THCS 6&7 năm 2019: quận 7 đăng quang ngôi vô địch</a></h2>
                                                </div>
                                            </li>
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» Gần 4.000 người tham gia cuộc đi bộ đồng hành vì người nghèo</a></h2>
                                                </div>
                                            </li>
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» ĐẠI HỘI ĐOÀN VIÊN CHI ĐOÀN TRUNG TÂM TDTT NHIỆM KỲ 2019 - 2022</a></h2>
                                                </div>
                                            </li>
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» Về Cao Lãnh - Đồng Tháp thăm mộ cụ Phó bảng Nguyễn Sinh Sắc</a></h2>
                                                </div>
                                            </li>
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» GIẢI KARATE HỘI KHỎE PHÙ ĐỔNG quận 7 NĂM HỌC 2019 - 2020</a></h2>
                                                </div>
                                            </li>
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» ĐỘI BÓNG U13 quận 7 vô địch Giải bóng đá thiếu niên U13 Yamaha Cup 2019</a></h2>
                                                </div>
                                            </li>
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» quận 7 KHAI MẠC HỘI KHỎE PHÙ ĐỔNG NĂM HỌC 2019 – 2020</a></h2>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <!-- sidebar -->
                        <?php include('sidebar.php') ?>
                        <!-- End sidebar -->
                    </div>
                </div>
            </div>
        </section>
        <!-- End block-wrapper-section -->
        <!-- footer
            ================================================== -->
        <?php include('footer.php') ?>
        <!-- End footer -->
    </div>
    <!-- End Container -->
    <?php include('script.php') ?>
</body>

</html>