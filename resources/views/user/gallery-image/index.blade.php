@extends('user.layout')

@section('title', $setting->title)
@section('description', $setting->description)
@section('keywords', $setting->keywords)

@section('content')
<div class="grid-box">
    <div class="title-section">
        <h1><span>Thư viện ảnh</span></h1>
    </div>
    @if(!empty($gallery) && count($gallery))
        @foreach($gallery as $item)
        <div class="gallery-post article-post">
            <div class="row">
                <div class="col-sm-3">
                    <div class="post-gallery">
                        @if (!empty($item->img) && file_exists($imgPath."/gallery/image/".$item->img))
                            <img src="/public/images/gallery/image/{{$item->img}}"  alt="{{$item->title}}" width="100%">
                        @else 
                            <img src="/public/images/logo_thumb.jpg" style="width: auto !important; height: 150px">
                        @endif
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="post-content">
                        <h2><a href="/thu-vien-anh/{{$item->link}}.html">{{$item->title}}</a></h2>
                         <p>{{$item->short_desc}}</p>
                        <a href="/thu-vien-anh/{{$item->link}}.html" class="read-more-button"><i class="fa fa-arrow-circle-right"></i>Xem thêm</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    @endif
</div>
@if(!empty($gallery) && count($gallery))
<div class="pagination-box">
    <ul class="pagination-list">
        @if ($gallery->lastPage() == 1)
            <li><a class="active" href="#">1</a></li>
        @elseif ($gallery->lastPage() > 1)
            @if($gallery->currentPage() > 1)
                <li><a href="{{ $gallery->url($gallery->currentPage() - 1) }}">Back</a></li>
            @endif

             @for ($i = 1; $i <= $gallery->lastPage(); $i++)
                <li><a class="{{ ($gallery->currentPage() == $i) ? ' active' : '' }}" href="{{ $gallery->url($i) }}">{{$i}}</a></li>
            @endfor

            @if($gallery->currentPage() < $gallery->lastPage())
            <li><a href="{{ $gallery->url($gallery->currentPage()+1) }}">Next</a></li>
            @endif
        @endif
    </ul>

    <p>Page {{$gallery->currentPage()}} of {{$gallery->lastPage()}}</p>
</div>
@else
    <p>Hiện tại không tìm thấy dữ liệu</p>
@endif
@endsection