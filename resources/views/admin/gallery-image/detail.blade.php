@extends('admin.layout')
@section('title', $title)
@section('content')

<div class="row page-titles">
    <div class="col-md-12 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <button type="button" class="btn btn-info d-none d-lg-block m-l-15" onclick="javascript:location.href='/admin/gallery-image'"><i class="fa fa-undo"></i> Trở lại</button>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-success">
                <h4 class="m-b-0 text-white">{{$title}}</h4>
            </div>
            <div class="card-body">
                <div class="form-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Dropzone</h4>
                                    <form class="dropzone" id="form-gallery">
                                        <div class="fallback">
                                            <input name="file" type="file" multiple />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row el-element-overlay" id="gallery">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
<?php if($status) { ?>
 $.toast({
    heading: "Thông báo",
    text: "Tạo thành công",
    position: 'bottom-right',
    loaderBg:'#ff6849',
    icon: 'success',
    hideAfter: 3500,
    stack: 6
});
<?php } ?>
reloadGallery();
$("#form-gallery").dropzone({
    url: "/admin/gallery-image/upload/{{$id}}",
    acceptedFiles: ".jpeg,.jpg,.png,.gif",
    queuecomplete: function(){
        reloadGallery();
    }
});
function reloadGallery(){
    $.ajax({
        url: '/admin/gallery-image/list/{{$id}}',
        type: 'GET',
    })
    .done(function(data) {
        var gallery = $("#gallery");
        gallery.empty();
        if(data.images && data.images.length >0) {
            data.images.forEach(function(item){
                var image = '<div class="col-lg-3 col-md-6">\
                    <div class="card">\
                        <div class="el-card-item">\
                            <div class="el-card-avatar el-overlay-1"> <img width="100%" src="/public/images/gallery/image/{{$id}}/'+item+'" alt="user" />\
                                <div class="el-overlay">\
                                    <ul class="list-style-none el-info">\
                                        <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="/public/images/gallery/image/{{$id}}/'+item+'"><i class="fa fa-search"></i></a></li>\
                                            <li> <a class="btn default btn-outline el-link del-file" href="#" rel="images/gallery/image/{{$id}}/'+item+'" ><i class="fa fa-times"></i></a>\
                                        </li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>';
               $("#gallery").append(image);
               $('.image-popup-vertical-fit').magnificPopup({
                    type: 'image',
                    closeOnContentClick: true,
                    mainClass: 'mfp-img-mobile',
                    image: {
                        verticalFit: true
                    }

                });
            });
        }
    })
    .fail(function() {
        console.log("Chưa có hình ảnh");
    });
}

$(document).on('click', '.del-file', function(event) {
    event.preventDefault();
    dir = $(this).prop("rel");
    $.ajax({
        url: '/admin/del-file',
        type: 'GET',
        data: {dir: dir},
    })
    .done(function() {
        reloadGallery();
    })
    .fail(function() {
        console.log("Lỗi ko xóa file thành công!!");
    });

});
</script>
@endsection


