<?php $__env->startSection('title', $title); ?>
<?php $__env->startSection('description', $description); ?>
<?php $__env->startSection('keywords', $keywords); ?>
<?php $__env->startSection('content'); ?>

<?php if(!empty($newsHome)): ?>
<div id="hot-news" class="grid-box">
    <div class="title-section">
        <h1><span>Tin nổi bật</span></h1>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="news-post image-post2">
                <div class="news-post standard-post2">
                    <div class="post-gallery">
                        <?php if(!empty($newsHome[0]->img) && file_exists($imgPath."/news/thumb/".$newsHome[0]->img)): ?>
                            <img src="/public/images/news/thumb/<?php echo e($newsHome[0]->img); ?>" alt="<?php echo e($newsHome[0]->title); ?>">
                        <?php endif; ?>
                    </div>
                    <div class="post-title">
                        <h2><a href="/tin-tuc/<?php echo e($newsHome[0]->link); ?>.html"><?php echo e($newsHome[0]->title); ?></a></h2>
                        <ul class="post-tags">
                            <li><?php echo e($newsHome[0]->short_desc); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <ul class="list-posts">
                <?php $__currentLoopData = $newsHome; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($key > 0): ?>
                    <li>
                        <div class="post-content">
                            <h2><a href="/tin-tuc/<?php echo e($item->link); ?>.html"><?php echo e($item->title); ?></a></h2>
                        </div>
                        <div class="clearfix">
                            <?php if(!empty($newsHome[0]->img) && file_exists($imgPath."/news/thumb/".$newsHome[0]->img)): ?>
                                <img src="/public/images/news/thumb/<?php echo e($item->img); ?>" alt="<?php echo e($item->title); ?>">
                            <?php endif; ?>
                            <p><?php echo e($item->short_desc); ?></p>
                        </div>
                    </li>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    </div>
</div>
<?php endif; ?>

<?php if(!empty($home->img_1) && file_exists($imgPath."/home/".$home->img_1)): ?>
<div class="advertisement">
    <img src="/public/images/home/<?php echo e($home->img_1); ?>" alt="banner advertisement" width="100%" style="max-height: 150px">
</div>
<?php endif; ?>

<div class="grid-box">
    <div class="row">
        <div class="col-md-6">
            <?php if(!empty($sportHome) && count($sportHome)): ?>
            <div class="title-section">
                <h1><span class="fashion">Các bộ môn</span></h1>
            </div>
            <div class="item news-post standard-post">
                <div class="post-gallery">
                    <?php if(!empty($sportHome[0]->img) && file_exists($imgPath."/sport-detail/thumb/".$sportHome[0]->img)): ?>
                        <img src="/public/images/sport-detail/thumb/<?php echo e($sportHome[0]->img); ?>" alt="<?php echo e($sportHome[0]->title); ?>">
                    <?php endif; ?>
                </div>
                <div class="post-content">
                    <h2><a href="/bo-mon/<?php echo e($sportHome[0]->id_sport); ?>-<?php echo e($sportHome[0]->link); ?>.html"><?php echo e($sportHome[0]->title); ?></a></h2>
                    <p><?php echo e($sportHome[0]->short_desc); ?></p>
                </div>
            </div>
            <div class="item">
                <ul class="list-posts">
                    <?php $__currentLoopData = $sportHome; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($key > 0): ?>
                    <li>
                        <div class="post-content">
                            <h2><a href="/bo-mon/<?php echo e($item->id_sport); ?>-<?php echo e($item->link); ?>.html"><?php echo e($item->title); ?></a></h2>
                        </div>
                    </li>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
            <?php endif; ?>
        </div>
        <div class="col-md-6">
            <?php if(!empty($infoHome) && count($infoHome)): ?>
            <div class="title-section">
                <h1><span class="world">Thông tin</span></h1>
            </div>
            <div class="item news-post standard-post">
                <div class="post-gallery">
                    <?php if(!empty($infoHome[0]->img) && file_exists($imgPath."/info/thumb/".$infoHome[0]->img)): ?>
                    <img src="/public/images/info/thumb/<?php echo e($infoHome[0]->img); ?>" alt="<?php echo e($infoHome[0]->title); ?>">
                    <?php endif; ?>
                </div>
                <div class="post-content">
                    <h2><a href="/thong-tin/<?php echo e($infoHome[0]->id_info); ?>-<?php echo e($infoHome[0]->link); ?>.html"><?php echo e($infoHome[0]->title); ?></a></h2>
                    <p><?php echo e($infoHome[0]->short_desc); ?></p>
                </div>
            </div>
            <div class="item">
                <ul class="list-posts">
                    <?php $__currentLoopData = $infoHome; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($key > 0): ?>
                        <li>
                            <div class="post-content">
                                <h2><a href="/thong-tin/<?php echo e($item->id_info); ?>-<?php echo e($item->link); ?>.html"><?php echo e($item->title); ?></a></h2>
                            </div>
                        </li>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php if(!empty($home->img_2) && file_exists($imgPath."/home/".$home->img_2)): ?>
<div class="advertisement">
    <img src="/public/images/home/<?php echo e($home->img_2); ?>" alt="banner advertisement" width="100%" style="max-height: 150px">
</div>
<?php endif; ?>

<div class="grid-box">
    <div class="row">
        <div class="col-md-6">
            <?php if(!empty($unionHome) && count($unionHome)): ?>
            <div class="title-section">
                <h1><span class="fashion">Đảng - đoàn thể</span></h1>
            </div>
            <div class="item news-post standard-post">
                <div class="post-gallery">
                    <?php if(!empty($unionHome[0]->img) && file_exists($imgPath."/union/thumb/".$unionHome[0]->img)): ?>
                        <img src="/public/images/union/thumb/<?php echo e($unionHome[0]->img); ?>" alt="<?php echo e($unionHome[0]->title); ?>">
                    <?php endif; ?>
                </div>
                <div class="post-content">
                    <h2><a href="/dang-doan-the/<?php echo e($unionHome[0]->id_union); ?>-<?php echo e($unionHome[0]->link); ?>.html"><?php echo e($unionHome[0]->title); ?></a></h2>
                    <p><?php echo e($unionHome[0]->short_desc); ?></p>
                </div>
            </div>
            <div class="item">
                <ul class="list-posts">
                    <?php $__currentLoopData = $unionHome; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($key > 0): ?>
                    <li>
                        <div class="post-content">
                            <h2><a href="/dang-doan-the/<?php echo e($item->id_union); ?>-<?php echo e($item->link); ?>.html"><?php echo e($item->title); ?></a></h2>
                        </div>
                    </li>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
            <?php endif; ?>
        </div>
        <div class="col-md-6">
            <?php if(!empty($admissionsHome) && count($admissionsHome)): ?>
            <div class="title-section">
                <h1><span class="world">Chiêu sinh</span></h1>
            </div>
            <div class="item news-post standard-post">
                <div class="post-gallery">
                    <?php if(!empty($admissionsHome[0]->img) && file_exists($imgPath."/admissions/thumb/".$admissionsHome[0]->img)): ?>
                        <img src="/public/images/admissions/thumb/<?php echo e($admissionsHome[0]->img); ?>" alt="<?php echo e($admissionsHome[0]->title); ?>">
                    <?php endif; ?>
                </div>
                <div class="post-content">
                    <h2><a href="/chieu-sinh/<?php echo e($admissionsHome[0]->id_admissions); ?>-<?php echo e($admissionsHome[0]->link); ?>.html"><?php echo e($admissionsHome[0]->title); ?></a></h2>
                    <p><?php echo e($admissionsHome[0]->short_desc); ?></p>
                </div>
            </div>
            <div class="item">
                <ul class="list-posts">
                    <?php $__currentLoopData = $admissionsHome; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($key > 0): ?>
                    <li>
                        <div class="post-content">
                            <h2><a href="/chieu-sinh/<?php echo e($item->id_admissions); ?>-<?php echo e($item->link); ?>.html"><?php echo e($item->title); ?></a></h2>
                        </div>
                    </li>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php if(!empty($home->img_3) && file_exists($imgPath."/home/".$home->img_3)): ?>
<div class="advertisement">
    <img src="/public/images/home/<?php echo e($home->img_3); ?>" alt="banner advertisement" width="100%" style="max-height: 150px">
</div>
<?php endif; ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('user.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>