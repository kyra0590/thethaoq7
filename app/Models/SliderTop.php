<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SliderTop extends Model
{
	protected $table      = 'slider_top';
	protected $primaryKey = 'id';
	public $timestamps    = true;
}
