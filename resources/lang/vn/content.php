<?php

return [
    'clients' => 'Khách hàng',
    'projects' => 'Dự án',
    'countries' => 'Quốc gia',
    'mission' => 'Sứ mệnh',
    'mission_message' => 'Chuyển đổi doanh nghiệp phát triển tự nhiên thành doanh nghiệp phát triển vĩ đại thông qua đầu tư chiến lược, thiết kế mô hình và tổ chức điều hành.',
    'contact_us' => 'Liên hệ',
    'vision' => 'Tầm nhìn',
    'vision_message' => 'Trở thành một tổ chức thịnh vượng cho khách hàng, nhân viên, cổ đông và các bên liên quan khác trên phạm vi toàn cầu.',
    'core_value' => 'Giá trị cốt lõi',
    'core_value_message' => 'Sáng tạo- Minh Bạch- Quyết liệt tới Cùng. <br><br>
    <div class="hc-component hc-cmp-steps box-steps">
        <div class="step-item">
            <span>1</span>
            <div class="content"><h3>Sáng tạo</h3>
            <p>Thể Thao Quận 7 chấp nhận những phản biện tích cực kể cả đối nghịch nhằm tạo kết quả cho khách hàng. Thể Thao Quận 7 luôn tìm kiếm các giải pháp mới và hữu ích cho hoạt động của từng người, từng bộ phận và công ty. Thể Thao Quận 7 khuyến khích hai hình thức sáng tạo gồm :
            <ul>
                <li>Sáng tạo theo kinh nghiệm: Là hình thức sáng tạo có tính mới và hữu ích ở cấp độ bộ phận.</li>
                <li>Sáng tạo đột phá: Là hình thức sáng tạo có tính mới và hữu ích ở cấp độ công ty trở lên.</li>
            </ul>
            </p></div>
        </div>
         <div class="step-item">
            <span>2</span>
            <div class="content"><h3>Minh Bạch</h3>
            <p>Thể Thao Quận 7 luôn rõ ràng và cụ thể trong mọi hoạt động của mình. Thể Thao Quận 7 ghi nhận hai hình thức minh bạch gồm:
            <ul>
                <li>Minh Bạch nội bộ: Là minh bạch với đồng nghiệp, cấp trên và cấp dưới.</li>
                <li>Minh bạch bên ngoài: Là minh bạch với khách hàng, nhà cung cấp và các đối tác khác.</li>
            </ul>
            </p></div>
        </div>
         <div class="step-item">
            <span>3</span>
            <div class="content"><h3>Quyết liệt đến cùng</h3>
            <p>Thể Thao Quận 7 sẽ không bao giờ bỏ cuộc khi chưa đạt mục tiêu đề ra. Thể Thao Quận 7  sẵn sàng chấp nhận thay đổi phương pháp thực thi nếu kết quả phản ánh sự không phù hợp của phương pháp. Thể Thao Quận 7 hành động nhanh chóng để đạt kết quả, phát hiện kịp thời những sai sót và làm lại để có kết quả mong muốn. </p></div>
        </div>
    </div>',
    'message_ceo' => 'Thông điệp CEO',
    'message_ceo_content' => 'Chúng tôi xin được bắt đầu bằng lời cám ơn chân thành và sâu sắc đến các khách hàng đã, đang và sẽ lựa chọn Thể Thao Quận 7 trong tương lai. Chúng tôi hiểu rất rõ rằng chúng tôi trong quá khứ là một doanh nghiệp đến từ một quốc gia chưa phải là nền kinh tế thị trường theo nghĩa đầy đủ của nó, nền văn hóa kinh doanh chưa thật sự chuyên nghiệp. Tuy nhiên, hiện tại chúng tôi đang sở hữu một đội ngũ nhân viên trẻ trung, đầy nhiệt huyết và tài năng. Bên cạnh đó, chúng tôi cũng hiểu rõ rằng doanh nhân phải là những người có ước mơ đủ lớn và quyết tâm cao độ để biến ước mơ thành sự thật dù thực tại chúng đang rất nhỏ, bằng chứng là đã có rất nhiều câu chuyện thành công từ gara ô tô đã tạo ra sự thay đổi toàn cầu.<br><br>
        Do vậy:  Xin trân trọng giới thiệu đến quý vị một công ty còn non trẻ nhưng có ước mơ thay đổi toàn cầu thông qua một tư duy Kỳ diệu là giúp khách hàng thực hiện được ước mơ thông qua  việc trải nghiệm dịch vụ và sản phẩm của chúng tôi.<br>
        <ul>
        <li>Chúng tôi không đơn thuần là tư vấn mà là cung cấp giải pháp đến khách hàng lựa chọn dịch vụ và sản phẩm giúp Quý vị thỏa mãn sau nhiều năm chưa có công cụ quản trị bài bản.</li>
        <li>Chúng tôi mong muốn gặp được những người bạn đồng hành, cùng thay đổi bản thân từ bên trong doanh nghiệp và thực hiện ước mơ vĩ đại thông qua trải nghiệm thực tế.</li>
        <li>Chúng tôi cung cấp cho bạn một giải pháp giá trị tạo ra sự khác biệt từ bên trong giúp cho bạn trở nên thay đổi toàn diện và thỏa mãn ước mơ của bạn. Đó gọi là Thể Thao Quận 7!</li>
        </ul>
        Cuối cùng, xin chúc quý vị thực hiện thành công quá trình thay đổi toàn diện để trở thành công ty toàn cầu, thỏa mãn ước mơ xây dựng doanh nghiệp khởi nghiệp thời đại.<br>
        <br>
        Trân trọng! ',
    'about_us_title_1' => 'THÔNG TIN CHUNG',
    'abount_us_content_1' => "
        Thành lập tại Việt Nam: Tháng 12 năm 2014 <br>
        Mô hình kinh doanh quản trị <br>
        Nhân viên: 150 người (toàn group) - 15 người (Business Producer) <br>
        Hình thức công ty: Cổ phần OTC <br>
        Số lượng dự án: 45 <br>
        Số lượng công ty theo mô hình: 40 <br>
        Thành lập tại Singapore: Tháng 09 năm 2019",

    'contact_title_2' => 'Thông tin liên hệ',
    'contact_subtitle_2' => 'Văn phòng Thể Thao Quận 7',
    'contact_title_1' => 'Gửi yêu cầu đến chúng tôi',
    'contact_policy' => 'Bạn chấp nhận điều kiện hệ thống và chính sách riêng tư',
    'working_process' => 'Quy trình làm việc',
    'working_process_title_1' => 'Khảo sát và thiết kế',
    'working_process_title_2' => 'Quyết định đầu tư',
    'working_process_title_3' => 'Thực hiện dự án',
    'working_process_content_1' => 'Đánh giá hiện trạng doanh nghiệp đang hoạt động, tập trung vào hiện trạng của: <br>
    -   Dòng tiền <br> -   Dòng sản phẩm <br> -   Dòng nhân sự<br> Xây dựng chiến lược đầu tư ',
    'working_process_content_2' => 'Thống nhất kế hoạch đầu tư và các nội dung liên quan',
    'working_process_content_3' => 'Đóng góp các nguồn lực để triển khai dự án theo kế hoạch: <br>
    - Vốn <br>
    - Nhân sự <br>
    - Sở hữu trí tuệ',
    'field' => 'Lĩnh vực',
    'field_title_1' => 'Đầu tư',
    'field_title_2' => 'Quản lý công ty',
    'field_title_3' => 'Tư vấn quản lý',
    'field_content_1' => '
        - Đầu tư công ty tư nhân <br>
        - Quỹ tăng trưởng',
    'field_content_2' => '- Công ty con <br>
        - Nhóm các công ty',
    'field_content_3' => '- Tư vấn quản lý <br>
        - Tư vấn M&A <br>
        - Hỗ trợ mở rộng thị trường ra Châu Á <br>
        - Đào tạo nhân lực cao cấp',


];
