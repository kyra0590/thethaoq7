<!doctype html>
<html lang="en" class="no-js">

<head>
    <title>Thể Thao Quận 7</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php include('css.php') ?>
</head>

<body>
    <!-- Container -->
    <div id="container">
        <!-- Header
            ================================================== -->
       <?php include('header.php') ?>
        <!-- End Header -->
        <!-- block-wrapper-section
            ================================================== -->
        <section class="block-wrapper">
            <div class="container shadow-white">
                <div class="row">
                    <div class="col-sm-9">
                        <!-- block content -->
                        <div class="block-content">
                            <!-- grid box -->
                            <div id="hot-news" class="grid-box">
                                <div class="title-section">
                                    <h1><span>Tin nổi bật</span></h1>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="news-post image-post2">
                                            <div class="news-post standard-post2">
                                                <div class="post-gallery">
                                                    <img src="images/news1.jpg" alt="">
                                                </div>
                                                <div class="post-title">
                                                    <h2><a href="news1.php">QUẬN ỦY – HĐND – UBND QUẬN 7 KHEN THƯỞNG VẬN ĐỘNG VIÊN ĐẠT THÀNH TÍCH XUẤT SẮC TẠI SEAGAMES</a></h2>
                                                    <ul class="post-tags">
                                                        <li>Sáng 23/12, trong buổi chào cờ đầu tuần, UBND Quận 7 đã tổ chức khen thưởng lực lượng huấn luyện viên, vận động viên của Quận đã thi đấu đạt thành tích xuất sắc tại SEA Games 30 trong thành phần đoàn Thể thao Việt Nam.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <ul class="list-posts">
                                            <li>
                                                <div class="post-content">
                                                    <h2><a href="news2.php">KHAI MẠC GIẢI VÕ CỔ TRUYỀN HỘI KHỎE PHÙ ĐỒNG THÀNH PHỐ HỒ CHÍ MINH NĂM HỌC 2019 – 2020</a></h2>
                                                </div>
                                                <div class="clearfix">
                                                <img src="images/news2.jpg" alt="">
                                                <p>Sáng 24/11/2019, Trung tâm thể dục thể thao Quận 7 đăng cai tổ chức giải Võ Cổ Truyền Hội khỏe Phù Đổng...</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="post-content">
                                                    <h2><a href="news3.php">TỔ CHỨC THÀNH CÔNG HỘI THAO NGÀNH GIÁO DỤC QUẬN 7</a></h2>
                                                </div>
                                                <div class="clearfix">
                                                <img src="images/news3.jpg" alt="">
                                                <p>Nhân dịp kỷ niệm 37 năm ngày Nhà giáo Việt Nam 20/11/1982 – 20/11/2019, Liên đoàn lao động Quận 7...</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="post-content">
                                                    <h2><a href="#">Về Cao Lãnh - Đồng Tháp thăm mộ cụ Phó bảng Nguyễn Sinh Sắc</a></h2>
                                                </div>
                                                <div class="clearfix">
                                                <img src="http://thethaophunhuan.com.vn/upload/news/68198511.jpg" alt="">
                                                <p>Chào mừng Kỷ niệm 50 năm thực hiện di chúc của Chủ tịch Hồ Chí Minh, 89 năm ngày thành lập Hội LHPN Việt Nam và 9 năm ngày Phụ nữ...</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="post-content">
                                                    <h2><a href="#">GIẢI KARATE HỘI KHỎE PHÙ ĐỔNG quận 7 NĂM HỌC 2019 - 2020</a></h2>
                                                </div>
                                                <div class="clearfix">
                                                <img src="http://thethaophunhuan.com.vn/upload/news/21000702.jpg" alt="">
                                                <p>Nhằm tuyển chọn vận động viên chuẩn bị tham dự Hội khỏe Phù đổng thành phố Hồ Chí Minh năm học 2019 – 2020. Chiều ngày...</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- End grid box -->
                            <!-- google addsense -->
                            <div class="advertisement">
                                <div class="desktop-advert">
                                    <img src="http://thethaophunhuan.com.vn/upload/hinhanh/644.png" width="100%" alt="">
                                </div>
                                <div class="tablet-advert">
                                    <img src="http://thethaophunhuan.com.vn/upload/hinhanh/644.png" width="100%" alt="">
                                </div>
                                <div class="mobile-advert">
                                    <img src="http://thethaophunhuan.com.vn/upload/hinhanh/644.png" width="100%" alt="">
                                </div>
                            </div>
                            <!-- End google addsense -->
                            <!-- carousel box -->
                            <div class="grid-box">
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="title-section">
                                            <h1><span class="fashion">Các bộ môn</span></h1>
                                        </div>
                                        <div class="item news-post standard-post">
                                            <div class="post-gallery">
                                                <img src="http://thethaophunhuan.com.vn/upload/news/48455540.jpg" alt="">
                                            </div>
                                            <div class="post-content">
                                                <h2><a href="#">Giải bóng đá mini cúp Doanh nghiệp quận 7 chào mừng ngày Doanh nhân Việt Nam 13/10 năm 2019</a></h2>
                                                <p>Sau gần ba tuần tranh tài sôi nổi, Giải bóng đá mini cúp Doanh nghiệp quận 7 chào mừng ngày Doanh nhân Việt Nam đã chính thức khép lại vào tối ngày 03/10/2019 tại câu lạc bộ Bóng đá quận 7.</p>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <ul class="list-posts">
                                                <li>
                                                    <div class="post-content">
                                                        <h2><a href="#">Giải Quần vợt kỷ niệm ngày Doanh nhân Việt Nam 13/10</a></h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="post-content">
                                                        <h2><a href="#">Đội bơi trung cao tuổi hạng nhất toàn đoàn giải Thành phố năm 2019</a></h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="post-content">
                                                        <h2><a href="#">Đội bóng đá tp. hcm (q. quận 7) vô địch vòng loại hội khỏe phù đổng học sinh tiểu học</a></h2>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="title-section">
                                            <h1><span class="world">Thông tin</span></h1>
                                        </div>
                                         <div class="item news-post standard-post">
                                            <div class="post-gallery">
                                                <img src="http://thethaophunhuan.com.vn/upload/news/51255451.jpg" alt="">
                                            </div>
                                            <div class="post-content">
                                                <h2><a href="#">Hội thi nấu ăn “Bữa tiệc nhỏ - Hạnh phúc lớn”</a></h2>
                                                <p>Vào sáng ngày 24/6/2019 tại Câu lạc bộ TDTT Rạch Miễu, Công đoàn Cơ sở Trung tâm Thể dục Thể thao quận 7 tổ chức hội thi nấu ăn chủ đề “Bữa tiệc nhỏ - Hạnh phúc lớn”. Tham dự Hội thi có 08 bếp ăn đến từ các tổ công đoàn: Văn phòng Trung tâm, Hồ bơi Rạch Miễu, nhà thi đấu Rạch Miễu, câu lạc bộ...</p>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <ul class="list-posts">
                                                <li>
                                                    <div class="post-content">
                                                        <h2><a href="#">Họp mặt nhân kỷ niệm 1979 năm cuộc khởi nghĩa Hai Bà Trưng và 109 năm ngày Quốc tế Phụ nữ</a></h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="post-content">
                                                        <h2><a href="#">HỘI NGHỊ CÁN BỘ VIÊN CHỨC VÀ NGƯỜI LAO ĐỘNGNĂM 2019</a></h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="post-content">
                                                        <h2><a href="#">CHI ĐOÀN HỖ TRỢ THANH NIÊN NGHÈO PHƯỜNG 13</a></h2>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End carousel box -->
                            <!-- google addsense -->
                            <div class="advertisement">
                                <div class="desktop-advert">
                                    <img src="http://thethaophunhuan.com.vn/upload/hinhanh/413.png" width="100%" alt="">
                                </div>
                                <div class="tablet-advert">
                                    <img src="http://thethaophunhuan.com.vn/upload/hinhanh/413.png" width="100%" alt="">
                                </div>
                                <div class="mobile-advert">
                                    <img src="http://thethaophunhuan.com.vn/upload/hinhanh/413.png" width="100%" alt="">
                                </div>
                            </div>
                            <!-- End google addsense -->
                        </div>
                        <!-- End block content -->
                    </div>
                    <div class="col-sm-3">
                        <!-- sidebar -->
                        <?php include('sidebar.php') ?>
                        <!-- End sidebar -->
                    </div>
                </div>
            </div>
        </section>
        <!-- End block-wrapper-section -->
        <!-- footer
            ================================================== -->
       <?php include('footer.php') ?>
        <!-- End footer -->
    </div>
    <!-- End Container -->
    <?php include('script.php') ?>
</body>

</html>