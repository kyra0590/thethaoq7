<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use App\Models\Setting;

class SettingController extends BaseController
{
    public function index(Request $request){
        $params  = $request->all();
        $isAjax  = (isset($params['is_ajax'])) ? $params['is_ajax'] : false;
        $start   = (!empty($params['start'])) ? $params['start'] : 0;
        $keyword = (!empty($params['keyword'])) ? $params['keyword'] : '';
        $limit   =  (!empty($params['limit'])) ? $params['limit'] : 50;
        $setting    = Setting::where('module', 'like', "%{$keyword}%")
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->offset($start)
            ->get();
        if($isAjax) {
            return response()->json(
                 [
                    "recordsFiltered" => count($setting),
                    "recordsTotal" => count($setting),
                    "data" => $setting
                 ], 200
            );
        }
        return view('admin/setting/index')
            ->with('title', "Danh sách Meta trang");
    }

    public function add(Request $request){
        $params = $request->all();
        $title = "Thêm Meta trang";
        $status = false;
        if(!empty($params)) {
            $params['title'] = trim($params['title']);
            $params['module'] = trim($params['module']);
            $params['created_at'] = date("Y-m-d");
            unset($params['_token']);
            $status = Setting::insert($params);
        }
        return view('admin/setting/add')
            ->with('title', $title)
            ->with('status', $status);
    }

     public function edit(Request $request, $id){
        $title = 'Sửa Meta trang';
        $setting = Setting::find($id);
        $status = false;
        if(!$id || empty($setting)) {
           return redirect('/admin/setting');
        }
        $params = $request->all();
        if(!empty($params)) {
            $setting->title       = trim($params['title']);
            $setting->module       = trim($params['module']);
            $setting->keywords    = $params['keywords'];
            $setting->description = $params['description'];
            $status = $setting->save();
        }
        return view('admin/setting/edit')
            ->with('title', $title)
            ->with('setting', $setting)
            ->with('status', $status);
    }

    public function del($id){
        // if($id){
        //     Setting::where('id', $id)->delete();
        // }
        // return redirect("/admin/setting");
    }

}
