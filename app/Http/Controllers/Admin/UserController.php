<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends BaseController
{
    public function index(Request $request){
        $params  = $request->all();
        $isAjax  = (isset($params['is_ajax'])) ? $params['is_ajax'] : false;
        $start   = (!empty($params['start'])) ? $params['start'] : 0;
        $keyword = (!empty($params['keyword'])) ? $params['keyword'] : '';
        $limit   =  (!empty($params['limit'])) ? $params['limit'] : 50;
        $attention    = User::where('username', 'like', "%{$keyword}%")
            ->orWhere('email', 'like', "%{$keyword}%")
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->offset($start)
            ->get();
        if($isAjax) {
            return response()->json(
                 [
                    "recordsFiltered" => count($attention),
                    "recordsTotal" => count($attention),
                    "data" => $attention
                 ], 200
            );
        }
        return view('admin/user/index')
            ->with('title', "Danh sách user");
    }

    public function add(Request $request){
        $params = $request->all();
        $status = false;
        if(!empty($params)) {
            $params['username']   = trim($params['username']);
            $params['fullname']   = trim($params['fullname']);
            $params['email']      = trim($params['email']);
            $params['block']      = (int) $params['block'];
            $params['password']   = Hash::make($params['password']);
            $params['created_at'] = date("Y-m-d");
            unset($params['_token']);
            $status = User::insert($params);
        }
        return view('admin/user/add')
            ->with('status', $status)
            ->with('title', 'Thêm user');
    }

    public function edit(Request $request, $id) {
        $user = User::find($id);
        if(!$id && !empty($user)) {
           return redirect('/admin/user');
        }
        $params = $request->all();
        $status = false;
        if(!empty($params)) {
            $user->username   = trim($params['username']);
            $user->fullname   = trim($params['fullname']);
            $user->email      = trim($params['email']);
            $user->tel        = trim($params['tel']);
            $user->block      = (int) $params['block'];
            $user->permission = (int) $params['permission'];
            $user->password   =  (!empty($params['password'])) ? Hash::make($params['password']) : $user->password;
            $status           = $user->save();
        }
        return view('admin/user/edit')
            ->with('user', $user)
            ->with('status', $status)
            ->with('title', 'Sửa user');
    }

    public function del($id){
        if($id){
            $user = User::find($id);
            if(!empty($user)) {
                $user->delete();
            }
        }
        return redirect("/admin/user");
    }

}