@extends('user.layout')

@section('title', $notify->title)
@section('description', $notify->description)
@section('keywords', $notify->keywords)

@section('content')
<div class="grid-box">
    <div class="single-post-box">
        <div class="title-post">
            <h1>{{$notify->title}}</h1>
            <ul class="post-tags">
                <li><i class="fa fa-clock-o"></i>{{date("d M Y", strtotime($notify->date_show))}}</li>
            </ul>
        </div>
        <div class="post-gallery">
            @if (!empty($notify->img) && file_exists($imgPath."/notify/".$notify->img))
            <img src="/public/images/notify/{{$notify->img}}" alt="{{$notify->caption}}">
            <span class="image-caption">{{$notify->caption}}</span>
            @endif
        </div>
        <div class="post-content">
            {!! $notify->content  !!}
        </div>
        @if(!empty($lastestNotify) && count($lastestNotify))
        <div class="col-md-12">
            <div class="title-section">
                <h4><span class="world">Thông báo khác</span></h4>
            </div>
            <ul class="list-posts">
                @foreach($lastestNotify as $item)
                <li style="border:none">
                    <div class="post-content">
                        <h2><a href="/thong-bao/{{$item->link}}.html">»» {{$item->title}}</a></h2>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>
@endsection