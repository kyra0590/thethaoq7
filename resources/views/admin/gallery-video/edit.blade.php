@extends('admin.layout')
@section('title', $title)
@section('content')

<div class="row page-titles">
    <div class="col-md-12 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <button type="button" class="btn btn-info d-none d-lg-block m-l-15" onclick="javascript:location.href='/admin/gallery-video'"><i class="fa fa-undo"></i> Trở lại</button>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-success">
               <h4 class="m-b-0 text-white">{{$title}}</h4>
            </div>
            <div class="card-body">
                <form action="" method="post" enctype="multipart/form-data">
                    @csrf
                     <div class="form-body">
                        <div class="row p-t-20">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Hiển thị</label>
                                    <?php $show = ["Ẩn", "Hiện"]; ?>
                                    <select class="form-control custom-select" name="is_showed">
                                        <?php foreach ($show as $key => $value) {
                                            $selected = ($gallery->is_showed == $key) ? 'selected' : '';
                                        ?>
                                            <option value="{{$key}}" {{$selected}}>{{$value}}</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                             <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"> <input type="checkbox" name="is_show_navi" <?= ($gallery->is_show_navi) ? 'checked' : '' ?> value="1"> Hiển thị điều hướng phải</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Tiêu đề</label>
                                    <input type="text" id="lastName" class="form-control" name="title" value="{{$gallery->title}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Ngày hiển thị</label>
                                    <input type="text" class="form-control" id="mdate" name="date_show" value="{{$gallery->date_show}}">
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Mô tả ngắn</label>
                                    <textarea name="short_desc" class="form-control" rows="4" width="100%">{{$gallery->short_desc}}</textarea>
                                </div>
                            </div>
                             <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Đường dẫn</label>
                                    <input type="text" id="link" class="form-control" name="link" value="{{$gallery->link}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Meta Keywords</label>
                                    <textarea class="form-control form-control-danger" name="keywords" rows="5">{{$gallery->keywords}}</textarea>
                                </div>
                            </div>
                             
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Meta Descriptions</label>
                                    <textarea class="form-control form-control-danger" name="description" rows="5">{{$gallery->description}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Image (375x250)</label>
                                    <input type="file"  id="input-file-now" class="img dropify" name='img' data-default-file="{{ !empty($gallery->img) ? '/public/images/gallery/video/'.$gallery->img : '' }}" data-allowed-file-extensions="jpeg jpg png gif"/>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                             <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Video ( < 100MB)</label>
                                    <input type="file"  id="input-file-now" class="dropify video" name='video' data-default-file="{{ !empty($gallery->video) ? '/public/images/sample_video.png' : '' }}" data-max-file-size="100M" data-allowed-file-extensions="mp4"/>
                                </div>
                                <?php if(!empty($gallery->video) && file_exists($_SERVER['DOCUMENT_ROOT']."/public/images/gallery/video/".$gallery->video)) { ?>
                                <div class="form-group">
                                    <label class="control-label">Video demo</label> <br>
                                    <video   width="320" height="240"  controls>
                                      <source src="/public/images/gallery/video/{{$gallery->video}}" type="video/mp4">
                                    </video>
                                </div>
                                <?php } ?>
                            </div>
                             <div class="col-md-6">
                               <div class="form-group">
                                    <label class="control-label">Dữ liệu video</label>
                                    <select class="form-control custom-select" name="type_video" >
                                        <?php
                                            $type_video = ["Nguồn trên side", "Nguồn khác"];
                                             foreach ($type_video as $key => $value) {
                                            $selected = ($gallery->type_video == $key) ? 'selected' : '';
                                        ?>
                                            <option value="{{$key}}" {{$selected}}>{{$value}}</option>
                                        <?php } ?>
                                    </select>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label">Đường dẫn Video</label>
                                    <input type="text" class="form-control" name='link_video' value="{{$gallery->link_video}}"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Lưu</button>
                        <button type="button" class="btn btn-danger" onclick="javascript:location.href='/admin/gallery-video'"> <i class="fa fa-ban"></i> Hủy bỏ</button>
                    </div>
                    <input type="hidden" id='remove_img' name='remove_img' value='0'>
                    <input type="hidden" id='remove_video' name='remove_video' value='0'>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
<?php if($status) { ?>
 $.toast({
    heading: "Thông báo",
    text: "Cập nhật thành công",
    position: 'bottom-right',
    loaderBg:'#ff6849',
    icon: 'success',
    hideAfter: 3500,
    stack: 6
});
<?php } ?>
</script>
@endsection
