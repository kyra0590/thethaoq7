<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use App\Models\GalleryVideo;
use Image;
use File;

class GalleryVideoController extends BaseController
{
    public function index(Request $request){
        $params  = $request->all();
        $isAjax  = (isset($params['is_ajax'])) ? $params['is_ajax'] : false;
        $start   = (!empty($params['start'])) ? $params['start'] : 0;
        $keyword = (!empty($params['keyword'])) ? $params['keyword'] : '';
        $limit   =  (!empty($params['limit'])) ? $params['limit'] : 50;
        $gallery    = GalleryVideo::where('title', 'like', "%{$keyword}%")
            ->orderBy('order', 'asc')
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->offset($start)
            ->get();
        if($isAjax) {
            return response()->json(
                 [
                    "recordsFiltered" => count($gallery),
                    "recordsTotal" => count($gallery),
                    "data" => $gallery
                 ], 200
            );
        }
        return view('admin/gallery-video/index')
            ->with('title', "Danh sách video");
    }

    public function add(Request $request){
        $params = $request->all();
        $title = "Thêm danh sách video";
        $status = false;
        if(!empty($params)) {
            $params['date_show'] = (!empty($params['date_show'])) ? $params['date_show'] : date("Y-m-d");
            $params['title'] = trim($params['title']);
            $params['created_at'] = date("Y-m-d");
            $params['order'] = !empty($params['order']) ? (int) $params['order'] : 0;
            $params['is_show_navi'] = !empty($params['is_show_navi']) ? (int) $params['is_show_navi'] : 0;
            unset($params['_token']);
            $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/gallery/video/";
            if($request->hasFile('img')) {
                $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                    $params['img'] = $fileName;

                    $imgThumb = Image::make($descriptionPath.$fileName);
                    $imgThumb->resize(170, 130, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($descriptionPath.$fileName);
                }
            }

            if($request->hasFile('video')) {
                $fileName = uniqid().strtotime('now').".{$request->video->extension()}";
                if(move_uploaded_file($_FILES['video']['tmp_name'], $descriptionPath.$fileName)) {
                    $params['video'] = $fileName;
                }
            }

            $status = GalleryVideo::insert($params);
        }
        return view('admin/gallery-video/add')
            ->with('title', $title)
            ->with('status', $status);
    }

    public function edit(Request $request, $id){
        $title = 'Sửa danh sách video';
        $gallery = GalleryVideo::find($id);
        $status = false;
        if(!$id || empty($gallery)) {
           return redirect('/admin/gallery-video');
        }
        $params = $request->all();
        if(!empty($params)) {
            $params['date_show'] = (!empty($params['date_show'])) ? $params['date_show'] : date("Y-m-d");
            $gallery->title       = trim($params['title']);
            $gallery->is_showed   = $params['is_showed'];
            $gallery->link        = $params['link'];
            $gallery->keywords    = $params['keywords'];
            $gallery->description = $params['description'];
            $gallery->short_desc  = $params['short_desc'];
            $gallery->date_show   = $params['date_show'];
            $gallery->type_video  = $params['type_video'];
            $gallery->link_video  = $params['link_video'];
            $gallery->order       = !empty($params['order']) ? (int) $params['order'] : 0;
            $gallery->is_show_navi = !empty($params['is_show_navi']) ? (int) $params['is_show_navi'] : 0;
            $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/gallery/video/";
            if($request->hasFile('img')) {
                $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                    if(!empty($gallery->img) && file_exists($descriptionPath.$gallery->img)) {
                        unlink($descriptionPath.$gallery->img);
                    }
                    $imgThumb = Image::make($descriptionPath.$fileName);
                    $imgThumb->resize(170, 130, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($descriptionPath.$fileName);
                    $gallery->img = $fileName;
                }
            } else {
                if( $params['remove_img'] && file_exists($descriptionPath.$gallery->img)) {
                    unlink($descriptionPath.$gallery->img);
                    $gallery->img = "";
                }
            }

            if($request->hasFile('video')) {
                $fileName = uniqid().strtotime('now').".{$request->video->extension()}";
                if(move_uploaded_file($_FILES['video']['tmp_name'], $descriptionPath.$fileName)) {
                    if(!empty($gallery->video) && file_exists($descriptionPath.$gallery->video)) {
                        unlink($descriptionPath.$gallery->video);
                    }
                    $gallery->video = $fileName;
                }
            } else {
                if( $params['remove_video'] && file_exists($descriptionPath.$gallery->video)) {
                    unlink($descriptionPath.$gallery->video);
                    $gallery->video = "";
                }
            }
            $status = $gallery->save();
        }
        return view('admin/gallery-video/edit')
            ->with('title', $title)
            ->with('gallery', $gallery)
            ->with('status', $status);
    }

    public function del($id){
         if($id){
            GalleryVideo::where('id', $id)->delete();
            $descriptionPath    = $_SERVER['DOCUMENT_ROOT']."/public/images/gallery/video/";
            if(!empty($gallery->img) && file_exists($descriptionPath.$gallery->img)) {
                unlink($descriptionPath.$gallery->img);
            }

            if(!empty($gallery->video) && file_exists($descriptionPath.$gallery->video)) {
                unlink($descriptionPath.$gallery->video);
            }
        }
        return redirect("/admin/gallery-video");
    }
}
