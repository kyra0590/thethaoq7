<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use App\Models\Home;
use Image;

class HomeController extends BaseController
{
     public function index(Request $request){
        $title  = 'Cập nhật trang chủ';
        $home   = Home::find(1);
        $status = false;
        if(empty($home)) {
           return redirect('/admin/');
        }
        $params = $request->all();
        if(!empty($params)) {
            $home->title       = trim($params['title']);
            $home->keywords    = $params['keywords'];
            $home->description = $params['description'];

            $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/home/";
            if($request->hasFile('img_1')) {
                $fileName = uniqid().strtotime('now').".{$request->img_1->extension()}";
                if(move_uploaded_file($_FILES['img_1']['tmp_name'], $descriptionPath.$fileName)) {
                    if(!empty($home->img_1) && file_exists($descriptionPath.$home->img_1)) {
                        unlink($descriptionPath.$home->img_1);
                    }
                    $home->img_1 = $fileName;

                    $imgThumb = Image::make($descriptionPath.$fileName);
                    $imgThumb->resize(780, 150, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($descriptionPath.$fileName);
                }
            } else {
                if( $params['remove_img_1'] && file_exists($descriptionPath.$home->img_1)) {
                    unlink($descriptionPath.$home->img_1);
                    $home->img_1 = "";
                }
            }

            if($request->hasFile('img_2')) {
                $fileName = uniqid().strtotime('now').".{$request->img_2->extension()}";
                if(move_uploaded_file($_FILES['img_2']['tmp_name'], $descriptionPath.$fileName)) {
                    if(!empty($home->img_2) && file_exists($descriptionPath.$home->img_2)) {
                        unlink($descriptionPath.$home->img_2);
                    }
                    $home->img_2 = $fileName;

                    $imgThumb = Image::make($descriptionPath.$fileName);
                    $imgThumb->resize(780, 150, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($descriptionPath.$fileName);
                }
            } else {
                if( $params['remove_img_2'] && file_exists($descriptionPath.$home->img_2)) {
                    unlink($descriptionPath.$home->img_2);
                    $home->img_2 = "";
                }
            }

            if($request->hasFile('img_3')) {
                $fileName = uniqid().strtotime('now').".{$request->img_3->extension()}";
                if(move_uploaded_file($_FILES['img_3']['tmp_name'], $descriptionPath.$fileName)) {
                    if(!empty($home->img_3) && file_exists($descriptionPath.$home->img_3)) {
                        unlink($descriptionPath.$home->img_3);
                    }
                    $home->img_3 = $fileName;

                    $imgThumb = Image::make($descriptionPath.$fileName);
                    $imgThumb->resize(780, 150, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($descriptionPath.$fileName);
                }
            } else {
                if( $params['remove_img_3'] && file_exists($descriptionPath.$home->img_3)) {
                    unlink($descriptionPath.$home->img_3);
                    $home->img_3 = "";
                }
            }

            $status = $home->save();
        }
        return view('admin/home/index')
            ->with('title', $title)
            ->with('home', $home)
            ->with('status', $status);
    }
}
