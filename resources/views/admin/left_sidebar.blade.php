<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                    <i class="far fa-circle text-info"></i><span class="hide-menu">Quản lý chung</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="/admin/home"><i class="far fa-circle text-info"></i> Trang chủ</a></li>
                        <li> <a href="/admin/user" aria-expanded="false"><i class="icon-user text-info"></i><span> Quản lý user</span></a></li>
                        <li><a href="/admin/setting"><i class="far fa-circle text-info"></i> Metadata trang</a></li>
                        <li><a href="/admin/relation"><i class="far fa-circle text-info"></i> Liên kết</a></li>
                    </ul>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                    <i class="far fa-circle text-info"></i><span class="hide-menu">Điều hướng phải</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="/admin/slider-top"><i class="far fa-circle text-info"></i> Slider Top</a></li>
                        <li><a href="/admin/slider"><i class="far fa-circle text-info"></i> Slider</a></li>
                        <li><a href="/admin/notify"><i class="far fa-circle text-info"></i> Thông báo</a></li>
                        <li><a href="/admin/partner"><i class="far fa-circle text-info"></i> Đối tác</a></li>
                    </ul>
                </li>


                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                    <i class="far fa-circle text-info"></i><span class="hide-menu">Giới thiệu</a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="/admin/intro"><i class="far fa-circle text-info"></i> Danh sách</a></li>
                        <li><a href="/admin/intro-detail"><i class="far fa-circle text-info"></i> Tin theo giới thiệu</a></li>
                    </ul>
                </li>
                <li><a href="/admin/news"><i class="far fa-circle text-info"></i> Tin tức - Sự kiện</a></li>
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                    <i class="far fa-circle text-info"></i><span class="hide-menu">Bộ môn</a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="/admin/sport"><i class="far fa-circle text-info"></i> Danh sách</a></li>
                        <li><a href="/admin/sport-detail"><i class="far fa-circle text-info"></i> Tin theo bộ môn</a></li>
                    </ul>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                    <i class="far fa-circle text-info"></i><span class="hide-menu">Thư viện</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="/admin/gallery-image"><i class="far fa-circle text-info"></i> Hình ảnh</a></li>
                        <li><a href="/admin/gallery-video"><i class="far fa-circle text-info"></i> Video</a></li>
                    </ul>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                    <i class="far fa-circle text-info"></i><span class="hide-menu">Thông tin</a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="/admin/info"><i class="far fa-circle text-info"></i> Danh sách</a></li>
                        <li><a href="/admin/info-detail"><i class="far fa-circle text-info"></i> Tin theo thông tin</a></li>
                    </ul>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                    <i class="far fa-circle text-info"></i><span class="hide-menu">Đảng - Đoàn thể</a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="/admin/union"><i class="far fa-circle text-info"></i> Danh sách</a></li>
                        <li><a href="/admin/union-detail"><i class="far fa-circle text-info"></i> Tin Đảng - Đoàn thể</a></li>
                    </ul>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                    <i class="far fa-circle text-info"></i><span class="hide-menu">Chiêu sinh</a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="/admin/admissions"><i class="far fa-circle text-info"></i> Danh sách</a></li>
                        <li><a href="/admin/admissions-detail"><i class="far fa-circle text-info"></i> Tin chiêu sinh</a></li>
                    </ul>
                </li>
                <li><a href="/admin/contact"><i class="far fa-circle text-info"></i> Liên hệ</a></li>

                <li> <a class="waves-effect waves-dark" href="/admin/logout" aria-expanded="false"><i class="icon-logout text-info"></i><span class="hide-menu"> Đăng xuất</span></a></li>
            </ul>
        </nav>
    </div>
</aside>
