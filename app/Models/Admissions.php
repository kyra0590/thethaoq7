<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admissions extends Model
{
	protected $table      = 'admissions';
	protected $primaryKey = 'id';
	public $timestamps    = true;
}
