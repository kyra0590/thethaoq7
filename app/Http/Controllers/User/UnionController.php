<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\User\BaseController;
use Illuminate\Http\Request;
use App\Models\Union;
use App\Models\UnionDetail;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;

class UnionController extends BaseController
{
    public function index(){
        $setting = Setting::where("module", "union")->first();
        if(empty($setting)) abort(404);

        $union = DB::table('union_detail')
            ->selectRaw("union_detail.id, union_detail.title, union.title as union_title,
                union_detail.short_desc, union_detail.content,
                union_detail.keywords, union_detail.is_showed,
                union_detail.date_show, union_detail.link,
                union_detail.order, union_detail.img,
                union.link as unionLink, union_detail.id_union,
                union_detail.caption")
            ->join('union', 'union.id', '=', 'union_detail.id_union')
            ->where('union.is_showed', 1)
            ->where('union_detail.is_showed', 1)
            ->orderBy('union_detail.order', 'asc')
            ->orderBy('union_detail.id', 'desc')
            ->paginate(10)
            ->withPath("/dang-doan-the");

        return view("user/union/index")
            ->with('setting', $setting)
            ->with('union', $union);
    }

    public function list($id = "", $link = "") {
    	if($id && !$link) {
    		abort(404);
    	}

    	$union = Union::where('is_showed', 1)
            ->where('id', $id)
            ->where('link', $link)->first();

        if(empty($union)) abort(404);

        $unionDetail = DB::table('union_detail')
            ->selectRaw("union_detail.id, union_detail.title, union.title as union_title,
                union_detail.short_desc, union_detail.content,
                union_detail.keywords, union_detail.is_showed,
                union_detail.date_show, union_detail.link,
                union_detail.order, union_detail.img,
                union.link as unionLink, union_detail.id_union,
                union_detail.caption")
            ->join('union', 'union.id', '=', 'union_detail.id_union')
            ->where('union.id', $id)
            ->where('union.is_showed', 1)
            ->where('union_detail.is_showed', 1)
            ->orderBy('union_detail.order', 'asc')
            ->orderBy('union_detail.id', 'desc')
            ->paginate(10)
            ->withPath("/dang-doan-the");

    	return view("user/union/list")
            ->with('union', $union)
            ->with('unionDetail', $unionDetail);
    }

     public function detail($id = "", $link = "") {
        if(!$id && !$link) {
            abort(404);
        }

        $union = Union::where('is_showed', 1)->where('id', $id)->first();

        if(empty($union)) abort(404);

        $unionDetail = UnionDetail::where('is_showed', 1)
            ->where('id_union', $id)
            ->where('link', $link)
            ->first();

        if(empty($unionDetail)) abort(404);

        $relationUnion =  UnionDetail::where('is_showed', 1)
            ->where('id_union', $id)
            ->where('link', "!=", $link)
            ->limit(5)
            ->get();

        return view("user/union/detail")
            ->with('union', $union)
            ->with('unionDetail', $unionDetail)
            ->with('relationUnion', $relationUnion);
    }
}
