<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Setting;
use Illuminate\Http\Request;
use Captcha;
use Illuminate\Support\Facades\Mail;

class ContactController extends BaseController
{
	public function index(Request $request){
		$setting = Setting::where("module", "contact")->first();
        if(empty($setting)) abort(404);

        $params = $request->all();
        $errors = [];
        if (!empty($params)){
        	$fullname = !empty($params['fullname']) ? trim(strip_tags($params['fullname'])) : '';
        	$phone = !empty($params['phone']) ? trim(strip_tags($params['phone'])) : '';
        	$email = !empty($params['email']) ? trim(strip_tags($params['email'])) : '';
        	$content = !empty($params['content']) ? trim(strip_tags($params['content'])) : '';
        	$captcha = !empty($params['captcha']) ? trim($params['captcha']) : '';

        	if(empty($fullname)) $errors['fullname'] = "Họ tên";
        	if(empty($phone)) $errors['phone'] = "Điện thoại";
        	else if(!is_numeric($phone)) $errors['phone'] = "Số điện thoại sai kiểu";
        	if(empty($email)) $errors['email'] = "Email";
        	else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) $errors['email'] = "Email sai kiểu";
        	if(empty($content)) $errors['content'] = "Nội dung gửi";
        	if(!Captcha::check($captcha)) $errors['captcha'] = "Sai mã captcha";

        	if(empty($errors)) {
        		Contact::insert([
        			'fullname' => $fullname,
        			'email' => $email,
        			'phone' => $phone,
        			'content' => $content,
        			'created_at' => date("Y-m-d H:i:s")
				]);
				// Mail::send('mails.demo', array('name'=> "họ tên",'email'=> "kyra0590@gmail.com", 'content'=> "Nội dung"), function($message){
				//     $message->to('kyra0590@gmail', 'Tam Visitor')->subject('Visitor Feedback!');
				// });
        		return redirect("/thanks");
        	}
		}

		return view('user/contact/index')
			->with('setting', $setting)
			->with('errors', $errors)
			->with('params', $params);
	}

    public function thanks(){
        $setting = Setting::where("module", "thanks")->first();
        return view('user/contact/thanks')
            ->with('setting', $setting);
    }

}