@extends('user.layout')

@section('title', $intro->title)
@section('description', $intro->description)
@section('keywords', $intro->keywords)

@section('content')
<div class="grid-box">
    <div class="single-post-box">
        <div class="title-post">
            <h1>{{$introDetail->title}}</h1>
            <ul class="post-tags">
                <li><i class="fa fa-clock-o"></i>{{date("d M Y", strtotime($introDetail->date_show))}}</li>
            </ul>
        </div>
        <div class="post-gallery">
            @if (!empty($introDetail->img) && file_exists($imgPath."/intro/".$introDetail->img))
            <img src="/public/images/intro/{{$introDetail->img}}" alt="{{$introDetail->caption}}">
            <span class="image-caption">{{$introDetail->caption}}</span>
            @endif
        </div>
        <div class="post-content">
            {!! $introDetail->content  !!}
        </div>
        @if(!empty($relationIntro) && count($relationIntro))
        <div class="col-md-12">
            <div class="title-section">
                <h1><span class="world">TIN LIÊN QUAN</span></h1>
            </div>
            <ul class="list-posts">
                @foreach($relationIntro as $item)
                <li style="border:none">
                    <div class="post-content">
                        <h2><a href="/gioi-thieu/{{$item->id_intro}}-{{$item->link}}.html">»» {{$item->title}}</a></h2>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>
@endsection