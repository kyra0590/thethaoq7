@extends('user.layout')

@section('title', $setting->title)
@section('description', $setting->description)
@section('keywords', $setting->keywords)

@section('content')
<div class="grid-box">
    <div class="title-section">
        <h1><span>Tìm kiếm Tin tức - Sự kiện</span></h1>
    </div>
    @if(!empty($news) && count($news))
        @foreach($news as $item)
        <div class="news-post article-post">
            <div class="row">
                <div class="col-sm-3">
                    <div class="post-gallery">
                        @if (!empty($item->img) && file_exists($imgPath."/news/thumb/".$item->img))
                            <img src="/public/images/news/thumb/{{$item->img}}"  alt="{{$item->title}}" width="100%">
                        @else
                            <img src="/public/images/logo_thumb.jpg" style="width: auto !important; height: 150px">
                        @endif
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="post-content">
                        <h2><a href="/tin-tuc/{{$item->link}}.html">{{$item->title}}</a></h2>
                        <p>{{$item->short_desc}}</p>
                        <a href="/tin-tuc/{{$item->link}}.html" class="read-more-button"><i class="fa fa-arrow-circle-right"></i>Đọc thêm</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    @endif
</div>
@if(!empty($news) && count($news))
<div class="pagination-box">
    <ul class="pagination-list">
        @if ($news->lastPage() == 1)
            <li><a class="active" href="#">1</a></li>
        @elseif ($news->lastPage() > 1)
            @if($news->currentPage() > 1)
                <li><a href="{{ $news->url($news->currentPage() - 1) }}">Back</a></li>
            @endif

             @for ($i = 1; $i <= $news->lastPage(); $i++)
                <li><a class="{{ ($news->currentPage() == $i) ? ' active' : '' }}" href="{{ $news->url($i) }}">{{$i}}</a></li>
            @endfor

            @if($news->currentPage() < $news->lastPage())
            <li><a href="{{ $news->url($news->currentPage()+1) }}">Next</a></li>
            @endif
        @endif
    </ul>

    <p>Page {{$news->currentPage()}} of {{$news->lastPage()}}</p>
</div>
@else
    <p>Hiện tại không tìm thấy dữ liệu</p>
@endif
@endsection