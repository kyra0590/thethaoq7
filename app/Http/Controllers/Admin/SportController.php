<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use App\Models\Sport;
use App\Models\SportDetail;
use Illuminate\Support\Facades\DB;
use Image;

class SportController extends BaseController
{
	public function index(Request $request){
        $params = $request->all();
        $isAjax = (isset($params['is_ajax'])) ? $params['is_ajax'] : false;
        $start = (!empty($params['start'])) ? $params['start'] : 0;
        $keyword = (!empty($params['keyword'])) ? $params['keyword'] : '';
        $limit =  (!empty($params['limit'])) ? $params['limit'] : 50;
        $sport = Sport::where('title', 'like', "%{$keyword}%")
            ->orderBy('order', 'asc')
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->offset($start)
            ->get();
        if($isAjax) {
            return response()->json(
                 [
                "recordsFiltered" => count($sport),
                "recordsTotal" => count($sport),
                "data" => $sport
            ], 200

                );
        }
        return view('admin/sport/index')
            ->with('title', "Danh sách bộ môn");
    }

    public function add(Request $request){
        $params = $request->all();
        $title = "Thêm bộ môn";
        $status = false;
        if(!empty($params)) {
            $params['title'] = trim($params['title']);
            $params['created_at'] = date("Y-m-d");
            $params['order'] = !empty($params['order']) ? (int) $params['order'] : 0;
            unset($params['_token']);
            if($request->hasFile('icon')) {
                $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/sport/";
                $fileName = uniqid().strtotime('now').".{$request->icon->extension()}";
                if(move_uploaded_file($_FILES['icon']['tmp_name'], $descriptionPath.$fileName)) {
                    $params['icon'] = $fileName;

                    $imgThumb = Image::make($descriptionPath.$fileName);
                    $imgThumb->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($descriptionPath.$fileName);
                }
            }
            $status = Sport::insert($params);
        }
        return view('admin/sport/add')
            ->with('title', $title)
            ->with('status', $status);
    }

     public function edit(Request $request, $id){
        $title = 'Sửa bộ môn';
        $sport = Sport::find($id);
        $status = false;
        if(!$id || empty($sport)) {
           return redirect('/admin/sport');
        }
        $params = $request->all();
        if(!empty($params)) {
            $sport->title       = trim($params['title']);
            $sport->is_showed   = $params['is_showed'];
            $sport->link        = $params['link'];
            $sport->keywords    = $params['keywords'];
            $sport->description = $params['description'];
            $sport->content     = $params['content'];
            $sport->order       = !empty($params['order']) ? (int) $params['order'] : 0;
            $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/sport/";
            if($request->hasFile('icon')) {
                $fileName = uniqid().strtotime('now').".{$request->icon->extension()}";
                if(move_uploaded_file($_FILES['icon']['tmp_name'], $descriptionPath.$fileName)) {
                    if(!empty($sport->icon) && file_exists($descriptionPath.$sport->icon)) {
                        unlink($descriptionPath.$sport->icon);
                    }
                    $sport->icon = $fileName;

                    $imgThumb = Image::make($descriptionPath.$fileName);
                    $imgThumb->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($descriptionPath.$fileName);
                }
            } else {
                if( $params['remove_img'] && file_exists($descriptionPath.$sport->icon)) {
                    unlink($descriptionPath.$sport->icon);
                    $sport->icon = "";
                }
            }
            $status             = $sport->save();
        }
        return view('admin/sport/edit')
            ->with('title', $title)
            ->with('sport', $sport)
            ->with('status', $status);
    }

    public function del($id){
         if($id){
            Sport::where('id', $id)->delete();
            SportDetail::where('id_sport', $id)->update(['id_sport' => 0, 'is_showed' => 0]);
            $descriptionPath    = $_SERVER['DOCUMENT_ROOT']."/public/images/sport/";
            if(!empty($sport->icon) && file_exists($descriptionPath.$sport->icon)) {
                unlink($descriptionPath.$sport->icon);
            }
        }
        return redirect("/admin/sport");
    }

    public function detail(Request $request){
        $title   = "Danh sách tin tức bộ môn";
        $params  = $request->all();
        $idSport = (!empty($params['idSport'])) ? trim($params['idSport']) : '';
        $isAjax  = (isset($params['is_ajax'])) ? $params['is_ajax'] : false;
        $start   = (!empty($params['start'])) ? $params['start'] : 0;
        $keyword = (!empty($params['keyword'])) ? $params['keyword'] : '';
        $limit   =  (!empty($params['limit'])) ? $params['limit'] : 50;
        $sportDetail = DB::table('sport_detail')
            ->selectRaw("sport_detail.id, sport_detail.title, sport.title as sport_title,
                sport_detail.short_desc, sport_detail.content,
                sport_detail.keywords, sport_detail.is_showed,
                sport_detail.date_show, sport_detail.link,
                sport_detail.order, sport_detail.img,
                sport_detail.is_new")
            ->join('sport', 'sport.id', '=', 'sport_detail.id_sport')
            ->where('sport_detail.title', 'like', "%{$keyword}%")
            ->orderBy('sport_detail.order', 'asc')
            ->orderBy('sport_detail.id', 'desc')
            ->limit($limit)
            ->offset($start);
        if($idSport != "") {
            $sportDetail->where('sport_detail.id_sport', $idSport);
            $sport = Sport::find($idSport);
            if(!empty($sport)) {
                $sport = $sport->first();
                $title .= " - ". $sport->title;
            }
        }
        $sportDetail = $sportDetail->get();
        if($isAjax) {
            return response()->json(
            [
                "recordsFiltered" => count($sportDetail),
                "recordsTotal"    => count($sportDetail),
                "data"            => $sportDetail
            ], 200 );
        }
        return view('admin/sport/detail')
            ->with("sportDetail", $sportDetail)
            ->with('idSport', $idSport)
            ->with('title', $title);
    }

     public function addDetail(Request $request){
        $title   = "Thêm tin bộ môn";
        $params  = $request->all();
        $status  = false;
        $idSport = '';
        $listSport = Sport::orderBy('order', 'ASC')->orderBy("id","DESC")->get()->keyBy("id")->toArray();
        if(!empty($params)) {
            $idSport = (!empty($params['idSport'])) ? trim($params['idSport']) : '';
            if($idSport != '') {
                $sport = Sport::find($idSport)->first();
                $title .= " - ". $sport->title;
            }
            if(!empty($params['_token'])) {
                $params['title'] = trim($params['title']);
                $params['caption'] = trim($params['caption']);
                $params['created_at'] = date("Y-m-d");
                $params['order'] = (!empty($params['order'])) ? (int) $params['order'] : 0;
                $params['is_new'] = (!empty($params['is_new'])) ? (int) $params['is_new'] : 0;
                $params['date_show'] = (!empty($params['date_show'])) ? $params['date_show'] : date("Y-m-d");
                unset($params['_token']);
                unset($params['idSport']);
                if($request->hasFile('img')) {
                    $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/sport-detail/";
                    $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                    if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                        $params['img'] = $fileName;

                        $imgThumb = Image::make($descriptionPath.$fileName);
                        $imgThumb->resize(375, 250, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($descriptionPath.'thumb/'.$fileName);
                    }
                }
                $status = SportDetail::insert($params);
            }
        }
        return view('admin/sport/add-detail')
            ->with('title', $title)
            ->with('idSport', $idSport)
            ->with('listSport', $listSport)
            ->with('status', $status);
    }

    public function editDetail(Request $request, $id) {
        $title = 'Sửa bộ môn';
        $params = $request->all();
        $status = false;
        $idSport = '';
        $listSport = Sport::orderBy('order', 'ASC')->orderBy("id","DESC")->get()->keyBy("id")->toArray();
        $sportDetail = SportDetail::find($id);
        if(!$id || empty($sportDetail)) {
            return redirect("/admin/sport-detail?idSport={$sportDetail->id_sport}");
        }
        if(!empty($params)) {
            $idSport = (!empty($params['idSport'])) ? trim($params['idSport']) : '';
            if($idSport != '') {
                $sport = Sport::find($idSport)->first();
                $title .= " - ". $sport->title;
            }
            if(!empty($params['_token'])) {
                $sportDetail->title       = trim($params['title']);
                $sportDetail->caption     = trim($params['caption']);
                $sportDetail->id_sport    = $params['id_sport'];
                $sportDetail->is_showed   = $params['is_showed'];
                $sportDetail->link        = $params['link'];
                $sportDetail->keywords    = $params['keywords'];
                $sportDetail->description = $params['description'];
                $sportDetail->date_show   = $params['date_show'];
                $sportDetail->content     = $params['content'];
                $sportDetail->short_desc  = $params['short_desc'];
                $sportDetail->order       = !empty($params['order']) ? (int) $params['order'] : 0;
                $sportDetail->is_new       = !empty($params['is_new']) ? (int) $params['is_new'] : 0;
                $descriptionPath    = $_SERVER['DOCUMENT_ROOT']."/public/images/sport-detail/";
                if($request->hasFile('img')) {
                    $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                    if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                        if(!empty($sportDetail->img)) {
                            if(file_exists($descriptionPath.$sportDetail->img)) {
                                unlink($descriptionPath.$sportDetail->img);
                            }

                            if(file_exists($descriptionPath.'thumb/'.$sportDetail->img))
                            {
                                unlink($descriptionPath.'thumb/'.$sportDetail->img);
                            }
                        }

                        $imgThumb = Image::make($descriptionPath.$fileName);
                        $imgThumb->resize(375, 250, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($descriptionPath.'thumb/'.$fileName);

                        $sportDetail->img = $fileName;
                    }
                } else {
                    if( $params['remove_img'] && file_exists($descriptionPath.$sportDetail->img)) {
                        unlink($descriptionPath.$sportDetail->img);
                        if(file_exists($descriptionPath.'thumb/'.$sportDetail->img))
                        {
                            unlink($descriptionPath.'thumb/'.$sportDetail->img);
                        }
                        $sportDetail->img = "";
                    }
                }
                $status = $sportDetail->save();
            }
        }
        return view('admin/sport/edit-detail')
            ->with('title', $title)
            ->with('sportDetail', $sportDetail)
            ->with('idSport', $idSport)
            ->with('listSport', $listSport)
            ->with('status', $status);
    }

    public function delDetail(Request $request, $id){
        $params = $request->all();
        $idSport = (!empty($params['idSport'])) ? trim($params['idSport']) : '';
        if($id){
            $sportDetail = SportDetail::find($id);
            $descriptionPath    = $_SERVER['DOCUMENT_ROOT']."/public/images/sport-detail/";
            if(!empty($sportDetail->img) && file_exists($descriptionPath.$sportDetail->img)) {
                unlink($descriptionPath.$sportDetail->img);
                if(file_exists($descriptionPath.'thumb/'.$sportDetail->img))
                {
                    unlink($descriptionPath.'/thumb/'.$sportDetail->img);
                }
            }
            $sportDetail->delete();
        }
        return redirect("/admin/sport-detail?idSport={$idSport}");
    }
}
