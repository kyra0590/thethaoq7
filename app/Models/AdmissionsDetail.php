<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdmissionsDetail extends Model
{
	protected $table      = 'admissions_detail';
	protected $primaryKey = 'id';
	public $timestamps    = true;
}
