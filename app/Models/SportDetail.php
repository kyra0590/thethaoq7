<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SportDetail extends Model
{
	protected $table      = 'sport_detail';
	protected $primaryKey = 'id';
	public $timestamps    = true;
}
