<!doctype html>
<html lang="vn" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>@yield("title")</title>
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    @include("user.css")
    @yield("css")
</head>
<body>
    <div id="container">
        @include("user.header")
        <section class="block-wrapper">
            <div class="container shadow-white">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="block-content">
                             @yield("content")
                        </div>
                    </div>
                    <div class="col-sm-3">
                        @include("user.sidebar")
                    </div>
                </div>
            </div>
        </section>
        @include("user.footer")
    </div>
    @include("user.script")
    @yield("js")
</body>
</html>