<div class="sidebar show_pc">
     <div class="widget features-slide-widget">
        <div class="title-section">
            <h1><span>Tìm kiếm</span></h1>
        </div>
         <div class="slider_sidebar blur">
            <form id="search_news" class="navbar-form navbar-left" role="search" method="get" action="/tim-kiem">
                <input type="text" name="keyword" placeholder="Tìm kiếm tin">
                <button type="submit" id="search-submit"><i class="fa fa-search"></i></button>
            </form>
        </div>
    </div>

    @if(!empty($notifys) && count($notifys))
   <div class="widget post-widget">
        <div class="title-section">
            <h1><span>Thông báo mới</span></h1>
        </div>
        <div class="slider_sidebar blur">

            <marquee behavior="scroll" direction="up" style="height: 300px" onmouseover="this.stop();" onmouseout="this.start();">
                <ul class="slide_side list-posts">
                @foreach ($notifys as $item)
                   <li>
                    @if (!empty($item->img) && file_exists($imgPath."/notify/thumb/".$item->img))
                    <img src="/public/images/notify/thumb/{{$item->img}}" alt="{{$item->title}}">
                    @else
                    <img src="/public/images/logo_thumb.jpg" alt="{{$item->title}}">
                    @endif
                    <div class="post-content">
                        <h2 style="margin-top:20px"><a href="/thong-bao/{{$item->link}}.html">{{$item->title}}</a></h2>
                    </div>
                  </li>
                @endforeach
                </ul>
            </marquee>
        </div>
    </div>
    @endif

    @if(!empty($galleryImages) && count($galleryImages))
    <div class="widget features-slide-widget">
        <div class="title-section">
            <h1><span>THƯ VIỆN ẢNH</span></h1>
        </div>
        <div class="image-post-slider">
            <ul class="bxslider">
                @foreach ($galleryImages as $item)
                @if (!empty($item->img) && file_exists($imgPath."/gallery/image/".$item->img))
                <li>
                    <div class="news-post image-post2">
                        <img src="/public/images/gallery/image/{{$item->img}}" alt="{{$item->title}}">
                    </div>
                </li>
                @endif
                @endforeach
            </ul>
        </div>
    </div>
    @endif

    @if (!empty($galleryVideos))
    <div class="widget features-slide-widget">
        <div class="title-section">
            <h1><span>Video</span></h1>
        </div>
        <div>
            @if ($galleryVideos->type_video == 1 && !empty($galleryVideos->link_video))
                <iframe width="100%" src="{{$galleryVideos->link_video}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            @else
                @if (!empty($galleryVideos->video) && file_exists($imgPath."/gallery/video/".$galleryVideos->video))
                <video border='0' width="100%" controls>
                  <source src="/public/images/gallery/video/{{$galleryVideos->video}}" type="video/mp4">
                </video>
                @endif
            @endif
        </div>
    </div>
    @endif

    @if(!empty($relations) && count($relations))
    <div class="widget features-slide-widget">
        <div class="title-section">
            <h1><span>Liên kết Website</span></h1>
        </div>

        <div>
            <select class="form-control" onchange="window.open(this.value,'_Blank');" style="width:100%;height:30px;">
            <option value="">-- Lựa chọn --</option>
                @foreach ($relations as $item)
                    <option value="{{$item->link}}">{{$item->title}}</option>
                @endforeach
            </select>
        </div>
    </div>
    @endif

    @if(!empty($partners) && count($partners))
   <div class="widget post-widget">
        <div class="title-section">
            <h1><span>ĐỐI TÁC, CÁC NHÀ TÀI TRỢ</span></h1>
        </div>
         <marquee behavior="scroll" direction="up" style="height: 300px" onmouseover="this.stop();" onmouseout="this.start();">
            @foreach ($partners as $item)
            <p class="text-center">
                <a href="{{$item->link}}" target="_blank">
                    @if (!empty($item->img) && file_exists($imgPath."/notify/thumb/".$item->img))
                    <img src="/public/images/partner/{{$item->img}}" alt="{{$item->title}}" width="150px">
                    @else
                    <img src="/public/images/logo_thumb.jpg" alt="{{$item->title}}" width="150px">
                    @endif
                </a>
            </p>
            @endforeach
        </marquee>
    </div>
    @endif
    <div class="widget post-widget">
        <div class="title-section">
            <h1><span>THỐNG KÊ TRUY CẬP</span></h1>
        </div>
        <div style="text-align: center;">
            <!-- Histats.com  (div with counter) --><div id="histats_counter" style="text-align: center;"></div>
            <!-- Histats.com  START  (aync)-->
            <script type="text/javascript">var _Hasync= _Hasync|| [];
            _Hasync.push(['Histats.start', '1,4411933,4,403,118,80,00011111']);
            _Hasync.push(['Histats.fasi', '1']);
            _Hasync.push(['Histats.track_hits', '']);
            (function() {
            var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
            hs.src = ('//s10.histats.com/js15_as.js');
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
            })();</script>
            <noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?4411933&101" alt="" border="0" width="100%"></a></noscript>
            <!-- Histats.com  END  -->
        </div>
    </div>
</div>