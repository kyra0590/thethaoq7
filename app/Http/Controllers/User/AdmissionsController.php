<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\User\BaseController;
use Illuminate\Http\Request;
use App\Models\Admissions;
use App\Models\AdmissionsDetail;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;

class AdmissionsController extends BaseController
{
    public function index(){
        $setting = Setting::where("module", "admissions")->first();
        if(empty($setting)) abort(404);

        $admissions = DB::table('admissions_detail')
            ->selectRaw("admissions_detail.id, admissions_detail.title, admissions.title as admissions_title,
                admissions_detail.short_desc, admissions_detail.content,
                admissions_detail.keywords, admissions_detail.is_showed,
                admissions_detail.date_show, admissions_detail.link,
                admissions_detail.order, admissions_detail.img,
                admissions.link as admissionsLink, admissions_detail.id_admissions,
                admissions_detail.caption")
            ->join('admissions', 'admissions.id', '=', 'admissions_detail.id_admissions')
            ->where('admissions.is_showed', 1)
            ->where('admissions_detail.is_showed', 1)
            ->orderBy('admissions_detail.order', 'asc')
            ->orderBy('admissions_detail.id', 'desc')
            ->paginate(10)
            ->withPath("/dang-doan-the");

        return view("user/admissions/index")
            ->with('setting', $setting)
            ->with('admissions', $admissions);
    }

    public function list($id = "", $link = "") {
    	if(!$link) {
    		abort(404);
    	}

    	$admissions = Admissions::where('is_showed', 1)
            ->where('id', $id)
            ->where('link', $link)->first();

        if(empty($admissions)) abort(404);

        $admissionsDetail = DB::table('admissions_detail')
            ->selectRaw("admissions_detail.id, admissions_detail.title, admissions.title as admissions_title,
                admissions_detail.short_desc, admissions_detail.content,
                admissions_detail.keywords, admissions_detail.is_showed,
                admissions_detail.date_show, admissions_detail.link,
                admissions_detail.order, admissions_detail.img,
                admissions.link as admissionsLink, admissions_detail.id_admissions,
                admissions_detail.caption")
            ->join('admissions', 'admissions.id', '=', 'admissions_detail.id_admissions')
            ->where('admissions.id', $id)
            ->where('admissions.is_showed', 1)
            ->where('admissions_detail.is_showed', 1)
            ->orderBy('admissions_detail.order', 'asc')
            ->orderBy('admissions_detail.id', 'desc')
            ->paginate(10)
            ->withPath("/dang-doan-the");

    	return view("user/admissions/list")
            ->with('admissions', $admissions)
            ->with('admissionsDetail', $admissionsDetail);
    }

     public function detail($id = "", $link = "") {
        if(!$id && !$link) {
            abort(404);
        }

        $admissions = Admissions::where('is_showed', 1)->where('id', $id)->first();

        if(empty($admissions)) abort(404);

        $admissionsDetail = AdmissionsDetail::where('is_showed', 1)
            ->where('id_admissions', $id)
            ->where('link', $link)
            ->first();

        if(empty($admissionsDetail)) abort(404);

        $relationAdmissions =  AdmissionsDetail::where('is_showed', 1)
            ->where('id_admissions', $id)
            ->where('link', "!=", $link)
            ->limit(5)
            ->get();

        return view("user/admissions/detail")
            ->with('admissions', $admissions)
            ->with('admissionsDetail', $admissionsDetail)
            ->with('relationAdmissions', $relationAdmissions);
    }
}
