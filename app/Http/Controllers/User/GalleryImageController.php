<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\User\BaseController;
use Illuminate\Http\Request;
use App\Models\GalleryImage;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;
use File;

class GalleryImageController extends BaseController
{
    public function index(){
         $setting = Setting::where("module", "image")->first();
        if(empty($setting)) abort(404);

        $gallery = GalleryImage::where('is_showed', 1)
            ->orderBy('order', 'acs')
            ->orderBy('id', 'desc')
            ->paginate(10)
            ->withPath("/thu-vien-anh");
        return view("user/gallery-image/index")
            ->with('setting', $setting)
            ->with('gallery', $gallery);
    }

    public function detail($link = ""){
        $setting = Setting::where("module", "image")->first();
        if(empty($setting)) abort(404);

        $images = null;
        $gallery = GalleryImage::where('is_showed', 1)
                ->where("link", $link)
                ->first();
        if(empty($gallery)) abort(404);

        $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/gallery/image/{$gallery->id}";
        if(is_dir($descriptionPath)) {
            $files = File::allFiles($descriptionPath);
            foreach ($files as $key => $value) {
                $images[] = $value->getFileName();
            }
        }

        return view("user/gallery-image/detail")
            ->with("setting", $setting)
            ->with("gallery", $gallery)
            ->with("images", $images);
    }
}
