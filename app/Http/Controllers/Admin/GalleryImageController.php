<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use App\Models\GalleryImage;
use Image;
use File;

class GalleryImageController extends BaseController
{
    public function index(Request $request){
        $params  = $request->all();
        $isAjax  = (isset($params['is_ajax'])) ? $params['is_ajax'] : false;
        $start   = (!empty($params['start'])) ? $params['start'] : 0;
        $keyword = (!empty($params['keyword'])) ? $params['keyword'] : '';
        $limit   =  (!empty($params['limit'])) ? $params['limit'] : 50;
        $gallery    = GalleryImage::where('title', 'like', "%{$keyword}%")
            ->orderBy('order', 'asc')
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->offset($start)
            ->get();
        if($isAjax) {
            return response()->json(
                 [
                    "recordsFiltered" => count($gallery),
                    "recordsTotal" => count($gallery),
                    "data" => $gallery
                 ], 200
            );
        }
        return view('admin/gallery-image/index')
            ->with('title', "Danh sách hình ảnh");
    }

    public function add(Request $request){
        $params = $request->all();
        $title = "Thêm danh sách hình ảnh";
        $status = false;
        if(!empty($params)) {
            $params['date_show'] = (!empty($params['date_show'])) ? $params['date_show'] : date("Y-m-d");
            $params['title'] = trim($params['title']);
            $params['created_at'] = date("Y-m-d");
            $params['order'] = !empty($params['order']) ? (int) $params['order'] : 0;
            unset($params['_token']);
            if($request->hasFile('img')) {
                $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/gallery/image/";
                $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                    $params['img'] = $fileName;

                    $imgThumb = Image::make($descriptionPath.$fileName);
                    $imgThumb->resize(170, 130, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($descriptionPath.$fileName);
                }
            }
            $status = GalleryImage::insert($params);
        }
        return view('admin/gallery-image/add')
            ->with('title', $title)
            ->with('status', $status);
    }

    public function edit(Request $request, $id){
        $title = 'Sửa danh sách hình ảnh';
        $gallery = GalleryImage::find($id);
        $status = false;
        if(!$id || empty($gallery)) {
           return redirect('/admin/gallery-image');
        }
        $params = $request->all();
        if(!empty($params)) {
            $params['date_show'] = (!empty($params['date_show'])) ? $params['date_show'] : date("Y-m-d");
            $gallery->title       = trim($params['title']);
            $gallery->is_showed   = $params['is_showed'];
            $gallery->link        = $params['link'];
            $gallery->keywords    = $params['keywords'];
            $gallery->description = $params['description'];
            $gallery->short_desc     = $params['short_desc'];
            $gallery->date_show   = $params['date_show'];
            $gallery->order       = !empty($params['order']) ? (int) $params['order'] : 0;
            $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/gallery/image/";
            if($request->hasFile('img')) {
                $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                    if(!empty($gallery->img) && file_exists($descriptionPath.$gallery->img)) {
                        unlink($descriptionPath.$gallery->img);
                    }
                    $imgThumb = Image::make($descriptionPath.$fileName);
                    $imgThumb->resize(170, 130, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($descriptionPath.$fileName);
                    $gallery->img = $fileName;
                }
            } else {
                if( $params['remove_img'] && file_exists($descriptionPath.$gallery->img)) {
                    unlink($descriptionPath.$gallery->img);
                    $gallery->img = "";
                }
            }
            $status = $gallery->save();
        }
        return view('admin/gallery-image/edit')
            ->with('title', $title)
            ->with('gallery', $gallery)
            ->with('status', $status);
    }

    public function del($id){
         if($id){
            $gallery = GalleryImage::where('id', $id)->delete();
            $descriptionPath    = $_SERVER['DOCUMENT_ROOT']."/public/images/gallery/image/";
            if(!empty($gallery->img) && file_exists($descriptionPath.$gallery->img)) {
                unlink($descriptionPath.$gallery->img);

            }
            
            $descriptionPathChild = $descriptionPath.$id;
            if(is_dir($descriptionPathChild)) {
                array_map('unlink', glob("{$descriptionPathChild}/*.*"));
                rmdir($descriptionPathChild);
            }
        }
        return redirect("/admin/gallery-image");
    }

    public function detail(Request $request, $id){
        $title = 'Cập nhật hình ảnh';
        $gallery = GalleryImage::find($id);
        $status = false;
        if(!$id || empty($gallery)) {
           return redirect('/admin/gallery-image');
        }

        return view('admin/gallery-image/detail')
            ->with('title', $title)
            ->with('gallery', $gallery)
            ->with('status', $status)
            ->with('id', $id);
    }

    public function upload(Request $request, $id){
        $status = false;
        $gallery = GalleryImage::find($id);
        if(!$id || empty($gallery)) {
           return response()->json(['status' => $status ], 400 );
        }
        $params = $request->all();
        if($request->hasFile('file')){
            $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/gallery/image/{$id}";
            if(!is_dir($descriptionPath)) {
                mkdir($descriptionPath);
            }
            if(move_uploaded_file($_FILES['file']['tmp_name'], $descriptionPath.'/'.$_FILES['file']['name'])) {
                $status = true;
            }
        }
        return response()->json(['status' => $status ], 200 );
    }

     public function list(Request $request, $id){
        $gallery = GalleryImage::find($id);
        $status = false;
        $images = null;
        if(!$id || empty($gallery)) {
           return response()->json(['status' => $status ], 400 );
        }
        $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/gallery/image/{$id}";
        if(!is_dir($descriptionPath)) {
            mkdir($descriptionPath);
        }
        $files = File::allFiles($descriptionPath);
        foreach ($files as $key => $value) {
            $images[] = $value->getFileName();
        }
        return response()->json(['status' => $status, 'images' => $images ], 200 );
    }
}
