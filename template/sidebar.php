<div class="sidebar">
     <div class="widget features-slide-widget">
        <div class="title-section">
            <h1><span>Tìm kiếm</span></h1>
        </div>
         <div class="slider_sidebar blur">
            <form id="search_news" class="navbar-form navbar-left" role="search">
            <input type="text" id="search" name="search" placeholder="Tìm kiếm tin">
            <button type="submit" id="search-submit"><i class="fa fa-search"></i></button>
            </form>
        </div>
    </div>
   <div class="widget post-widget">
        <div class="title-section">
            <h1><span>Thông báo mới</span></h1>
        </div>
        <div class="slider_sidebar blur">
            <marquee behavior="scroll" direction="up" style="height: 300px" onmouseover="this.stop();" onmouseout="this.start();">
            <ul class="slide_side list-posts">
              <li>
                <img src="http://thethaophunhuan.com.vn/upload/news/80608499_vi.png" alt="">
                <div class="post-content">
                    <h2><a href="#">LỊCH DẠY CÁC LỚP THỂ THAO PHONG TRÀO</a></h2>
                </div>
              </li>
              <li>
                <img src="http://thethaophunhuan.com.vn/upload/news/80608499_vi.png" alt="">
                <div class="post-content">
                    <h2><a href="#">ĐIỀU LỆ GIẢI QUẦN VỢT CÚP PNCo quận 7 NĂM 2019</a></h2>
                </div>
              </li>
              <li>
                <img src="http://thethaophunhuan.com.vn/upload/news/80608499_vi.png" alt="">
                <div class="post-content">
                    <h2><a href="#">Lịch dạy các lớp thể thao hè 2019</a></h2>
                </div>
              </li>
            </ul>
        </marquee>
        </div>
    </div>
    <div class="widget features-slide-widget">
        <div class="title-section">
            <h1><span>THƯ VIỆN ẢNH</span></h1>
        </div>
        <div class="image-post-slider">
            <ul class="bxslider">
                <li>
                    <div class="news-post image-post2">
                        <img src="http://thethaophunhuan.com.vn/upload/hinhanh/962484.jpg" alt="">
                    </div>
                </li>
                <li>
                    <div class="news-post image-post2">
                        <img src="http://thethaophunhuan.com.vn/upload/hinhanh/882965.jpg" alt="">
                    </div>
                </li>
                <li>
                    <div class="news-post image-post2">
                        <img src="http://thethaophunhuan.com.vn/upload/hinhanh/899593.JPG" alt="">
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="widget features-slide-widget">
        <div class="title-section">
            <h1><span>Video</span></h1>
        </div>
        <div>
            <iframe title="" width="100%" src="http://www.youtube.com/embed/pT9arVlVQ08" frameborder="0" allowfullscreen=""></iframe>
        </div>
    </div>
    <div class="widget features-slide-widget">
        <div class="title-section">
            <h1><span>Web Link</span></h1>
        </div>
        <div>
            <select class="form-control" onchange="window.open(this.value,'_Blank');" style="width:100%;height:30px;">
            <option value="">-- Web link --</option>
                     <option value="http://www.phunhuan.hochiminhcity.gov.vn/">UBND quận 7</option>
                     <option value="http://www.bvhttdl.gov.vn/">Bộ Văn hóa Thể thao và Du lịch</option>
                     <option value="http://svhtt.hochiminhcity.gov.vn/">Sở Văn hóa và Thể thao TP.HCM</option>
            </select>
        </div>
    </div>
   <div class="widget post-widget">
        <div class="title-section">
            <h1><span>ĐỐI TÁC, CÁC NHÀ TÀI TRỢ</span></h1>
        </div>
         <marquee behavior="scroll" direction="up" style="height: 300px" onmouseover="this.stop();" onmouseout="this.start();">
          <p class="text-center"> <a href="#"><img src="http://thethaophunhuan.com.vn/upload/hinhanh/8904552.jpg" width="200px" alt=""></a> </p>
          <p class="text-center"> <a href="#"><img src="http://thethaophunhuan.com.vn/upload/hinhanh/8221320.jpg" width="200px" alt=""></a> </p>
          <p class="text-center"> <a href="#"><img src="http://thethaophunhuan.com.vn/upload/hinhanh/8505190.JPG" width="200px" alt=""></a> </p>
          <p class="text-center"> <a href="#"><img src="http://thethaophunhuan.com.vn/upload/hinhanh/6856830.jpg" width="200px" alt=""></a> </p>
          <p class="text-center"> <a href="#"><img src="http://thethaophunhuan.com.vn/upload/hinhanh/6145452.jpg" width="200px" alt=""></a> </p>
          <p class="text-center"> <a href="#"><img src="http://thethaophunhuan.com.vn/upload/hinhanh/9319430.JPG" width="200px" alt=""></a> </p>
          <p class="text-center"> <a href="#"><img src="http://thethaophunhuan.com.vn/upload/hinhanh/764628.png" width="200px" alt=""></a> </p>
          <p class="text-center"> <a href="#"><img src="http://thethaophunhuan.com.vn/upload/hinhanh/6961920.png" width="200px" alt=""></a> </p>
          <p class="text-center"> <a href="#"><img src="http://thethaophunhuan.com.vn/upload/hinhanh/8505191.jpg" width="200px" alt=""></a> </p>
       </marquee>
    </div>
    <div class="widget post-widget">
        <div class="title-section">
            <h1><span>THỐNG KÊ TRUY CẬP</span></h1>
        </div>
        <div>
            <ul>
                <li>Số người đang online: 8</li>
                <li>Truy cập trong hôm nay: 471</li>
                <li>Truy cập trong hôm qua: 400</li>
                <li>Truy cập trong tuan: 1733</li>
                <li>Truy cập trong tháng: 5830</li>
                <li>Lượt truy cập: 669951</li>
            </ul>
        </div>
    </div>
<!--     <div class="advertisement">
        <div class="desktop-advert">
            <span>Banner Quảng cáo</span>
            <img src="images/300x250.jpg" width="100%" alt="">
        </div>
        <div class="tablet-advert">
            <span>Banner Quảng cáo</span>
            <img src="images/200x200.jpg" width="100%" alt="">
        </div>
        <div class="mobile-advert">
            <span>Banner Quảng cáo</span>
            <img src="images/300x250.jpg" width="100%" alt="">
        </div>
    </div> -->
</div>