<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\User\BaseController;
use Illuminate\Http\Request;
use App\Models\News;
use App\Models\Setting;

class NewsController extends BaseController
{
    public function index(){
        $setting = Setting::where("module", "news")->first();
        if(empty($setting)) abort(404);

        $news = News::where('is_showed', 1)
            ->orderBy('order', 'acs')
            ->orderBy('id', 'desc')
            ->paginate(10)
            ->withPath("/tin-tuc");
        return view("user/news/index")
            ->with('setting', $setting)
            ->with('news', $news);
    }

    public function detail($link = "") {
    	if(!$link) {
    		abort(404);
    	}

    	$news = News::where('is_showed', 1)->where('link', $link)->first();

        if(empty($news)) abort(404);

        $lastestNews =  News::where('is_showed', 1)
            ->where('is_new', 1)
            ->where('id','!=', $news->id)
            ->orderBy('order', 'acs')
            ->orderBy('id', 'desc')
            ->limit(5)
            ->get();

    	return view("user/news/detail")
            ->with('news', $news)
            ->with('lastestNews', $lastestNews);
    }

     public function search(Request $request){
        $setting = Setting::where("module", "search")->first();
        if(empty($setting)) abort(404);

        $params = $request->all();
        $keyword = !empty($params['keyword']) ? trim(strip_tags($params['keyword'])) : "";

        $news = News::where('is_showed', 1)
            ->where("title", 'like', "%{$keyword}%")
            ->orderBy('order', 'acs')
            ->orderBy('id', 'desc')
            ->paginate(10)
            ->withPath("/tim-kiem?keyword={$keyword}");

        return view("user/news/search")
            ->with('setting', $setting)
            ->with('news', $news);
    }
}
