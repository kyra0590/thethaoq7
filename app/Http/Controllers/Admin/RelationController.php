<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use App\Models\Relation;

class RelationController extends BaseController
{
    public function index(Request $request){
        $params  = $request->all();
        $isAjax  = (isset($params['is_ajax'])) ? $params['is_ajax'] : false;
        $start   = (!empty($params['start'])) ? $params['start'] : 0;
        $keyword = (!empty($params['keyword'])) ? $params['keyword'] : '';
        $limit   =  (!empty($params['limit'])) ? $params['limit'] : 50;
        $relation    = Relation::where('title', 'like', "%{$keyword}%")
            ->orderBy('order', 'asc')
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->offset($start)
            ->get();
        if($isAjax) {
            return response()->json(
                 [
                    "recordsFiltered" => count($relation),
                    "recordsTotal" => count($relation),
                    "data" => $relation
                 ], 200
            );
        }
        return view('admin/relation/index')
            ->with('title', "Danh sách liên kết");
    }

    public function add(Request $request){
        $params = $request->all();
        $title = "Thêm liên kết";
        $status = false;
        if(!empty($params)) {
            $params['title']      = trim($params['title']);
            $params['created_at'] = date("Y-m-d");
            $params['order']      = !empty($params['order']) ? (int) $params['order'] : 0;
            unset($params['_token']);
            $status = Relation::insert($params);
        }
        return view('admin/relation/add')
            ->with('title', $title)
            ->with('status', $status);
    }

     public function edit(Request $request, $id){
        $title = 'Sửa liên kết';
        $relation = Relation::find($id);
        $status = false;
        if(!$id || empty($relation)) {
           return redirect('/admin/relation');
        }
        $params = $request->all();
        if(!empty($params)) {
            $relation->title       = trim($params['title']);
            $relation->is_showed   = $params['is_showed'];
            $relation->link        = $params['link'];
            $relation->order       = !empty($params['order']) ? (int) $params['order'] : 0;
            $status = $relation->save();
        }
        return view('admin/relation/edit')
            ->with('title', $title)
            ->with('relation', $relation)
            ->with('status', $status);
    }

    public function del($id){
         if($id){
            Relation::where('id', $id)->delete();
        }
        return redirect("/admin/relation");
    }

}
