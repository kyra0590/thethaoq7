<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IntroDetail extends Model
{
	protected $table      = 'intro_detail';
	protected $primaryKey = 'id';
	public $timestamps    = true;
}
