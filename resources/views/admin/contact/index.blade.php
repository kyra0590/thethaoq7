@extends('admin.layout')
@section('title', $title)
@section('content')

<div class="row page-titles">
    <div class="col-md-12 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-success">
                <h4 class="m-b-0 text-white">{{$title}}</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive m-t-20">
                    <form action="" id="frm_data">
                        <table id="dataTable"
                            class="display nowrap table table-hover table-striped table-bordered"
                            cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Họ tên</th>
                                    <th>Điện thoại</th>
                                    <th>Email</th>
                                    <th>Nội dung</th>
                                    <th>Xử lý</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script>
$('#dataTable').DataTable({
    "bSort" : false,
    // "order": [[ 0, "asc" ]],
    "serverSide" : true,
    "paging": true,
    "pageLength": 50,
    "ajax" : {
        url : "/admin/contact?is_ajax=true",
        type: 'POST',
        data : function(d) {
            return $.extend( {}, d, {
                "nums" : d.start,
                "keyword" : d.search['value'],
                "limit" : d.length,
            });
        },
    },
    columns:[
        {
            "width" : "50px",
            "data" : 'id',
            "name"  : "id"
        },
        {
            "width" : "100px",
            "data" : 'fullname',
            "name"  : "fullname"
        },
        {
            "width" : "100px",
            "data" : 'phone',
            "name"  : "phone"
        },
        {
            "width" : "100px",
            "data" : 'email',
            "name"  : "email"
        },
        {
            "width" : "100px",
            "data" : 'content',
            "name"  : "content",
            render: function (data, type, full) {
                return '<textarea class="form-control" width="100%" rows="3">'+data+'</textarea>';
            }
        },
        {   "width" : "100px",
            "aTargets": [4],
            "mData": null,
            render: function (data, type, full) {
                return '<a href="/admin/contact/del/'+ data.id +'" class="btnDelete"><i class="far fa-trash-alt"></i></a>';
            }
        }
    ]
});

</script>
@endsection
