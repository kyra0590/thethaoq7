{{-- Load Layout User --}}
@extends('admin.layout')

{{-- set title --}}
@section('title', $title)

@section('content')
 <!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">{{$title}}</h4>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Info box -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-lg-12">
        <p style="text-align:center">
            <img width="100%" src="/public/images/banner-Thể Thao Quận 7.jpg">
        </p>
    </div>
</div>
@endsection

