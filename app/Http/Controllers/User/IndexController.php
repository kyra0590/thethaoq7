<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\User\BaseController;
use Illuminate\Http\Request;
use App\Models\News;
use App\Models\Home;
use App\Models\Info;
use App\Models\Union;
use Illuminate\Support\Facades\DB;

class IndexController extends BaseController
{
    public function index(){
        $home        = Home::find(1);
        $title       = "Trang chủ";
        $keywords    = "";
        $description = "";
        if(!empty($home)) {
            $home        = $home->first();
            $title       = $home->title;
            $keywords    = $home->keywords;
            $description = $home->description;
        }

        $newsHome = News::where('is_new', 1)
            ->where('is_showed', 1)
            ->orderBy('order', 'asc')
            ->orderBy('id', 'desc')
            ->limit(5)
            ->get();

        $sportHome = DB::table('sport_detail')
            ->selectRaw("sport_detail.id, sport_detail.title, sport.title as sport_title,
                sport_detail.short_desc, sport_detail.content,
                sport_detail.keywords, sport_detail.is_showed,
                sport_detail.date_show, sport_detail.link,
                sport_detail.order, sport_detail.img,
                sport.link as sportLink, sport_detail.id_sport")
            ->join('sport', 'sport.id', '=', 'sport_detail.id_sport')
            ->where('sport.is_showed', 1)
            ->where('sport_detail.is_showed', 1)
            ->where('sport_detail.is_new', 1)
            ->orderBy('sport_detail.order', 'asc')
            ->orderBy('sport_detail.id', 'desc')
            ->limit(5)
            ->get();

        $infoHome = DB::table('info_detail')
            ->selectRaw("info_detail.id, info_detail.title, info.title as info_title,
                info_detail.short_desc, info_detail.content,
                info_detail.keywords, info_detail.is_showed,
                info_detail.date_show, info_detail.link,
                info_detail.order, info_detail.img,
                info.link as infoLink, info_detail.id_info")
            ->join('info', 'info.id', '=', 'info_detail.id_info')
            ->where('info.is_showed', 1)
            ->where('info_detail.is_showed', 1)
            ->where('info_detail.is_new', 1)
            ->orderBy('info_detail.order', 'asc')
            ->orderBy('info_detail.id', 'desc')
            ->limit(5)
            ->get();

        $unionHome = DB::table('union_detail')
            ->selectRaw("union_detail.id, union_detail.title, union.title as union_title,
                union_detail.short_desc, union_detail.content,
                union_detail.keywords, union_detail.is_showed,
                union_detail.date_show, union_detail.link,
                union_detail.order, union_detail.img,
                union.link as unionLink, union_detail.id_union")
            ->join('union', 'union.id', '=', 'union_detail.id_union')
            ->where('union.is_showed', 1)
            ->where('union_detail.is_showed', 1)
            ->where('union_detail.is_new', 1)
            ->orderBy('union_detail.order', 'asc')
            ->orderBy('union_detail.id', 'desc')
            ->limit(5)
            ->get();

         $admissionsHome = DB::table('admissions_detail')
            ->selectRaw("admissions_detail.id, admissions_detail.title, admissions.title as admissions_title,
                admissions_detail.short_desc, admissions_detail.content,
                admissions_detail.keywords, admissions_detail.is_showed,
                admissions_detail.date_show, admissions_detail.link,
                admissions_detail.order, admissions_detail.img,
                admissions.link as admissionsLink, admissions_detail.id_admissions")
            ->join('admissions', 'admissions.id', '=', 'admissions_detail.id_admissions')
            ->where('admissions.is_showed', 1)
            ->where('admissions_detail.is_showed', 1)
            ->where('admissions_detail.is_new', 1)
            ->orderBy('admissions_detail.order', 'asc')
            ->orderBy('admissions_detail.id', 'desc')
            ->limit(5)
            ->get();

        return view('user/index/index')
            ->with('title', $title)
            ->with('keywords', $keywords)
            ->with('home', $home)
            ->with('description', $description)
            ->with('newsHome', $newsHome)
            ->with('sportHome', $sportHome)
            ->with('infoHome', $infoHome)
            ->with('unionHome', $unionHome)
            ->with('admissionsHome', $admissionsHome);
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function refreshCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }

    // public function contact(Request $request){
    //     $params = $request->all();
    //     if(!empty($params)) {
    //         $contact = new Contact();
    //         $contact->fullname = trim(strip_tags($params['fullname']));
    //         $contact->phone = strip_tags($params['phone']);
    //         $contact->email = $params['email'];
    //         $contact->title = trim(strip_tags($params['title']));
    //         $contact->content = trim(strip_tags($params['content']));
    //         $contact->save();
    //         return redirect('/');
    //     }
    //     return view('user/index/contact');
    // }

    // public function about () {
    //     return view('user/index/about');
    // }

    // public function messageCeo() {
    //     return view('user/index/messageCeo');
    // }
}
