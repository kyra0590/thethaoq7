@extends('user.layout')

@section('title', $setting->title)
@section('description', $setting->description)
@section('keywords', $setting->keywords)

@section('content')

<div class="jumbotron text-center">
  <h1 class="display-3">Thank You!</h1>
  <p class="lead"><strong>Cảm ơn bạn đã gửi nội dung liên hệ đến chúng tôi</strong> </p>
  <hr>
  <p class="lead">
    <a class="btn btn-primary btn-sm" href="/" role="button">Trở về trang chủ</a>
  </p>
</div>
@endsection