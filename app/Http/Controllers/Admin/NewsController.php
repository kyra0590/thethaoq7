<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use App\Models\News;
use Image;

class NewsController extends BaseController
{
    public function index(Request $request){
        $params  = $request->all();
        $isAjax  = (isset($params['is_ajax'])) ? $params['is_ajax'] : false;
        $start   = (!empty($params['start'])) ? $params['start'] : 0;
        $keyword = (!empty($params['keyword'])) ? $params['keyword'] : '';
        $limit   =  (!empty($params['limit'])) ? $params['limit'] : 50;
        $news    = News::where('title', 'like', "%{$keyword}%")
            ->orderBy('order', 'asc')
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->offset($start)
            ->get();
        if($isAjax) {
            return response()->json(
                 [
                    "recordsFiltered" => count($news),
                    "recordsTotal" => count($news),
                    "data" => $news
                 ], 200
            );
        }
        return view('admin/news/index')
            ->with('title', "Danh sách tin tức - sự kiện");
    }

    public function add(Request $request){
        $params = $request->all();
        $title = "Thêm tin tức - sự kiện";
        $status = false;
        if(!empty($params)) {
            $params['date_show'] = (!empty($params['date_show'])) ? $params['date_show'] : date("Y-m-d");
            $params['title'] = trim($params['title']);
            $params['created_at'] = date("Y-m-d");
            $params['order'] = !empty($params['order']) ? (int) $params['order'] : 0;
            $params['is_new'] = !empty($params['is_new']) ? (int) $params['is_new'] : 0;
            unset($params['_token']);
            if($request->hasFile('img')) {
                $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/news/";
                $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                    $params['img'] = $fileName;

                    $imgThumb = Image::make($descriptionPath.$fileName);
                    $imgThumb->resize(375, 250, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($descriptionPath.'/thumb/'.$fileName);
                }
            }
            $status = News::insert($params);
        }
        return view('admin/news/add')
            ->with('title', $title)
            ->with('status', $status);
    }

     public function edit(Request $request, $id){
        $title = 'Sửa tin tức - sự kiện';
        $news = News::find($id);
        $status = false;
        if(!$id || empty($news)) {
           return redirect('/admin/news');
        }
        $params = $request->all();
        if(!empty($params)) {
            $params['date_show'] = (!empty($params['date_show'])) ? $params['date_show'] : date("Y-m-d");
            $news->title       = trim($params['title']);
            $news->is_showed   = $params['is_showed'];
            $news->link        = $params['link'];
            $news->keywords    = $params['keywords'];
            $news->description = $params['description'];
            $news->content     = $params['content'];
            $news->short_desc  = $params['short_desc'];
            $news->date_show   = $params['date_show'];
            $news->order       = !empty($params['order']) ? (int) $params['order'] : 0;
            $news->is_new       = !empty($params['is_new']) ? (int) $params['is_new'] : 0;
            $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/news/";
            if($request->hasFile('img')) {
                $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                    if(!empty($news->img)) {
                        if(file_exists($descriptionPath.$news->img)) {
                            unlink($descriptionPath.$news->img);
                        }

                        if(file_exists($descriptionPath.'thumb/'.$news->img))
                        {
                            unlink($descriptionPath.'thumb/'.$news->img);
                        }
                    }
                    $imgThumb = Image::make($descriptionPath.$fileName);
                    $imgThumb->resize(375, 250, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($descriptionPath.'thumb/'.$fileName);
                    $news->img = $fileName;
                }
            } else {
                if( $params['remove_img'] && file_exists($descriptionPath.$news->img)) {
                    unlink($descriptionPath.$news->img);
                    if(file_exists($descriptionPath.'thumb/'.$news->img))
                    {
                        unlink($descriptionPath.'thumb/'.$news->img);
                    }
                    $news->img = "";
                }
            }
            $status = $news->save();
        }
        return view('admin/news/edit')
            ->with('title', $title)
            ->with('news', $news)
            ->with('status', $status);
    }

    public function del($id){
         if($id){
            News::where('id', $id)->delete();
            $descriptionPath    = $_SERVER['DOCUMENT_ROOT']."/public/images/news/";
            if(!empty($news->img) && file_exists($descriptionPath.$news->img)) {
                unlink($descriptionPath.$news->img);
                if(file_exists($descriptionPath.'thumb/'.$news->img))
                {
                    unlink($descriptionPath.'thumb/'.$news->img);
                }
            }
        }
        return redirect("/admin/news");
    }

}
