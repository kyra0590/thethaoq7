<header class="clearfix fifth-style">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="logo-advertisement">
            <div class="container">
                @if(!empty($sliderTop) && count($sliderTop))
                <div id="slider_top" class="show_pc">
                    <div id="slider_nivo" class="nivoSlider">
                        @foreach ($sliderTop as $item)
                            @if (!empty($item->img) && file_exists($imgPath."/slider-top/".$item->img))
                            <img src="/public/images/slider-top/{{$item->img}}" alt="">
                            @endif
                        @endforeach
                    </div>
                      <div class="bg_slider_top">
                        <img src="/public/images/bg_slider_top.png" alt="">
                    </div>
                </div>
                @endif
                <div class="navbar-header show_sp">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="/public/images/logo_thumb.jpg" height="100px" alt="logo"></a>
                </div>
            </div>
        </div>

        <div class="nav-list-container">
            <div class="container">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left">
                        <li><a class="home" href="/">Trang chủ</a>
                        </li>
                        <li class="drop"><a class="tech" href="/gioi-thieu">Giới thiệu</a>
                            @if (!empty($intros) && count($intros))
                            <ul class="dropdown">
                                @foreach ($intros as $item)
                                    <li><a href="/gioi-thieu/{{$item->id}}/{{$item->link}}.html">{{$item->title}}</a></li>
                                @endforeach
                            </ul>
                            @endif
                        </li>
                        <li><a class="travel" href="/tin-tuc">Tin tức - Sự kiện</a></li>
                        <li><a class="sport" href="/bo-mon">Các Bộ môn</a>
                            @if (!empty($sports) && count($sports))
                            <div id="slider_sports" class="megadropdown">
                                <div class="container">
                                    <div class="inner-megadropdown tech-dropdown">
                                        <div class="owl-wrapper">
                                            <h1>Danh sách các bộ môn</h1>
                                            <div class="owl-carousel" data-num="8">
                                                @foreach ($sports as $item)
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        @if (!empty($item->icon) && file_exists($imgPath."/sport/".$item->icon))
                                                        <img src="/public/images/sport/{{$item->icon}}" alt="{{$item->title}}">
                                                        @endif
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="/bo-mon/{{$item->id}}/{{$item->link}}.html">{{$item->title}}</a></h2>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </li>
                        <li class="drop"><a class="fashion" href="#">Thư viện</a>
                            <ul class="dropdown">
                                <li><a href="/thu-vien-video">Video clip</a></li>
                                <li><a href="/thu-vien-anh">Hình ảnh</a></li>
                            </ul>
                        </li>
                        <li class="drop"><a class="video" href="/thong-tin">Thông tin</a>
                            @if (!empty($infos) && count($infos))
                            <ul class="dropdown">
                                @foreach ($infos as $item)
                                    <li><a href="/thong-tin/{{$item->id}}/{{$item->link}}.html">{{$item->title}}</a></li>
                                @endforeach
                            </ul>
                            @endif
                        </li>
                        <li class="drop"><a class="food" href="/dang-doan-the">Đảng - đoàn thể</a>
                            @if (!empty($unions) && count($unions))
                            <ul class="dropdown">
                                @foreach ($unions as $item)
                                    <li><a href="/dang-doan-the/{{$item->id}}/{{$item->link}}.html">{{$item->title}}</a></li>
                                @endforeach
                            </ul>
                            @endif
                        </li>
                         <li class="drop"><a class="travel" href="/chieu-sinh">Chiêu sinh</a>
                            @if (!empty($admissionss) && count($admissionss))
                            <ul class="dropdown">
                                @foreach ($admissionss as $item)
                                    <li><a href="/chieu-sinh/{{$item->id}}/{{$item->link}}.html">{{$item->title}}</a></li>
                                @endforeach
                            </ul>
                            @endif
                        </li>
                        <li>
                            <a class="features" href="/lien-he">Liên hệ</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</header>

@if(!empty($slider) && count($slider))
    <section class="big-slider">
        <div class="container">
            <div class="image-slider">
                <ul class="big-bxslider">
                    @foreach($slider as $item)
                        @if (!empty($item->img) && file_exists($imgPath."/slider/".$item->img))
                            <li>
                                <div class="news-post image-post2">
                                    <div class="post-gallery">
                                        @if($item->link != "")
                                        <a href="{{ $item->link }}" target="{{$item->target ? '_blank' : '_self' }}">
                                            <img src="/public/images/slider/{{$item->img}}" alt="{{$item->title}}">
                                        </a>
                                        @else
                                            <img src="/public/images/slider/{{$item->img}}" alt="{{$item->title}}">
                                        @endif
                                    </div>
                                </div>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </section>
@endif