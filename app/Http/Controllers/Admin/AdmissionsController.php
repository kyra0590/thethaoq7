<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use App\Models\Admissions;
use App\Models\AdmissionsDetail;
use Illuminate\Support\Facades\DB;
use Image;

class AdmissionsController extends BaseController
{
    public function index(Request $request){
        $params = $request->all();
        $isAjax = (isset($params['is_ajax'])) ? $params['is_ajax'] : false;
        $start = (!empty($params['start'])) ? $params['start'] : 0;
        $keyword = (!empty($params['keyword'])) ? $params['keyword'] : '';
        $limit =  (!empty($params['limit'])) ? $params['limit'] : 50;
        $admissions = Admissions::where('title', 'like', "%{$keyword}%")
            ->orderBy('order', 'asc')
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->offset($start)
            ->get();
        if($isAjax) {
            return response()->json(
                 [
                "recordsFiltered" => count($admissions),
                "recordsTotal" => count($admissions),
                "data" => $admissions
            ], 200

                );
        }
        return view('admin/admissions/index')
            ->with('title', "Danh sách chiêu sinh");
    }

    public function add(Request $request){
        $params = $request->all();
        $title = "Thêm chiêu sinh";
        $status = false;
        if(!empty($params)) {
            $params['title'] = trim($params['title']);
            $params['created_at'] = date("Y-m-d");
            $params['order'] = !empty($params['order']) ? (int) $params['order'] : 0;
            unset($params['_token']);
            $status = Admissions::insert($params);
        }
        return view('admin/admissions/add')
            ->with('title', $title)
            ->with('status', $status);
    }

     public function edit(Request $request, $id = ""){
        $title = 'Sửa chiêu sinh';
        $admissions = Admissions::find($id);
        $status = false;
        if(!$id || empty($admissions)) {
           return redirect('/admin/admissions');
        }
        $params = $request->all();
        if(!empty($params)) {
            $admissions->title        = trim($params['title']);
            $admissions->is_showed    = $params['is_showed'];
            $admissions->link         = $params['link'];
            $admissions->keywords     = $params['keywords'];
            $admissions->description  = $params['description'];
            $admissions->order        = !empty($params['order']) ? (int) $params['order'] : 0;
            $admissions->is_show_navi = !empty($params['is_show_navi']) ? (int) $params['is_show_navi'] : 0;
            $status              = $admissions->save();
        }
        return view('admin/admissions/edit')
            ->with('title', $title)
            ->with('admissions', $admissions)
            ->with('status', $status);
    }

    public function del($id){
         if($id){
            Admissions::where('id', $id)->delete();
            AdmissionsDetail::where('id_admissions', $id)->update(['id_admissions' => 0, 'is_showed' => 0]);
        }
        return redirect("/admin/admissions");
    }

    public function detail(Request $request){
        $title   = "Danh sách tin tức chiêu sinh";
        $params  = $request->all();
        $idAdmissions = (!empty($params['idAdmissions'])) ? trim($params['idAdmissions']) : '';
        $isAjax  = (isset($params['is_ajax'])) ? $params['is_ajax'] : false;
        $start   = (!empty($params['start'])) ? $params['start'] : 0;
        $keyword = (!empty($params['keyword'])) ? $params['keyword'] : '';
        $limit   =  (!empty($params['limit'])) ? $params['limit'] : 50;
        $admissionsDetail = DB::table('admissions_detail')
            ->selectRaw("admissions_detail.id, admissions_detail.title,
                admissions.title as admissions_title,
                admissions_detail.short_desc, admissions_detail.content,
                admissions_detail.keywords, admissions_detail.is_showed,
                admissions_detail.date_show, admissions_detail.link,
                admissions_detail.order, admissions_detail.img,
                admissions_detail.is_new")
            ->join('admissions', 'admissions.id', '=', 'admissions_detail.id_admissions')
            ->where('admissions_detail.title', 'like', "%{$keyword}%")
            ->orderBy('admissions_detail.order', 'asc')
            ->orderBy('admissions_detail.id', 'desc')
            ->limit($limit)
            ->offset($start);
        if($idAdmissions != "") {
            $admissionsDetail->where('admissions_detail.id_admissions', $idAdmissions);
            $admissions = Admissions::find($idAdmissions);
            if(!empty($admissions)) {
                $admissions = $admissions->first();
                $title .= " - ". $admissions->title;
            }
        }
        $admissionsDetail = $admissionsDetail->get();
        if($isAjax) {
            return response()->json(
            [
                "recordsFiltered" => count($admissionsDetail),
                "recordsTotal"    => count($admissionsDetail),
                "data"            => $admissionsDetail
            ], 200 );
        }
        return view('admin/admissions/detail')
            ->with("admissionsDetail", $admissionsDetail)
            ->with('idAdmissions', $idAdmissions)
            ->with('title', $title);
    }

     public function addDetail(Request $request){
        $title   = "Thêm tin chiêu sinh";
        $params  = $request->all();
        $status  = false;
        $idAdmissions = '';
        $listAdmissions = Admissions::orderBy('order', 'ASC')->orderBy("id","DESC")->get()->keyBy("id")->toArray();
        if(!empty($params)) {
            $idAdmissions = (!empty($params['idAdmissions'])) ? trim($params['idAdmissions']) : '';
            if($idAdmissions != '') {
                $admissions = Admissions::find($idAdmissions)->first();
                $title .= " - ". $admissions->title;
            }
            if(!empty($params['_token'])) {
                $params['title'] = trim($params['title']);
                $params['caption'] = trim($params['caption']);
                $params['created_at'] = date("Y-m-d");
                $params['order'] = (!empty($params['order'])) ? (int) $params['order'] : 0;
                $params['is_new'] = (!empty($params['is_new'])) ? (int) $params['is_new'] : 0;
                $params['date_show'] = (!empty($params['date_show'])) ? $params['date_show'] : date("Y-m-d");
                unset($params['_token']);
                if($request->hasFile('img')) {
                    $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/admissions/";
                    $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                    if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                        $params['img'] = $fileName;

                        $imgThumb = Image::make($descriptionPath.$fileName);
                        $imgThumb->resize(375, 250, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($descriptionPath.'thumb/'.$fileName);
                    }
                }
                $status = AdmissionsDetail::insert($params);
            }
        }
        return view('admin/admissions/add-detail')
            ->with('title', $title)
            ->with('idAdmissions', $idAdmissions)
            ->with('listAdmissions', $listAdmissions)
            ->with('status', $status);
    }

    public function editDetail(Request $request, $id = "") {
        $title       = 'Sửa tin chiêu sinh';
        $params      = $request->all();
        $status      = false;
        $idAdmissions     = '';
        $listAdmissions   = Admissions::orderBy('order', 'ASC')->orderBy("id","DESC")->get()->keyBy("id")->toArray();
        $admissionsDetail = AdmissionsDetail::find($id);
        if(!$id || empty($admissionsDetail)) {
            return redirect("/admin/admissions-detail?idAdmissions={$admissionsDetail->id_admissions}");
        }
        if(!empty($params)) {
            $idAdmissions = (!empty($params['idAdmissions'])) ? trim($params['idAdmissions']) : '';
            if($idAdmissions != '') {
                $admissions = Admissions::find($idAdmissions)->first();
                $title .= " - ". $admissions->title;
            }
            if(!empty($params['_token'])) {
                $admissionsDetail->title       = trim($params['title']);
                $admissionsDetail->caption     = trim($params['caption']);
                $admissionsDetail->id_admissions    = $params['id_admissions'];
                $admissionsDetail->is_showed   = $params['is_showed'];
                $admissionsDetail->link        = $params['link'];
                $admissionsDetail->keywords    = $params['keywords'];
                $admissionsDetail->description = $params['description'];
                $admissionsDetail->date_show   = $params['date_show'];
                $admissionsDetail->content     = $params['content'];
                $admissionsDetail->short_desc     = $params['short_desc'];
                $admissionsDetail->order       = !empty($params['order']) ? (int) $params['order'] : 0;
                $admissionsDetail->is_new       = !empty($params['is_new']) ? (int) $params['is_new'] : 0;
                $descriptionPath    = $_SERVER['DOCUMENT_ROOT']."/public/images/admissions/";
                if($request->hasFile('img')) {
                    $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                    if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                        if(!empty($admissionsDetail->img)) {
                            if(file_exists($descriptionPath.$admissionsDetail->img)) {
                                unlink($descriptionPath.$admissionsDetail->img);
                            }

                            if(file_exists($descriptionPath.'thumb/'.$admissionsDetail->img))
                            {
                                unlink($descriptionPath.'thumb/'.$admissionsDetail->img);
                            }
                        }

                        $imgThumb = Image::make($descriptionPath.$fileName);
                        $imgThumb->resize(375, 250, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($descriptionPath.'thumb/'.$fileName);

                        $admissionsDetail->img = $fileName;
                    }
                } else {
                    if( $params['remove_img'] && file_exists($descriptionPath.$admissionsDetail->img)) {
                        unlink($descriptionPath.$admissionsDetail->img);
                        if(file_exists($descriptionPath.'thumb/'.$admissionsDetail->img))
                        {
                            unlink($descriptionPath.'thumb/'.$admissionsDetail->img);
                        }
                        $admissionsDetail->img = "";
                    }
                }
                $status = $admissionsDetail->save();
            }
        }
        return view('admin/admissions/edit-detail')
            ->with('title', $title)
            ->with('admissionsDetail', $admissionsDetail)
            ->with('idAdmissions', $idAdmissions)
            ->with('listAdmissions', $listAdmissions)
            ->with('status', $status);
    }

    public function delDetail(Request $request, $id = ""){
        $params = $request->all();
        $idAdmissions = (!empty($params['idAdmissions'])) ? trim($params['idAdmissions']) : '';
        if($id){
            $admissionsDetail = AdmissionsDetail::find($id);
            $descriptionPath    = $_SERVER['DOCUMENT_ROOT']."/public/images/admissions/";
            if(!empty($admissionsDetail->img) && file_exists($descriptionPath.$admissionsDetail->img)) {
                unlink($descriptionPath.$admissionsDetail->img);
                if(file_exists($descriptionPath.'thumb/'.$admissionsDetail->img))
                {
                    unlink($descriptionPath.'/thumb/'.$admissionsDetail->img);
                }
            }
            $admissionsDetail->delete();
        }
        return redirect("/admin/admissions-detail?idAdmissions={$idAdmissions}");
    }
}
