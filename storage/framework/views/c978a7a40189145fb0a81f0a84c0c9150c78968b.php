<!doctype html>
<html lang="vn" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?php echo $__env->yieldContent("title"); ?></title>
    <meta name="description" content="<?php echo $__env->yieldContent('description'); ?>">
    <meta name="keywords" content="<?php echo $__env->yieldContent('keywords'); ?>">
    <?php echo $__env->make("user.css", \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent("css"); ?>
</head>
<body>
    <div id="container">
        <?php echo $__env->make("user.header", \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <section class="block-wrapper">
            <div class="container shadow-white">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="block-content">
                             <?php echo $__env->yieldContent("content"); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $__env->make("user.sidebar", \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>
                </div>
            </div>
        </section>
        <?php echo $__env->make("user.footer", \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
    <?php echo $__env->make("user.script", \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent("js"); ?>
</body>
</html>