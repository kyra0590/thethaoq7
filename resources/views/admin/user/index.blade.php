@extends('admin.layout')
@section('title', $title)
@section('content')

<div class="row page-titles">
    <div class="col-md-12 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <button type="button" class="btn btn-info d-none d-lg-block m-l-15" onclick="location.href ='/admin/user/add'"><i
                    class="fa fa-plus-circle"></i> Thêm</button>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-success">
                <h4 class="m-b-0 text-white">{{$title}}</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive m-t-20">
                    <form action="" id="frm_data">
                        <input type="hidden" name='table_name' value="intro">
                        <table id="dataTable"
                            class="display nowrap table table-hover table-striped table-bordered"
                            cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Khóa</th>
                                    <th>Tên tài khoản</th>
                                    <th>Họ tên</th>
                                    <th>Email</th>
                                    <th>Quyền</th>
                                    <th>Xử lý</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script>
$('#dataTable').DataTable({
    "bSort" : false,
    // "order": [[ 0, "asc" ]],
    "serverSide" : true,
    "paging": true,
    "pageLength": 50,
    "ajax" : {
        url : "/admin/user?is_ajax=true",
        type: 'POST',
        data : function(d) {
           return $.extend( {}, d, {
                "nums" : d.start,
                "keyword" : d.search['value'],
                "limit" : d.length,
            });
        },
    },
    columns:[
        {
            "width" : "50px",
            "data" : 'id',
            "name"  : "id"
        },
         { "data" : 'block' , "name" : "block", "width": "100px",
            render: function ( data, type, row ) {
                if(data) {
                    return "<p style='text-align:center'><i class='fa fa-lock'></i></p>";
                }
                return "<p style='text-align:center'><i class='fa fa-unlock'></i></p>";
            }
        },
        { "data" : 'username' , "name" : "username",
            render: function ( data, type, row ) {
                return data;
            }
        },
         { "data" : 'fullname' , "name" : "fullname",
            render: function ( data, type, row ) {
                return data;
            }
        },
         { "data" : 'email' , "name" : "email",
            render: function ( data, type, row ) {
                return data;
            }
        },
        { "data" : 'permission' , "name" : "permission", "width": "100px",
            render: function ( data, type, row ) {
                if(data) {
                    return "<p style='text-align:center'><i class='fa fa-crown'></i></p>";
                }
                return "<p style='text-align:center'><i class='fa fa-user'></i></p>";
            }
        },
        {   "width" : "100px",
            "aTargets": [4],
            "mData": null,
            render: function (data, type, full) {
                return '<a href="/admin/user/edit/'+data.id+'"><i class="far fa-edit"></i></a> | '+
                    '<a href="/admin/user/del/'+ data.id +'" class="btnDelete"><i class="far fa-trash-alt"></i></a>';
            }
        }
    ]
});
</script>
@endsection