@extends('user.layout')

@section('title', $union->title)
@section('description', $union->description)
@section('keywords', $union->keywords)

@section('content')
<div class="grid-box">
    <div class="single-post-box">
        <div class="title-post">
            <h1>{{$unionDetail->title}}</h1>
             <ul class="post-tags">
                <li><i class="fa fa-clock-o"></i>{{date("d M Y", strtotime($unionDetail->date_show))}}</li>
            </ul>
        </div>
        <div class="post-gallery">
            @if (!empty($unionDetail->img) && file_exists($imgPath."/union/".$unionDetail->img))
            <img src="/public/images/union/{{$unionDetail->img}}" alt="{{$unionDetail->caption}}">
            <span class="image-caption">{{$unionDetail->caption}}</span>
            @endif
        </div>
        <div class="post-content">
            {!! $unionDetail->content  !!}
        </div>
        @if(!empty($relationUnion) && count($relationUnion))
        <div class="col-md-12">
            <div class="title-section">
                <h1><span class="world">TIN LIÊN QUAN</span></h1>
            </div>
            <ul class="list-posts">
                @foreach($relationUnion as $item)
                <li style="border:none">
                    <div class="post-content">
                        <h2><a href="/dang-doan-the/{{$item->id_union}}-{{$item->link}}.html">»» {{$item->title}}</a></h2>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>
@endsection