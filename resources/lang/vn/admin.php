    <?php
return [
    'homepage'           => 'Trang chủ',
    'partner'            => 'Đối tác',
    'contact'            => 'Liên Hệ',
    'manage_page'        => 'Quản lý trang',
    'news'               => 'Tin tức',
    'news_type'          => 'Loại tin tức',
    'menu_top'           => 'Điều hướng top',
    'manage_user'        => 'Quản lý user',
    'logout'             => 'Đăng xuất',
    'my_profile'         => 'Thông tin chung',
    'add_successfull'    => 'Thêm thành công',
    'edit_successfull'   => 'Sửa thành công',
    'update_successfull' => 'Cập nhật thành công',
    'update_failed'      => 'Cập nhật thất bại',
    'update_order'       => 'Cập nhật vị trí',
    'add_user'           => 'Thêm người dùng',
    'edit_user'          => 'Sửa người dùng',
    'add_partner'        => 'Thêm đối tác',
    'edit_partner'       => 'Sửa đối tác',
    'add_slider'         => 'Thêm Slider',
    'edit_slider'        => 'Sửa Slider',
    'edit_home'          => 'Sửa trang chủ',
    'add_type_of_news'   => 'Thêm loại tin tức',
    'edit_type_of_news'  => 'Sửa loại tin tức',
    'add_news'           => "Thêm tin tức",
    'edit_news'          => 'Sửa tin tức',
    'edit_page'          => 'Sửa trang',
    'edit_menu_top'      => 'Sửa điều hướng top',
    'add_page' => 'Thêm trang',
];