<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use App\Models\Info;
use App\Models\InfoDetail;
use Illuminate\Support\Facades\DB;
use Image;

class InfoController extends BaseController
{
    public function index(Request $request){
        $params = $request->all();
        $isAjax = (isset($params['is_ajax'])) ? $params['is_ajax'] : false;
        $start = (!empty($params['start'])) ? $params['start'] : 0;
        $keyword = (!empty($params['keyword'])) ? $params['keyword'] : '';
        $limit =  (!empty($params['limit'])) ? $params['limit'] : 50;
        $info = Info::where('title', 'like', "%{$keyword}%")
            ->orderBy('order', 'asc')
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->offset($start)
            ->get();
        if($isAjax) {
            return response()->json(
                 [
                "recordsFiltered" => count($info),
                "recordsTotal" => count($info),
                "data" => $info
            ], 200

                );
        }
        return view('admin/info/index')
            ->with('title', "Danh sách thông tin");
    }

    public function add(Request $request){
        $params = $request->all();
        $title = "Thêm thông tin";
        $status = false;
        if(!empty($params)) {
            $params['title'] = trim($params['title']);
            $params['created_at'] = date("Y-m-d");
            $params['order'] = !empty($params['order']) ? (int) $params['order'] : 0;
            unset($params['_token']);
            $status = Info::insert($params);
        }
        return view('admin/info/add')
            ->with('title', $title)
            ->with('status', $status);
    }

     public function edit(Request $request, $id){
        $title = 'Sửa thông tin';
        $info = Info::find($id);
        $status = false;
        if(!$id || empty($info)) {
           return redirect('/admin/info');
        }
        $params = $request->all();
        if(!empty($params)) {
            $info->title        = trim($params['title']);
            $info->is_showed    = $params['is_showed'];
            $info->link         = $params['link'];
            $info->keywords     = $params['keywords'];
            $info->description  = $params['description'];
            $info->order        = !empty($params['order']) ? (int) $params['order'] : 0;
            $info->is_show_navi = !empty($params['is_show_navi']) ? (int) $params['is_show_navi'] : 0;
            $status              = $info->save();
        }
        return view('admin/info/edit')
            ->with('title', $title)
            ->with('info', $info)
            ->with('status', $status);
    }

    public function del($id){
         if($id){
            Info::where('id', $id)->delete();
            InfoDetail::where('id_info', $id)->update(['id_info' => 0, 'is_showed' => 0]);
        }
        return redirect("/admin/info");
    }

    public function detail(Request $request){
        $title   = "Danh sách tin tức thông tin";
        $params  = $request->all();
        $idInfo = (!empty($params['idInfo'])) ? trim($params['idInfo']) : '';
        $isAjax  = (isset($params['is_ajax'])) ? $params['is_ajax'] : false;
        $start   = (!empty($params['start'])) ? $params['start'] : 0;
        $keyword = (!empty($params['keyword'])) ? $params['keyword'] : '';
        $limit   =  (!empty($params['limit'])) ? $params['limit'] : 50;
        $infoDetail = DB::table('info_detail')
            ->selectRaw("info_detail.id, info_detail.title,
                info.title as info_title,
                info_detail.short_desc, info_detail.content,
                info_detail.keywords, info_detail.is_showed,
                info_detail.date_show, info_detail.link,
                info_detail.order, info_detail.img,
                info_detail.is_new")
            ->join('info', 'info.id', '=', 'info_detail.id_info')
            ->where('info_detail.title', 'like', "%{$keyword}%")
            ->orderBy('info_detail.order', 'asc')
            ->orderBy('info_detail.id', 'desc')
            ->limit($limit)
            ->offset($start);
        if($idInfo != "") {
            $infoDetail->where('info_detail.id_info', $idInfo);
            $info = Info::find($idInfo);
            if(!empty($info)) {
                $info = $info->first();
                $title .= " - ". $info->title;
            }
        }
        $infoDetail = $infoDetail->get();
        if($isAjax) {
            return response()->json(
            [
                "recordsFiltered" => count($infoDetail),
                "recordsTotal"    => count($infoDetail),
                "data"            => $infoDetail
            ], 200 );
        }
        return view('admin/info/detail')
            ->with("infoDetail", $infoDetail)
            ->with('idInfo', $idInfo)
            ->with('title', $title);
    }

     public function addDetail(Request $request){
        $title   = "Thêm tin thông tin";
        $params  = $request->all();
        $status  = false;
        $idInfo = '';
        $listInfo = Info::orderBy('order', 'ASC')->orderBy("id","DESC")->get()->keyBy("id")->toArray();
        if(!empty($params)) {
            $idInfo = (!empty($params['idInfo'])) ? trim($params['idInfo']) : '';
            if($idInfo != '') {
                $info = Info::find($idInfo)->first();
                $title .= " - ". $info->title;
            }
            if(!empty($params['_token'])) {
                $params['title'] = trim($params['title']);
                $params['caption'] = trim($params['caption']);
                $params['created_at'] = date("Y-m-d");
                $params['order'] = (!empty($params['order'])) ? (int) $params['order'] : 0;
                $params['is_new'] = (!empty($params['is_new'])) ? (int) $params['is_new'] : 0;
                $params['date_show'] = (!empty($params['date_show'])) ? $params['date_show'] : date("Y-m-d");
                unset($params['_token']);
                if($request->hasFile('img')) {
                    $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/info/";
                    $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                    if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                        $params['img'] = $fileName;

                        $imgThumb = Image::make($descriptionPath.$fileName);
                        $imgThumb->resize(375, 250, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($descriptionPath.'thumb/'.$fileName);
                    }
                }
                $status = InfoDetail::insert($params);
            }
        }
        return view('admin/info/add-detail')
            ->with('title', $title)
            ->with('idInfo', $idInfo)
            ->with('listInfo', $listInfo)
            ->with('status', $status);
    }

    public function editDetail(Request $request, $id) {
        $title       = 'Sửa tin thông tin';
        $params      = $request->all();
        $status      = false;
        $idInfo     = '';
        $listInfo   = Info::orderBy('order', 'ASC')->orderBy("id","DESC")->get()->keyBy("id")->toArray();
        $infoDetail = InfoDetail::find($id);
        if(!$id || empty($infoDetail)) {
            return redirect("/admin/info-detail?idInfo={$infoDetail->id_info}");
        }
        if(!empty($params)) {
            $idInfo = (!empty($params['idInfo'])) ? trim($params['idInfo']) : '';
            if($idInfo != '') {
                $info = Info::find($idInfo)->first();
                $title .= " - ". $info->title;
            }
            if(!empty($params['_token'])) {
                $infoDetail->title       = trim($params['title']);
                $infoDetail->caption     = trim($params['caption']);
                $infoDetail->id_info    = $params['id_info'];
                $infoDetail->is_showed   = $params['is_showed'];
                $infoDetail->link        = $params['link'];
                $infoDetail->keywords    = $params['keywords'];
                $infoDetail->description = $params['description'];
                $infoDetail->date_show   = $params['date_show'];
                $infoDetail->content     = $params['content'];
                $infoDetail->short_desc     = $params['short_desc'];
                $infoDetail->order       = !empty($params['order']) ? (int) $params['order'] : 0;
                $infoDetail->is_new       = !empty($params['is_new']) ? (int) $params['is_new'] : 0;
                $descriptionPath    = $_SERVER['DOCUMENT_ROOT']."/public/images/info/";
                if($request->hasFile('img')) {
                    $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                    if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                        if(!empty($infoDetail->img)) {
                            if(file_exists($descriptionPath.$infoDetail->img)) {
                                unlink($descriptionPath.$infoDetail->img);
                            }

                            if(file_exists($descriptionPath.'thumb/'.$infoDetail->img))
                            {
                                unlink($descriptionPath.'thumb/'.$infoDetail->img);
                            }
                        }

                        $imgThumb = Image::make($descriptionPath.$fileName);
                        $imgThumb->resize(375, 250, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($descriptionPath.'thumb/'.$fileName);

                        $infoDetail->img = $fileName;
                    }
                } else {
                    if( $params['remove_img'] && file_exists($descriptionPath.$infoDetail->img)) {
                        unlink($descriptionPath.$infoDetail->img);
                        if(file_exists($descriptionPath.'thumb/'.$infoDetail->img))
                        {
                            unlink($descriptionPath.'thumb/'.$infoDetail->img);
                        }
                        $infoDetail->img = "";
                    }
                }
                $status = $infoDetail->save();
            }
        }
        return view('admin/info/edit-detail')
            ->with('title', $title)
            ->with('infoDetail', $infoDetail)
            ->with('idInfo', $idInfo)
            ->with('listInfo', $listInfo)
            ->with('status', $status);
    }

    public function delDetail(Request $request, $id){
        $params = $request->all();
        $idInfo = (!empty($params['idInfo'])) ? trim($params['idInfo']) : '';
        if($id){
            $infoDetail = InfoDetail::find($id);
            $descriptionPath    = $_SERVER['DOCUMENT_ROOT']."/public/images/info/";
            if(!empty($infoDetail->img) && file_exists($descriptionPath.$infoDetail->img)) {
                unlink($descriptionPath.$infoDetail->img);
                if(file_exists($descriptionPath.'thumb/'.$infoDetail->img))
                {
                    unlink($descriptionPath.'/thumb/'.$infoDetail->img);
                }
            }
            $infoDetail->delete();
        }
        return redirect("/admin/info-detail?idInfo={$idInfo}");
    }
}
