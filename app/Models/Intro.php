<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Intro extends Model
{
	protected $table      = 'intro';
	protected $primaryKey = 'id';
	public $timestamps    = true;
}
