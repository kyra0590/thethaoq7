<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\Menu;

class SiteController extends BaseController
{
	public function index(){
	    $page = Page::orderBy("id","DESC")->get();
        return view('admin/site/index')
            ->with("page", $page);
    }

    public function add(Request $request){
        $params = $request->all();
        $status = false;
        if(!empty($params)) {
            $params['date_show'] = (!empty($params['date_show'])) ? $params['date_show'] : date("Y-m-d");
            $params['title'] = trim($params['title']);
            $params['created_at'] = date("Y-m-d");
            $params['link'] = trim($params['link']);
            unset($params['_token']);
            if($request->hasFile('img')) {
                $img = $request->img;
                $descriptionPath = $this->img_path['page'];
                $fileName = strtotime('now').".{$img->extension()}";
                if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                    $params['img'] = $fileName;
                }
            }
            $status = Page::insert($params);
        }
        return view('admin/site/add')
            ->with('status', $status);
    }

    public function edit(Request $request, $id) {
        if(!$id) {
           abort(404);
        }
        $params = $request->all();
        $status = false;
        $page = Page::find($id);
        if(!empty($params)) {
            $params['date_show'] = (!empty($params['date_show'])) ? $params['date_show'] : date("Y-m-d");
            $page->title       = trim($params['title']);
            $page->is_showed   = $params['is_showed'];
            $page->date_show   = $params['date_show'];
            $page->content     = $params['content'];
            $page->keywords    = $params['keywords'];
            $page->description = $params['description'];
            $page->link = trim($params['link']);
            $descriptionPath = $this->img_path['page'];
            if($request->hasFile('img')) {
                $img = $request->img;
                $fileName = strtotime('now').".{$img->extension()}";
                if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                    if(!empty($page->img) && file_exists($descriptionPath.$page->img)) {
                        unlink($descriptionPath.$page->img);
                    }
                    $page->img= $fileName;
                }
            } else {
                if( $params['remove_img'] && file_exists($descriptionPath.$page->img)) {
                    unlink($descriptionPath.$page->img);
                    $page->img = "";
                }
            }
            $status = $page->save();
        }
        return view('admin/site/edit')
            ->with('status', $status)
            ->with('page', $page);
    }

    public function del($id){
        if($id){
            $page = Page::find($id);
            $descriptionPath = $this->img_path['page'];
            if(!empty($page->img) && file_exists($descriptionPath.$page->img)) {
                unlink($descriptionPath.$page->img);
            }
            $page->delete();
        }
        return redirect("/admin/site");
    }

    public function menu(Request $request){
        $menu = Menu::find(1);
        $status = false;
        $params = $request->all();
        if(!empty($params)) {
            $menu->data = $params['data'];
            $status = $menu->save();
        }
        return view('admin/site/menu')
            ->with('menu', $menu)
            ->with('status', $status);
    }
}
