<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use App\Models\Contact;

class ContactController extends BaseController
{
    public function index(Request $request){
        $params  = $request->all();
        $isAjax  = (isset($params['is_ajax'])) ? $params['is_ajax'] : false;
        $start   = (!empty($params['start'])) ? $params['start'] : 0;
        $keyword = (!empty($params['keyword'])) ? $params['keyword'] : '';
        $limit   =  (!empty($params['limit'])) ? $params['limit'] : 50;
        $contact    = Contact::where('content', 'like', "%{$keyword}%")
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->offset($start)
            ->get();
        if($isAjax) {
            return response()->json(
                 [
                    "recordsFiltered" => count($contact),
                    "recordsTotal" => count($contact),
                    "data" => $contact
                 ], 200
            );
        }
        return view('admin/contact/index')
            ->with('title', "Danh sách liên hệ");
    }

    public function del($id = ""){
        if($id){
            Contact::where('id', $id)->delete();
        }
        return redirect("/admin/contact");
    }

}
