-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.14-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for thethao


-- Dumping structure for table thethao.admissions
CREATE TABLE IF NOT EXISTS `admissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_showed` tinyint(4) NOT NULL DEFAULT 0,
  `is_show_navi` tinyint(4) NOT NULL DEFAULT 0,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.admissions: 1 rows
/*!40000 ALTER TABLE `admissions` DISABLE KEYS */;
INSERT INTO `admissions` (`id`, `title`, `keywords`, `description`, `is_showed`, `is_show_navi`, `link`, `order`, `created_at`, `updated_at`) VALUES
	(30, 'Chiêu sinh khóa 2019 - 2020', NULL, NULL, 1, 1, NULL, 0, '2020-06-10 00:00:00', '2020-06-10 13:48:06');
/*!40000 ALTER TABLE `admissions` ENABLE KEYS */;

-- Dumping structure for table thethao.admissions_detail
CREATE TABLE IF NOT EXISTS `admissions_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_admissions` int(11) DEFAULT NULL,
  `short_desc` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_showed` tinyint(4) NOT NULL DEFAULT 0,
  `is_new` tinyint(4) NOT NULL DEFAULT 0,
  `date_show` date DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.admissions_detail: 1 rows
/*!40000 ALTER TABLE `admissions_detail` DISABLE KEYS */;
INSERT INTO `admissions_detail` (`id`, `title`, `id_admissions`, `short_desc`, `content`, `keywords`, `description`, `is_showed`, `is_new`, `date_show`, `link`, `order`, `img`, `caption`, `created_at`, `updated_at`) VALUES
	(29, 'Chieu sinh judo, takendo, da banh 3-4/2020', 30, 'Chieu sinh judo, takendo, da banh 3-4/2020', '<p>Chieu sinh judo, takendo, da banh 3-4/2020</p>', 'Chieu sinh judo, takendo, da banh 3-4/2020', 'asda', 1, 1, '2020-06-10', 'a2a2', 0, '5ee082adb8c3e1591771821.jpeg', 'asdasd', '2020-06-10 00:00:00', '2020-06-10 06:51:01');
/*!40000 ALTER TABLE `admissions_detail` ENABLE KEYS */;

-- Dumping structure for table thethao.contact
CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.contact: ~0 rows (approximately)
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` (`id`, `fullname`, `phone`, `email`, `content`, `updated_at`, `created_at`) VALUES
	(3, 'Len Yates', '12312312312', 'henax@mailinator.net', 'Assumenda officia ut', '2020-01-11 17:29:41', '2020-01-11 17:29:41'),
	(4, 'Aurelia Ortega', '123123', 'hodab@mailinator.com', 'Quibusdam deleniti d', '2020-06-15 17:39:08', '2020-06-15 10:39:08');
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;

-- Dumping structure for table thethao.gallery_image
CREATE TABLE IF NOT EXISTS `gallery_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_desc` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_showed` tinyint(4) DEFAULT 0,
  `is_show_navi` tinyint(4) DEFAULT 0,
  `date_show` date DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.gallery_image: 2 rows
/*!40000 ALTER TABLE `gallery_image` DISABLE KEYS */;
INSERT INTO `gallery_image` (`id`, `title`, `short_desc`, `keywords`, `description`, `is_showed`, `is_show_navi`, `date_show`, `link`, `order`, `img`, `created_at`, `updated_at`) VALUES
	(23, 'sda', NULL, NULL, NULL, 1, 1, '2020-06-02', 'a', 0, '5ed5f4b1a93401591080113.png', '2020-06-02 00:00:00', '2020-06-11 14:33:59'),
	(24, '2222', NULL, NULL, NULL, 1, 1, NULL, 'b', 1, 'image.png', NULL, '2020-06-11 14:34:01');
/*!40000 ALTER TABLE `gallery_image` ENABLE KEYS */;

-- Dumping structure for table thethao.gallery_video
CREATE TABLE IF NOT EXISTS `gallery_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_desc` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_showed` tinyint(4) DEFAULT 0,
  `is_show_navi` tinyint(4) DEFAULT 0,
  `date_show` date DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_video` tinyint(4) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.gallery_video: 2 rows
/*!40000 ALTER TABLE `gallery_video` DISABLE KEYS */;
INSERT INTO `gallery_video` (`id`, `title`, `short_desc`, `keywords`, `description`, `is_showed`, `is_show_navi`, `date_show`, `link`, `order`, `video`, `img`, `link_video`, `type_video`, `created_at`, `updated_at`) VALUES
	(23, '1111', '2', '3', '4', 1, 1, '2020-06-17', '1231', 0, 'mov_bbb.mp4', '5ed75322eaa0d1591169826.jpeg', 'https://www.youtube.com/embed/Hd2IfU2W4bI', 0, '2020-06-02 00:00:00', '2020-06-03 07:38:13'),
	(24, 'awdas', NULL, NULL, NULL, 1, 1, '2020-06-03', 'https://www.youtube.com/watch?v=5sYlqUtVJzE', 1, NULL, '5ed75322eaa0d1591169826.jpeg', 'https://www.youtube.com/embed/Hd2IfU2W4bI', 1, '2020-06-03 00:00:00', '2020-06-12 17:13:26');
/*!40000 ALTER TABLE `gallery_video` ENABLE KEYS */;

-- Dumping structure for table thethao.home
CREATE TABLE IF NOT EXISTS `home` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.home: ~0 rows (approximately)
/*!40000 ALTER TABLE `home` DISABLE KEYS */;
INSERT INTO `home` (`id`, `title`, `img_1`, `img_2`, `img_3`, `keywords`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'Home page', '5ede18053c2ea1591613445.png', '5ede1805512e31591613445.png', '5ede1805672b31591613445.png', 'Home page', 'Home page', '2019-12-10 00:00:00', '2020-06-08 10:50:45');
/*!40000 ALTER TABLE `home` ENABLE KEYS */;

-- Dumping structure for table thethao.info
CREATE TABLE IF NOT EXISTS `info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_desc` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_showed` tinyint(4) DEFAULT 0,
  `is_show_navi` tinyint(4) NOT NULL DEFAULT 0,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.info: 2 rows
/*!40000 ALTER TABLE `info` DISABLE KEYS */;
INSERT INTO `info` (`id`, `title`, `short_desc`, `keywords`, `description`, `is_showed`, `is_show_navi`, `link`, `order`, `created_at`, `updated_at`) VALUES
	(24, 'Đào tạo nghiệp vụ', NULL, '12', '123', 1, 1, 'dao-tao-nghiep-vu', 0, '2020-06-04 00:00:00', '2020-06-08 09:34:28'),
	(25, 'Tuyển sinh hè 2020', NULL, NULL, NULL, 1, 1, 'tuyen-sinh-he-2020', 0, '2020-06-04 00:00:00', '2020-06-08 16:37:06');
/*!40000 ALTER TABLE `info` ENABLE KEYS */;

-- Dumping structure for table thethao.info_detail
CREATE TABLE IF NOT EXISTS `info_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_info` int(11) DEFAULT NULL,
  `short_desc` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_showed` tinyint(4) DEFAULT NULL,
  `date_show` date DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.info_detail: 2 rows
/*!40000 ALTER TABLE `info_detail` DISABLE KEYS */;
INSERT INTO `info_detail` (`id`, `title`, `id_info`, `short_desc`, `content`, `keywords`, `description`, `is_showed`, `date_show`, `link`, `order`, `img`, `caption`, `created_at`, `updated_at`) VALUES
	(28, 'Hội thi nấu ăn “Bữa tiệc nhỏ - Hạnh phúc lớn”', 24, 'Vào sáng ngày 24/6/2019 tại Câu lạc bộ TDTT Rạch Miễu, Công đoàn Cơ sở Trung tâm Thể dục Thể thao quận 7 tổ chức hội thi nấu ăn chủ đề “Bữa tiệc nhỏ - Hạnh phúc lớn”. Tham dự Hội thi có 08 bếp ăn đến từ các tổ công đoàn: Văn phòng Trung tâm, Hồ bơi Rạch Miễu, nhà thi đấu Rạch Miễu, câu lạc bộ...', '<p>V&agrave;o s&aacute;ng ng&agrave;y 24/6/2019 tại C&acirc;u lạc bộ TDTT Rạch Miễu, C&ocirc;ng đo&agrave;n Cơ sở Trung t&acirc;m Thể dục Thể thao quận 7 tổ chức hội thi nấu ăn chủ đề &ldquo;Bữa tiệc nhỏ - Hạnh ph&uacute;c lớn&rdquo;. Tham dự Hội thi c&oacute; 08 bếp ăn đến từ c&aacute;c tổ c&ocirc;ng đo&agrave;n: Văn ph&ograve;ng Trung t&acirc;m, Hồ bơi Rạch Miễu, nh&agrave; thi đấu Rạch Miễu, c&acirc;u lạc bộ...</p>', 'aaa', 'tinab', 1, '2020-06-09', 'tin-ab', 0, '5ee070886696d1591767176.jpeg', 'sdab', '2020-06-09 00:00:00', '2020-06-10 06:05:56'),
	(29, 'Họp mặt nhân kỷ niệm 1979 năm cuộc khởi nghĩa Hai Bà Trưng và 109 năm ngày Quốc tế Phụ nữ', 24, 'Họp mặt nhân kỷ niệm 1979 năm cuộc khởi nghĩa Hai Bà Trưng và 109 năm ngày Quốc tế Phụ nữ', '<p>Họp mặt nh&acirc;n kỷ niệm 1979 năm cuộc khởi nghĩa Hai B&agrave; Trưng v&agrave; 109 năm ng&agrave;y Quốc tế Phụ nữ</p>', 'Họp mặt nhân kỷ niệm 1979 năm cuộc khởi nghĩa Hai Bà Trưng và 109 năm ngày Quốc tế Phụ nữ', 'Họp mặt nhân kỷ niệm 1979 năm cuộc khởi nghĩa Hai Bà Trưng và 109 năm ngày Quốc tế Phụ nữ', 1, '2020-06-10', 'tin-ac', 0, '5ee07939d79141591769401.jpeg', '', '2020-06-10 00:00:00', '2020-06-10 06:10:13');
/*!40000 ALTER TABLE `info_detail` ENABLE KEYS */;

-- Dumping structure for table thethao.intro
CREATE TABLE IF NOT EXISTS `intro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_showed` tinyint(4) DEFAULT 0,
  `is_show_navi` tinyint(4) DEFAULT 0,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.intro: 4 rows
/*!40000 ALTER TABLE `intro` DISABLE KEYS */;
INSERT INTO `intro` (`id`, `title`, `keywords`, `description`, `is_showed`, `is_show_navi`, `link`, `order`, `created_at`, `updated_at`) VALUES
	(24, 'Chi bộ Trung tâm', NULL, NULL, 1, 1, 'chi-bo-trung-tam', 1, '2020-06-04 00:00:00', '2020-06-10 15:49:34'),
	(26, 'Trung tâm', NULL, NULL, 1, 1, 'trung-tam', 0, '2020-06-04 00:00:00', '2020-06-09 07:51:38'),
	(27, 'Cơ cấu tổ chức', NULL, NULL, 1, 1, 'co-cau-to-chuc', 0, '2020-06-04 00:00:00', '2020-06-09 07:51:44'),
	(28, 'Phương hướng hoạt động', NULL, NULL, 1, 1, 'phuong-huong-hoat-dong', 0, '2020-06-04 00:00:00', '2020-06-09 07:51:51');
/*!40000 ALTER TABLE `intro` ENABLE KEYS */;

-- Dumping structure for table thethao.intro_detail
CREATE TABLE IF NOT EXISTS `intro_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_intro` int(11) DEFAULT NULL,
  `short_desc` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_showed` tinyint(4) DEFAULT NULL,
  `is_new` tinyint(4) NOT NULL DEFAULT 0,
  `date_show` date DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.intro_detail: 2 rows
/*!40000 ALTER TABLE `intro_detail` DISABLE KEYS */;
INSERT INTO `intro_detail` (`id`, `title`, `id_intro`, `short_desc`, `content`, `keywords`, `description`, `is_showed`, `is_new`, `date_show`, `link`, `order`, `img`, `caption`, `created_at`, `updated_at`) VALUES
	(27, 'phuong huong 1', 28, 'te', '<p>phuong-huong-1</p>', 'phuong-huong-1', 'phuong-huong-1', 1, 1, '2020-06-09', 'phuong-huong-1', 0, '5edf3fb9ddad51591689145.jpeg', 'phuong-huong-1', '2020-06-09 00:00:00', '2020-06-09 08:00:38'),
	(28, 'phuong huong 2', 28, 'test', NULL, NULL, NULL, 1, 1, NULL, 'phuong-huong-2', 1, '5edf3fb9ddad51591689145.jpeg', 'phuong-huong-2', '2020-06-10 15:51:34', '2020-06-10 15:51:37');
/*!40000 ALTER TABLE `intro_detail` ENABLE KEYS */;

-- Dumping structure for table thethao.location_vn
CREATE TABLE IF NOT EXISTS `location_vn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.location_vn: 1 rows
/*!40000 ALTER TABLE `location_vn` DISABLE KEYS */;
INSERT INTO `location_vn` (`id`, `data`) VALUES
	(1, '[{"text":"Africa","href":"","icon":"empty","target":"_self","title":"","content":"","children":[{"text":"Angola","href":"","icon":"empty","target":"_self","title":"","content":"<h4><a href=\\"#\\">Luanda</a></h4>\\n<p>Avenida Lenine, <br/>\\n\\tTorres Oceano, <br/>\\n\\tTorre A, 21ºA <br/>\\n\\tIngombota <br/>\\n\\tLuanda, Angola <br/>\\n\\tTel:+244 226 425 400 <br/>\\n\\tFax:+244 226 425 401 <br/>\\n\\tMap it <br/>\\n\\tMcKinsey Angola (Português) <br/>\\n\\tMcKinsey Angola (English)</p>"},{"text":"Egypt","href":"","icon":"empty","target":"_self","title":"","content":"<h4><a href=\\"#\\">Cairo Suite 2239,</a></h4>\\n<p>Regus Business CentreNile City Towers, <br>\\nNorth Tower, <br>\\n22nd FloorCairo 11624<br>\\nEgypt <br>\\nTel: +20 (2) 2461 8535 <br>\\nFax: +20 (2) 2461 8720 \\n"}]}]');
/*!40000 ALTER TABLE `location_vn` ENABLE KEYS */;

-- Dumping structure for table thethao.log_user
CREATE TABLE IF NOT EXISTS `log_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) DEFAULT NULL,
  `userid` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table thethao.log_user: 16 rows
/*!40000 ALTER TABLE `log_user` DISABLE KEYS */;
INSERT INTO `log_user` (`id`, `ip`, `userid`, `updated_at`, `created_at`) VALUES
	(4, '1', 0, '2020-05-22 14:59:33', '2017-01-21 04:35:42'),
	(5, '2', 0, '2020-05-22 14:59:33', '2018-02-24 04:35:42'),
	(6, '3', 0, '2020-05-22 14:59:33', '2019-02-23 04:35:42'),
	(7, '4', 0, '2020-05-22 14:59:33', '2020-01-01 05:35:42'),
	(8, '5', 0, '2020-05-22 14:59:33', '2019-01-22 04:35:42'),
	(9, '127.0.0.1', 0, '2020-05-22 14:59:33', '2020-01-31 02:17:50'),
	(10, '127.0.0.1', 0, '2020-05-22 14:59:33', '2020-02-13 02:10:01'),
	(11, '127.0.0.1', 0, '2020-05-22 14:59:33', '2020-02-14 06:59:18'),
	(12, '127.0.0.1', 0, '2020-05-22 14:59:33', '2020-02-17 02:06:00'),
	(13, '127.0.0.1', 0, '2020-05-22 14:59:33', '2020-02-19 02:38:04'),
	(14, '127.0.0.1', 0, '2020-05-22 14:59:33', '2020-02-25 02:35:47'),
	(15, '127.0.0.1', 0, '2020-05-22 14:59:33', '2020-02-26 06:44:52'),
	(16, '127.0.0.1', 0, '2020-05-22 14:59:33', '2020-02-27 02:18:32'),
	(17, '127.0.0.1', 0, '2020-05-22 14:59:33', '2020-03-02 02:34:09'),
	(18, '127.0.0.1', 0, '2020-05-22 14:59:33', '2020-03-03 02:26:53'),
	(19, '127.0.0.1', 0, '2020-05-22 14:59:33', '2020-03-05 04:41:46');
/*!40000 ALTER TABLE `log_user` ENABLE KEYS */;

-- Dumping structure for table thethao.news
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_desc` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_showed` tinyint(4) DEFAULT 0,
  `date_show` date DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_new` tinyint(4) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.news: 5 rows
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` (`id`, `title`, `short_desc`, `content`, `keywords`, `description`, `is_showed`, `date_show`, `link`, `order`, `img`, `is_new`, `created_at`, `updated_at`, `caption`) VALUES
	(25, 'Về Cao Lãnh - Đồng Tháp thăm mộ cụ Phó bảng Nguyễn Sinh Sắc', 'Chào mừng Kỷ niệm 50 năm thực hiện di chúc của Chủ tịch Hồ Chí Minh, 89 năm ngày thành lập Hội LHPN Việt Nam và 9 năm ngày Phụ nữ...', ' <blockquote>\r\n                                            <p>Sáng 23/12, trong buổi chào cờ đầu tuần, UBND Quận 7 đã tổ chức khen thưởng lực lượng huấn luyện viên, vận động viên của Quận đã thi đấu đạt thành tích xuất sắc tại SEA Games 30 trong thành phần đoàn Thể thao Việt Nam.</p>\r\n                                        </blockquote>\r\n                                        <p>Đóng góp 7 VĐV trong thành phần đoàn Thể thao Việt Nam tham dự SEA Games 30 ở 4 bộ môn gồm Kickboxing, Taekwondo, Judo, Muay Thái và Đua thuyền, ngành Thể thao Quận 7 đã góp công mang về 2 HCV, 1 HCB và 4 HCĐ. Với những thành tích đó, UBND Quận đã trao tặng giấy khen và tiền thưởng cho các 3 huấn luyện viên và 7 vận động viên xuất sắc.</p>\r\n\r\n                                        <p>Được khen thưởng đợt này là các vận động viên: Huỳnh Văn Tuấn (Huy chương vàng môn Kickboxing); Trần Hồ Duy (Huy chương vàng môn Taekwondo); Nguyễn Hoàng Dương (Huy chương bạc môn Judo); Huỳnh Hà Hữu Hiếu (Huy chương đồng môn Muay Thái); Trần Thị Lụa (Huy chương đồng môn Muay Thái); Phan Mạnh Linh (Huy chương đồng môn Đua thuyền); Nguyễn Mậu Trường (Huy chương đồng môn Đua thuyền). Ngoài ra, còn có 3 huấn luyện viên các bộ môn Taekwondo, Muay Thái và Đua thuyền được nhận giấy khen của UBND Quận 7.\r\n                                        <p style="text-align: right;"><b>TT. TDTT Q7.</b></p>', NULL, NULL, 1, '2020-06-10', 'the-gioi-ngay-nay', 0, '5ee0753a4221c1591768378.jpeg', 1, '2020-06-10 00:00:00', '2020-06-10 05:53:40', NULL),
	(16, 'TỔ CHỨC THÀNH CÔNG HỘI THAO NGÀNH GIÁO DỤC QUẬN 7', 'Nhân dịp kỷ niệm 37 năm ngày Nhà giáo Việt Nam 20/11/1982 – 20/11/2019, Liên đoàn lao động Quận 7...', '<p>Nh&acirc;n dịp kỷ niệm 37 năm ng&agrave;y Nh&agrave; gi&aacute;o Việt Nam 20/11/1982 &ndash; 20/11/2019, Li&ecirc;n đo&agrave;n lao động Quận 7...</p>', NULL, NULL, 1, '2019-11-22', 'the-gioi-ngay-nay-2', 0, '5eddfe718e11f1591606897.jpeg', 1, NULL, '2020-06-08 09:01:37', 'bb'),
	(23, 'KHAI MẠC GIẢI VÕ CỔ TRUYỀN HỘI KHỎE PHÙ ĐỒNG THÀNH PHỐ HỒ CHÍ MINH NĂM HỌC 2019 – 2020', 'Sáng 24/11/2019, Trung tâm thể dục thể thao Quận 7 đăng cai tổ chức giải Võ Cổ Truyền Hội khỏe Phù Đổng...', '<p>Ch&agrave;o mừng Kỷ niệm 50 năm thực hiện di ch&uacute;c của Chủ tịch Hồ Ch&iacute; Minh, 89 năm ng&agrave;y th&agrave;nh lập Hội LHPN Việt Nam v&agrave; 9 năm ng&agrave;y Phụ nữ...</p>', NULL, NULL, 1, '2020-06-08', 'the-gioi-ngay-nay-3', 0, '5eddfe5f48ff51591606879.jpeg', 1, NULL, '2020-06-10 05:45:41', 'cc'),
	(24, 'QUẬN ỦY – HĐND – UBND QUẬN 7 KHEN THƯỞNG VẬN ĐỘNG VIÊN ĐẠT THÀNH TÍCH XUẤT SẮC TẠI SEAGAMES', 'Sáng 23/12, trong buổi chào cờ đầu tuần, UBND Quận 7 đã tổ chức khen thưởng lực lượng huấn luyện viên, vận động viên của Quận đã thi đấu đạt thành tích xuất sắc tại SEA Games 30 trong thành phần đoàn Thể thao Việt Nam.', '<ul>\r\n	<li>S&aacute;ng 23/12, trong buổi ch&agrave;o cờ đầu tuần, UBND Quận 7 đ&atilde; tổ chức khen thưởng lực lượng huấn luyện vi&ecirc;n, vận động vi&ecirc;n của Quận đ&atilde; thi đấu đạt th&agrave;nh t&iacute;ch xuất sắc tại SEA Games 30 trong th&agrave;nh phần đo&agrave;n Thể thao Việt Nam.</li>\r\n</ul>', 'd', 'd', 1, '2020-06-08', 'the-gioi-ngay-nay-4', 0, '5eddfe3fac68d1591606847.jpeg', 1, NULL, '2020-06-10 05:40:00', 'dd'),
	(26, 'GIẢI KARATE HỘI KHỎE PHÙ ĐỔNG quận 7 NĂM HỌC 2019 - 2020', 'Nhằm tuyển chọn vận động viên chuẩn bị tham dự Hội khỏe Phù đổng thành phố Hồ Chí Minh năm học 2019 – 2020. Chiều ngày...', '<p>Nhằm tuy&ecirc;̉n chọn vận động vi&ecirc;n chuẩn bị tham dự Hội khỏe Ph&ugrave; đổng th&agrave;nh phố Hồ Ch&iacute; Minh năm học 2019 &ndash; 2020. Chiều ng&agrave;y...</p>', NULL, NULL, 1, '2020-06-10', 'the-gioi-ngay-nay-5', 0, '5ee07558427211591768408.jpeg', 1, '2020-06-10 00:00:00', NULL, NULL);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;

-- Dumping structure for table thethao.notify
CREATE TABLE IF NOT EXISTS `notify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_desc` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_showed` tinyint(4) DEFAULT 0,
  `date_show` date DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.notify: 3 rows
/*!40000 ALTER TABLE `notify` DISABLE KEYS */;
INSERT INTO `notify` (`id`, `title`, `short_desc`, `content`, `keywords`, `description`, `is_showed`, `date_show`, `link`, `order`, `img`, `created_at`, `updated_at`, `caption`) VALUES
	(25, 'LỊCH DẠY CÁC LỚP THỂ THAO PHONG TRÀO', NULL, NULL, NULL, NULL, 1, NULL, 'a', 0, NULL, NULL, NULL, NULL),
	(26, 'ĐIỀU LỆ GIẢI QUẦN VỢT CÚP PNCo quận 7 NĂM 2019', NULL, NULL, NULL, NULL, 1, NULL, 'b', 1, NULL, NULL, NULL, NULL),
	(27, 'Lịch dạy các lớp thể thao hè 2019', NULL, NULL, NULL, NULL, 1, NULL, 'c', 2, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `notify` ENABLE KEYS */;

-- Dumping structure for table thethao.page_vn
CREATE TABLE IF NOT EXISTS `page_vn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_showed` tinyint(4) DEFAULT NULL,
  `date_show` date DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=99 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.page_vn: 9 rows
/*!40000 ALTER TABLE `page_vn` DISABLE KEYS */;
INSERT INTO `page_vn` (`id`, `title`, `content`, `keywords`, `description`, `is_showed`, `date_show`, `img`, `link`, `created_at`, `updated_at`) VALUES
	(88, 'Con người', '<h3>NHÂN SỰ CỐT LÕI&nbsp;</h3>\r\n\r\n<h4><br />\r\nQUẢN LÝ TỔNG THỂ DỰ ÁN:&nbsp;</h4>\r\n\r\n<p><strong>Nguyễn Ngọc Cao Kỳ</strong>: Nhà cố vấn chiến lược&nbsp;</p>\r\n\r\n<p>Thực chiến điều hành: Siliom, vTrade, vmware (USA), Microsoft, HP…&nbsp;</p>\r\n\r\n<p><strong>Nguyễn ngọc Chánh Thi</strong>: Nhà quản trị&nbsp;</p>\r\n\r\n<p>Thực chiến điều hành: Viettel Telecom&nbsp;</p>\r\n\r\n<h4>ĐIỀU HÀNH CHỨC NĂNG:&nbsp;</h4>\r\n\r\n<p><strong>Võ Văn Bàng</strong>: Điều hành sản xuất&nbsp;</p>\r\n\r\n<h4>CỐ VẤN CAO CẤP:&nbsp;</h4>\r\n\r\n<p><strong>Chuyên gia kỹ thuật Từ các hãng lớn thế giới, Miton Roy, Finish Thompson&nbsp;</strong></p>\r\n\r\n<p>Chuyên gia kinh nghiệm làm việc ở các lĩnh vực khác như công ty thương mại tổng hợp, công ty Công nghệ thông tin và viễn thông, cơ quan tài chính chính phủ và kiểm toán viên.&nbsp;</p>\r\n\r\n<h3>NHÂN SỰ CỘNG TÁC&nbsp;</h3>\r\n\r\n<ul>\r\n    <li>Nhà tư vấn chiến lược &nbsp;</li>\r\n    <li>Chuyên viên Ngân hàng đầu tư&nbsp;</li>\r\n    <li>Chuyên gia kỹ thuật (Ph.D.)&nbsp;</li>\r\n    <li>Kiểm toán viên&nbsp;</li>\r\n    <li>Chuyên gia tư vấn chính sách công&nbsp;</li>\r\n    <li>Nhà tư vấn nhân sự&nbsp;</li>\r\n</ul>\r\n', '', '', 1, '2020-01-12', '1578797764.jpeg', 'con-nguoi', '2020-01-12 00:00:00', '2020-01-12 07:38:21'),
	(90, 'Mô hình kinh doanh', '<h3>MÔ HÌNH KINH DOANH<br />\r\n&nbsp;</h3>\r\n\r\n<table border="1" cellpadding="0" cellspacing="0" class="table table-border table-full-sm" style="width:100%;">\r\n    <tbody>\r\n        <tr>\r\n            <td style="vertical-align: top;">\r\n            <h4 style="text-align: center;">VẤN ĐỀ&nbsp;</h4>\r\n\r\n            <p style="text-align: center;">SỰ GIỚI HẠN CỦA MÔ HÌNH PHÁT TRIỂN DOANH NGHIỆP TỰ NHIÊN</p>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h3>GIẢI PHÁP&nbsp;</h3>\r\n\r\n            <p>THIẾT KẾ LẠI MÔ HÌNH</p>\r\n\r\n            <p>ĐẦU TƯ CHIẾN LƯỢC</p>\r\n\r\n            <p>ĐIỀU HÀNH THỰC TẾ THEO MÔ HÌNH MỚI</p>\r\n\r\n            <p>&nbsp;</p>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h3>GIÁ TRỊ CUNG CẤP</h3>\r\n\r\n            <p>DOANH NGHIỆP PHÁT TRIỂN TRƯỜNG TỒN</p>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h3>LỢI THẾ CẠNH TRANH</h3>\r\n\r\n            <p>MÔ HÌNH KINH DOANH HÌNH THÀNH TỪ TRẢI NGHIỆM KẾT HỢP CHUẨN MỰC KINH DOANH QUỐC TẾ.</p>\r\n\r\n            <p>ĐIỀU HÀNH THỰC TẾ ĐỂ TẠO KẾT QUẢ</p>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h3>PHÂN KHÚC KHÁCH HÀNG</h3>\r\n\r\n            <p>DOANH NGHIỆP SẢN XUẤT, PHÂN PHỐI SẢN PHẨM PHỤC VỤ SẢN XUẤT CÔNG NGHIỆP NHẸ</p>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td></td>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h3>THƯỚC ĐO THÀNH CÔNG&nbsp;</h3>\r\n\r\n            <p>SỐ LƯỢNG DỰ ÁN&nbsp;</p>\r\n\r\n            <p>SỐ LƯỢNG CHỦ DOANH NGHIỆP TỰ DO (RỜI KHỎI DOANH NGHIỆP MÀ KHÔNG LO LẮNG)</p>\r\n            </td>\r\n            <td></td>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h3>KÊNH</h3>\r\n\r\n            <p>TRỰC TIẾP&nbsp;</p>\r\n            </td>\r\n            <td></td>\r\n        </tr>\r\n        <tr>\r\n            <td colspan="3"  style="text-align: center; vertical-align: top;">\r\n            <h3>CẤU TRÚC CHI PHÍ&nbsp;</h3>\r\n\r\n            <p>MẶT BẰNG&nbsp;</p>\r\n\r\n            <p>HẠ TẦNG CÔNG NGHỆ</p>\r\n\r\n            <p>LƯƠNG</p>\r\n            </td>\r\n            <td colspan="2" rowspan="1" style="text-align: center; vertical-align: top;">\r\n            <h3>DOANH THU&nbsp;</h3>\r\n\r\n            <p>TƯ VẤN&nbsp;</p>\r\n\r\n            <p>ĐIỀU HÀNH&nbsp;</p>\r\n\r\n            <p>ĐẦU TƯ&nbsp;</p>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n\r\n\r\n', '', '', 1, '2020-01-12', NULL, 'mo-hinh-kinh-doanh', '2020-01-12 00:00:00', '2020-01-12 08:19:14'),
	(91, 'Lĩnh vực kinh doanh', '<p style="text-align: center;"><img height="431" src="/public/files/image-20200112152301-3.png" width="505" /></p>\r\n', '', '', 1, '2020-01-12', NULL, 'linh-vuc-kinh-doanh', '2020-01-12 00:00:00', '2020-01-12 08:24:17'),
	(92, 'Quy trình đầu tư', '<h3>QUY TRÌNH QUẢN LÝ DOANH NGHIỆP&nbsp;<br />\r\n&nbsp;</h3>\r\n\r\n<table border="1" cellpadding="0" cellspacing="0" class="table table-border" style="width:100%;">\r\n    <tbody>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>GIAI ĐOẠN</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>KHẢO SÁT và THIẾT KẾ</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>TRIỂN KHAI THÔNG QUA ĐIỀU HÀNH MẪU</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>TRIỂN KHAI THÔNG QUA ĐIỀU HÀNH</h4>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>CÔNG VIỆC</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <p>Đánh giá hiện trạng hoạt động của doanh nghiệp, tập trung vào hiện trạng của:</p>\r\n\r\n            <ul>\r\n                <li style="text-align: left;">Dòng tiền</li>\r\n                <li style="text-align: left;">Dòng sản phẩm&nbsp;</li>\r\n                <li style="text-align: left;">Dòng nhân sự&nbsp;</li>\r\n            </ul>\r\n\r\n            <p>Thiết kế lại mô hình phát triển theo công thức của vTimes&nbsp;</p>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">Bố trí nhân sự đứng đầu các bộ phận để trực tiếp tổ chức lại các hoạt động của các bộ phận theo hướng “pháp trị” thông qua xây dựng cấu trúc chi tiết, quy trình thực hiện công việc, chính sách của từng bộ phận và tuyển dụng, đào tạo nhân sự đáp ứng nhu cầu hoạt động&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Bố trí CEO và các nhân sự đứng đầu các bộ phận để tổ chức hoạt động kinh doanh hằng ngày của Công ty&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>Thời gian</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">03-06 tháng</td>\r\n            <td style="text-align: center; vertical-align: top;">06 tháng – 03 năm</td>\r\n            <td style="text-align: center; vertical-align: top;">03 năm – 05 năm</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>Kết quả</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">Bản thiết kế chiến lược phát triển</td>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <p>Cấu trúc, công việc của từng bộ phận&nbsp;</p>\r\n\r\n            <p>Quy trình cơ bản&nbsp;</p>\r\n\r\n            <p>Chính sách kinh doanh&nbsp;</p>\r\n\r\n            <p>Kế hoạch kinh doanh&nbsp;</p>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <p>Doanh thu, lợi nhuận cam kết&nbsp;</p>\r\n\r\n            <p>Thị phần cam kết&nbsp;</p>\r\n\r\n            <p>Hệ thống quản trị vận hành đồng bộ&nbsp;</p>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>Phối hợp</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">Cung cấp thông tin</td>\r\n            <td style="text-align: center; vertical-align: top;">Hỗ trợ triển khai</td>\r\n            <td style="text-align: center; vertical-align: top;">Trao đổi định hướng</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>QUY TRÌNH ĐẦU TƯ&nbsp;<br />\r\n<br />\r\n&nbsp;</h3>\r\n\r\n<table border="1" cellpadding="1" cellspacing="1" class="table table-border table-full-sm" style="width:100%;">\r\n    <tbody>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>GIAI ĐOẠN&nbsp;</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>KHẢO SÁT và THIẾT KẾ&nbsp;</h4>\r\n            </td>\r\n            <td>\r\n            <h4>QUYẾT ĐỊNH ĐẦU TƯ&nbsp;</h4>\r\n            </td>\r\n            <td>\r\n            <h4>THỰC HIỆN DỰ ÁN&nbsp;</h4>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>CÔNG VIỆC&nbsp;</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <p>Đánh giá hiện trạng doanh nghiệp đang hoạt động, tập trung vào hiện trạng của:&nbsp;</p>\r\n\r\n            <ul>\r\n                <li style="text-align: left;">Dòng tiền</li>\r\n                <li style="text-align: left;">Dòng sản phẩm&nbsp;</li>\r\n                <li style="text-align: left;">Dòng nhân sự</li>\r\n            </ul>\r\n\r\n            <p>Xây dựng Chiến lược đầu tư&nbsp;</p>\r\n            </td>\r\n            <td>Thống nhất Kế hoạch đầu tư và các nội dung liên quan</td>\r\n            <td>Đóng góp các nguồn lực (vốn, nhân sự, sở hữu trí tuệ) để triển khai dự án theo kế hoạch&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>Thời gian</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">03-06 tháng</td>\r\n            <td>06 tháng - 01 năm&nbsp;</td>\r\n            <td>05-10 năm&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>Kết quả&nbsp;</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">Chiến lược đầu tư &nbsp;</td>\r\n            <td>Kế hoạch đầu tư&nbsp;</td>\r\n            <td>Lợi nhuận&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>Quy tắc hợp tác đầu tư&nbsp;</h4>\r\n            </td>\r\n            <td colspan="3" rowspan="1" style="text-align: center; vertical-align: top;">4-4-2&nbsp;</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>QUY TRÌNH TƯ VẤN KINH DOANH&nbsp;<br />\r\n<br />\r\n&nbsp;</h3>\r\n\r\n<table border="1" cellpadding="1" cellspacing="1" class="table table-border table-full-sm" style="width:100%;">\r\n    <tbody>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>GIAI ĐOẠN&nbsp;</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>KHẢO SÁT và THIẾT KẾ&nbsp;</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>TRIỂN KHAI GIẢI PHÁP&nbsp;</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>THEO DÕI VÀ ĐIỀU CHỈNH&nbsp;</h4>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>CÔNG VIỆC&nbsp;</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <p>Đánh giá hiện trạng doanh nghiệp đang hoạt động, tập trung vào hiện trạng của:&nbsp;</p>\r\n\r\n            <ul>\r\n                <li style="text-align: left;">Dòng tiền;&nbsp;</li>\r\n                <li style="text-align: left;">Dòng sản phẩm;&nbsp;</li>\r\n                <li style="text-align: left;">Dòng nhân sự&nbsp;</li>\r\n            </ul>\r\n\r\n            <p>Xây dựng Phương án xử lý&nbsp;</p>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">Triển khai các giải pháp đã thống nhất&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Theo dõi, đánh giá và điều chỉnh các vấn đề phát sinh của dự án&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>Thời gian&nbsp;</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">03-06 tháng</td>\r\n            <td style="text-align: center; vertical-align: top;">06 tháng - 01 năm&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Định kỳ 06 tháng&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>Kết quả&nbsp;</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">Phương án xử lý các vấn đề trong kinh doanh&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Vấn đề được giải quyết, doanh nghiệp hoạt động bình thường theo mô tả&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Xử lý kịp thời những phát sinh từ các phương án đã xử lý&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>Phối hợp&nbsp;</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">Cung cấp thông tin&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Tham gia triển khai&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Thông tin phản hồi&nbsp;</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n', '', '', 1, '2020-01-12', NULL, 'quy-trinh-thuc-hien-dich-vu', '2020-01-12 00:00:00', '2020-01-12 10:22:56'),
	(93, 'NGÀNH NGHỀ VÀ KINH NGHIỆM', '<h3>vTimes&nbsp;có&nbsp;kinh&nbsp;nghiệm&nbsp;sâu&nbsp;sắc&nbsp;trong&nbsp;các&nbsp;ngành&nbsp;nghề&nbsp;kinh&nbsp;doanh&nbsp;phổ&nbsp;biến,&nbsp;cụ&nbsp;thể&nbsp;như&nbsp;sau:&nbsp;<br />\r\n<br />\r\n&nbsp;</h3>\r\n\r\n<table border="1" cellpadding="0" cellspacing="0" class="table table-border" style="width:100%;">\r\n    <tbody>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top; width: 100px;">\r\n            <h3>TT</h3>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h3>Ngành nghề</h3>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h3>Kinh nghiệm</h3>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>1</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">Máy tính&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Ra mắt sản phẩm tại Mỹ và Châu Âu&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>2</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">Công nghệ cao&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Khởi nghiệp từ các hạt giống công nghệ&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>3</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">IT&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Cải thiện quy trình phát triển sản phẩm mới&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>4</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">Hàng tiêu dùng&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Chiến lược tăng trưởng toàn cầu – Phát triển tiếp thị trực tiếp&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>5</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">Ngân hàng&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Chiến lược kinh doanh và cơ cấu lại tổ chức&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>6</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">Thực phẩm&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Đào tạo quản lý, điều hành&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>7</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">Dược&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Xem xét lại chiến lược kinh doanh tại Trung Quốc&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>8</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">Thương mại&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Thành lập cơ cấu tổ chức ASEAN&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>9</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">Bán lẻ&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Kiểm toán chiến lược&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>10</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">Giải trí&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Phát triển lĩnh vực kinh doanh mới tại Malaysia&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>11</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">Logictics&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Lập chiến lược kinh doanh và hỗ trợ thực hiện dự án thí điểm&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>12</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">Tài chính&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Lập chiến lược trung hạn&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>13</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">Cho thuê&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Chiến lược M&amp;A&nbsp;</td>\r\n        </tr>\r\n        <tr>\r\n            <td style="text-align: center; vertical-align: top;">\r\n            <h4>14</h4>\r\n            </td>\r\n            <td style="text-align: center; vertical-align: top;">Xây dựng&nbsp;</td>\r\n            <td style="text-align: center; vertical-align: top;">Định nghĩa lại giá trị, tầm nhìn&nbsp;</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n', '', '', 1, '2020-01-12', NULL, 'nganh-nghe-va-kinh-nghiem', '2020-01-12 00:00:00', '2020-01-12 10:42:54'),
	(94, 'KHÁCH HÀNG HIỆN TẠI', '<h3>Các dự án mà vTimes đang đồng hành để triển khai bao gồm:<br />\r\n&nbsp;</h3>\r\n\r\n<ul class="text-list text-list-base">\r\n    <li>\r\n    <h3>Công ty kỹ thuật môi trường Long Trường Vũ:&nbsp; <a href="http://www.longtruongvu.vn">www.longtruongvu.vn</a></h3>\r\n\r\n    <p>- Hệ sinh thái môi trường, xử lý hệ thống nước thải, nước cấp.<br />\r\n    - Hóa chất tẩy rửa cáo cạn, chóng ăn mòn, chóng oxy hóa.</p>\r\n    </li>\r\n    <li>\r\n    <h3>Công CP đầu tư Đại Việt IDC: <a href="http://www.daivietIDC.com.vn">www.daivietIDC.com.vn</a></h3>\r\n<p>    - Nhà tổng thầu các&nbsp; dự án nhà máy công nghiệp, Bến tàu, Bến xe.<br />\r\n    - Xây hệ thống xử lý môi trường.<br />\r\n    - Đào tạo xuất khẩu lao động đi Nhật Bản.</p>\r\n    </li>\r\n    <li>\r\n    <h3>Công ty CP giải pháp Tinh Hoa: <a href="http://www.giaiphaptinhhoa.com">www.giaiphaptinhhoa.com</a></h3>\r\n\r\n    <p>- Phần giải pháp chấm công tự động một hệ thống 5000 trở lên.<br />\r\n    - Giải pháp tính lương tự động hóa.</p>\r\n    </li>\r\n    <li>\r\n    <h3>Công ty TNHH SX TM XNK DV Kỹ thuật Lâm Nguyễn:&nbsp; <a href="http://lamnguyentechnic.com.vn\\">http://lamnguyentechnic.com.vn</a></h3>\r\n\r\n    <p>- Hệ thống toàn quốc<br />\r\n    - Chuyển cấp giải pháp bơm hóa chất cho các nhà máy công nghiệp</p>\r\n    </li>\r\n    <li>\r\n    <h3>Công ty TNHH Năng Lượng Đỉnh Việt (DIVICO): <a href="http://www.divigroup.com.vn">www.divigroup.com.vn</a></h3>\r\n\r\n    <p>- Quy mô thị trường toàn quốc<br />\r\n    - Cấp hệ thống lò hơi</p>\r\n    </li>\r\n</ul>\r\n', '', '', 1, '2020-01-12', NULL, 'thi-truong-chinh', '2020-01-12 00:00:00', '2020-01-12 11:14:21'),
	(96, 'Cảm nhận của khách hàng', '<ul class="accordion-list">\r\n    <li><a href="#">CEO của Lâm Nguyễn</a>\r\n    <div class="content">\r\n    <p>Sau gần 15 năm kinh doanh trong ngành, chúng tôi biết là chúng tôi đã chạm đến những giới hạn trong ngành với cách thức kinh doanh đã qua. Và mọi thứ sẽ về 0 nếu như không làm gì đó khác. Chúng tôi đã tìm kiếm một đơn vị đồng hành suốt 02 năm qua, và cuối cùng đã gặp vTimes. Mọi thứ dường như chỉ là bắt đầu.</p>\r\n    </div>\r\n    </li>\r\n    <li><a href="#">CEO của Lâm Trường Vũ&nbsp;</a>\r\n    <div class="content">\r\n    <p>Từ khi chuyển giao công ty cho vTimes, tôi trải nghiệm những ngày làm chủ doanh nghiệp đúng nghĩa. Doanh nghiệp lớn lên mỗi ngày mà không cần sự quản lý, điều hành của tôi. Giờ đây, tôi đủ thời gian để thực hiện những điều mình từng bỏ lỡ vì bận rộn.</p>\r\n    </div>\r\n    </li>\r\n</ul>\r\n', '', '', 1, '2020-01-12', NULL, 'cam-nhan-cua-khach-hang', '2020-01-12 00:00:00', '2020-01-12 12:37:27'),
	(97, 'Nét độc đáo', '<h3>vTimes sở hữu điểm độc nhất khác biệt hoàn toàn với những Đơn vị cung cấp Dịch vụ tư vấn quản lý, tư vấn tái cấu trúc truyền thống. Điểm độc nhất đó được cấu thành từ các yếu tố sau:<br />\r\n&nbsp;</h3>\r\n\r\n<h3>QUY TRÌNH RIÊNG BIỆT&nbsp;</h3>\r\n\r\n<p>Với mỗi dự án, chúng tôi sẽ lần lượt đi qua các công đoạn sau, mỗi bước được thực hiện một cách thực tế và hệ thống.&nbsp;</p>\r\n\r\n<ol>\r\n    <li>Phác thảo khái niệm (Khởi đầu)&nbsp;<br />\r\n    Những ý tưởng vượt khỏi giới hạn ngành nghề&nbsp;</li>\r\n    <li>Xây dựng chiến lược&nbsp;<br />\r\n    Lộ trình hiện thực hóa dựa trên phân tích thị trường một cách toàn diện và nhanh chóng&nbsp;</li>\r\n    <li>Thiết lập mối quan hệ<br />\r\n    Đặt công ty trong mối quan hệ với khách hàng, các ngành công nghiệp, chính phủ, đại học, và cộng đồng&nbsp;</li>\r\n    <li>Lập quy tắc<br />\r\n    Xây dựng các quy tắc làm cơ sở hỗ trợ các hoạt động kinh doanh/ tái cơ cấu những phương thức truyền thống trong ngành.&nbsp;</li>\r\n    <li>Hỗ trợ bên trong/bên ngoài cho công ty&nbsp;<br />\r\n    Quản lý dự án theo từng giai đoạn&nbsp;</li>\r\n    <li>Mang lại kết quả (mục tiêu)&nbsp;<br />\r\n    &nbsp;</li>\r\n</ol>\r\n\r\n<h3>CHÍNH SÁCH CÔNG NGHIỆP ĐẶC THÙ&nbsp;</h3>\r\n\r\n<p>Chúng tôi chọn công nghiệp để triển khai nhằm tạo ra sự phát triển bền vững cho cả nền kinh tế. Chúng tôi tập trung vào các chính sách sau:&nbsp;</p>\r\n\r\n<ul>\r\n    <li>&nbsp;“Kế hoạch Thành phố xanh"&nbsp;</li>\r\n    <li>Chiếm vị trí ưu tiên hàng đầu trong chính sách của METI&nbsp;</li>\r\n    <li>Triển khai thiết bị tiết kiệm năng lương toàn cầu&nbsp;</li>\r\n    <li>Chiến lược mở rộng công nghệ tiết kiệm năng lượng (bao gồm chiến lược tiêu chuẩn hóa)&nbsp;</li>\r\n    <li>Chiến lược chuyển giao hệ thống tái chế sang nước ngoài&nbsp;</li>\r\n    <li>Chiến lược hệ thống xuất khẩu và cơ sở hạ tầng&nbsp;</li>\r\n    <li>Bản đồ lộ trình công nghiệp hóa kỹ thuật nông lâm thủy sản tiềm năng&nbsp;</li>\r\n    <li>Phát triển khu dân cư dành cho người Già&nbsp;<br />\r\n    &nbsp;</li>\r\n</ul>\r\n\r\n<h3>HỆ SINH THÁI ĐẶC BIỆT&nbsp;</h3>\r\n\r\n<p>Để có thể thực thi chính sách công nghiệp, chúng tôi thiết lập một hệ sinh thái gồm những tổ chức sau:&nbsp;</p>\r\n\r\n<ul>\r\n    <li>Nhà sản xuất pin&nbsp;</li>\r\n    <li>Nhà sản xuất điện gia dụng điện tử&nbsp;</li>\r\n    <li>Nhà sản xuất điện bằng năng lượng mặt trời&nbsp;</li>\r\n    <li>Nhà sản xuất điện bằng sức gió&nbsp;</li>\r\n    <li>Nhà sản xuất hàng điện tử&nbsp;</li>\r\n    <li>Trường Đại học&nbsp;</li>\r\n    <li>Công ty thương mại&nbsp;</li>\r\n    <li>Công ty tái chế &nbsp;</li>\r\n    <li>Nhà sản xuất thiết bị gia dụng&nbsp;</li>\r\n    <li>Nhà sản xuất thiết bị điện tử&nbsp;</li>\r\n    <li>Nhà cung cấp CNTT&nbsp;</li>\r\n    <li>Công ty sản xuất F&amp;B&nbsp;</li>\r\n    <li>Công ty phân phối thực phẩm&nbsp;</li>\r\n    <li>Chính quyền địa phương&nbsp;</li>\r\n    <li>Công ty phát triển bất động sản&nbsp;</li>\r\n    <li>Nhà sản xuất xe hơi.&nbsp;</li>\r\n</ul>\r\n', '', '', 1, '2020-01-12', NULL, 'net-doc-dao', '2020-01-12 00:00:00', '2020-01-12 13:01:19'),
	(98, 'TUYỂN DỤNG', '<h3>KHÁCH HÀNG&nbsp;<br />\r\n&nbsp;</h3>\r\n\r\n<h4>BẠN MUỐN ĐƯA DOANH NGHIỆP CỦA MÌNH LÊN MỘT TẦM CAO MỚI&nbsp;</h4>\r\n\r\n<p>Hơn ai hết, bạn biết rõ rằng những thành tựu của mình đã qua sẽ không lặp lại. Trong một sự biến động của môi trường kinh doanh như hiện tại, giải pháp an toàn nhất có thể là tạm dừng mọi thứ lại để bảo toàn những gì đã có. Nhưng chúng tôi biết là bạn sẽ không làm thế, “miệng ăn núi lở”, nên nếu dừng lại thôi nghĩa là bạn chấp nhận những gì đang có hôm nay sẽ biến mất vào một ngày nào đó. Chúng tôi đề xuất với bạn một giải pháp mới, nhìn lại và vẽ mới con đường phía trước để lối đi thênh thang hơn. Bạn sẵn sàng bước lên một tầm cao mới rồi chứ? Hãy gọi cho chúng tôi.&nbsp;<br />\r\n&nbsp;</p>\r\n\r\n<h3>CHUYÊN GIA&nbsp;<br />\r\n&nbsp;</h3>\r\n\r\n<h4>BẠN MUỐN THỊNH VƯỢNG THEO MỘT CÁCH KHÁC&nbsp;</h4>\r\n\r\n<p>Thịnh vượng không chỉ là vấn đề nghề nghiệp, càng không chỉ là vấn đề sở hữu mà đó còn là một phong cách sống. Bạn đang muốn tạo ra một cuộc sống thịnh vượng cho chính mình và những người xung quanh theo một cách thức độc đáo? Hãy tham gia vào đội ngũ chuyên gia của chúng tôi, nếu như bạn có những tố chất sau đây:&nbsp;</p>\r\n\r\n<ul>\r\n    <li>Thích tự do&nbsp;</li>\r\n    <li>Quyết liệt trong hành động&nbsp;</li>\r\n    <li>Muốn đi xa hơn là đi nhanh&nbsp;</li>\r\n</ul>\r\n', '', '', 1, '2020-01-12', NULL, 'tuyen-dung', '2020-01-12 00:00:00', '2020-01-12 13:04:37');
/*!40000 ALTER TABLE `page_vn` ENABLE KEYS */;

-- Dumping structure for table thethao.partner
CREATE TABLE IF NOT EXISTS `partner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_showed` tinyint(4) DEFAULT 0,
  `is_show_navi` tinyint(4) DEFAULT 0,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.partner: 1 rows
/*!40000 ALTER TABLE `partner` DISABLE KEYS */;
INSERT INTO `partner` (`id`, `title`, `is_showed`, `is_show_navi`, `link`, `order`, `img`, `created_at`, `updated_at`) VALUES
	(25, 'adas', 1, 1, 'asdas', 0, NULL, NULL, NULL);
/*!40000 ALTER TABLE `partner` ENABLE KEYS */;

-- Dumping structure for table thethao.relation
CREATE TABLE IF NOT EXISTS `relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_showed` tinyint(4) DEFAULT 0,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.relation: 3 rows
/*!40000 ALTER TABLE `relation` DISABLE KEYS */;
INSERT INTO `relation` (`id`, `title`, `is_showed`, `link`, `order`, `created_at`, `updated_at`) VALUES
	(25, 'UBND quận 7', 1, 'http://www.phunhuan.hochiminhcity.gov.vn/', 0, '2020-06-05 16:06:15', '2020-06-05 16:06:14'),
	(26, 'Bộ Văn hóa Thể thao và Du lịch', 1, 'http://www.bvhttdl.gov.vn/', 1, '2020-06-05 16:06:13', '2020-06-05 16:06:14'),
	(27, 'Sở Văn hóa và Thể thao TP.HCM', 1, 'http://svhtt.hochiminhcity.gov.vn/', 2, '2020-06-05 16:08:24', '2020-06-05 16:08:25');
/*!40000 ALTER TABLE `relation` ENABLE KEYS */;

-- Dumping structure for table thethao.setting
CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.setting: ~8 rows (approximately)
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` (`id`, `module`, `title`, `keywords`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'news', 'danh sach tin tuc', 'danh sach tin tuc su kien', 'danh sach tin tuc su kien', '2020-06-10 14:19:56', '2020-06-10 14:19:56'),
	(2, 'intro', 'danh sach gioi thieu', 'danh sach gioi thieu', 'danh sach gioi thieu', '2020-06-10 15:47:30', '2020-06-10 15:47:30'),
	(3, 'sport', 'danh sach the thao', 'danh sach the thao', 'danh sach the thao', '2020-06-11 09:49:10', '2020-06-11 09:49:10'),
	(4, 'info', 'danh sach thong tin', 'danh sach thong tin', 'danh sach thong tin', '2020-06-11 11:38:17', '2020-06-11 11:38:18'),
	(5, 'union', 'danh sach dang doan the', 'danh sach dang doan the', 'danh sach dang doan the', '2020-06-11 12:50:02', '2020-06-11 12:50:02'),
	(6, 'admissions', 'danh sach chieu sinh', 'danh sach chieu sinh', 'danh sach chieu sinh', '2020-06-11 14:20:09', '2020-06-11 14:20:10'),
	(7, 'image', 'thu vien hinh anh', 'thu vien hinh anh', 'thu vien hinh anh', '2020-06-11 14:27:18', '2020-06-11 14:27:18'),
	(8, 'video', 'thu vien video', 'thu vien video', 'thu vien video', '2020-06-11 14:27:47', '2020-06-11 14:27:47'),
	(9, 'contact', 'lien he', 'lien he', 'lien he', '2020-06-15 14:38:45', '2020-06-15 14:38:46'),
	(10, 'thanks', 'thanks', 'thanks', 'thanks', '2020-06-15 17:56:29', '2020-06-15 17:56:30');
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;

-- Dumping structure for table thethao.slider
CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_showed` tinyint(4) NOT NULL DEFAULT 0,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `target` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.slider: 5 rows
/*!40000 ALTER TABLE `slider` DISABLE KEYS */;
INSERT INTO `slider` (`id`, `title`, `is_showed`, `link`, `order`, `img`, `created_at`, `updated_at`, `target`) VALUES
	(23, 'a', 1, 'http://thethao.local', 0, 'hd1.jpg', '2020-06-01 00:00:00', '2020-06-10 09:54:01', 1),
	(24, 'b', 1, 'http://thethao.local', 1, 'hd2.jpg', NULL, '2020-06-10 09:54:01', 1),
	(25, 'c', 1, 'http://thethao.local', 2, 'hd3.jpg', NULL, '2020-06-10 09:54:02', 0),
	(26, 'd', 1, 'http://thethao.local', 3, 'hd4.jpg', NULL, '2020-06-10 09:54:02', 1),
	(27, 'e', 1, 'http://thethao.local', 4, 'hd5.jpg', NULL, '2020-06-10 09:54:03', 0);
/*!40000 ALTER TABLE `slider` ENABLE KEYS */;

-- Dumping structure for table thethao.slider_top
CREATE TABLE IF NOT EXISTS `slider_top` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_showed` tinyint(4) DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.slider_top: 4 rows
/*!40000 ALTER TABLE `slider_top` DISABLE KEYS */;
INSERT INTO `slider_top` (`id`, `is_showed`, `order`, `img`, `title`, `created_at`, `updated_at`) VALUES
	(24, 0, 0, '5ed89c6267ee91591254114.jpeg', '1', '2020-06-04 00:00:00', '2020-06-04 14:05:41'),
	(25, 0, 1, '5ed89c76dded61591254134.jpeg', '2', '2020-06-04 00:00:00', '2020-06-04 14:05:41'),
	(26, 1, 2, '5ed89c7f7eb7b1591254143.jpeg', '3', '2020-06-04 00:00:00', '2020-06-04 14:05:41'),
	(27, 1, 3, '5ed89c8ca78b61591254156.jpeg', '4', '2020-06-04 00:00:00', '2020-06-04 14:05:41');
/*!40000 ALTER TABLE `slider_top` ENABLE KEYS */;

-- Dumping structure for table thethao.slider_vn
CREATE TABLE IF NOT EXISTS `slider_vn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `order` int(11) NOT NULL DEFAULT 0,
  `target` tinyint(4) NOT NULL DEFAULT 0,
  `is_showed` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.slider_vn: 1 rows
/*!40000 ALTER TABLE `slider_vn` DISABLE KEYS */;
INSERT INTO `slider_vn` (`id`, `title`, `img`, `link`, `order`, `target`, `is_showed`, `created_at`, `updated_at`) VALUES
	(7, 'ABC', '5e18a04bc217b1578672203.jpeg', '#', 0, 0, 1, '2020-01-10', NULL);
/*!40000 ALTER TABLE `slider_vn` ENABLE KEYS */;

-- Dumping structure for table thethao.sport
CREATE TABLE IF NOT EXISTS `sport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `is_showed` tinyint(4) NOT NULL DEFAULT 0,
  `keywords` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.sport: 27 rows
/*!40000 ALTER TABLE `sport` DISABLE KEYS */;
INSERT INTO `sport` (`id`, `title`, `is_showed`, `keywords`, `description`, `order`, `icon`, `content`, `link`, `created_at`, `updated_at`) VALUES
	(29, 'Boxing', 1, NULL, NULL, 0, 'boxing.jpg', '<p><img alt="" src="/public/files/image-20200112152241-1.png" style="width: 505px; height: 431px;" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&aacute;đ&acirc;sd asdasdasd</p>\r\n\r\n<p>&nbsp;</p>', 'a', NULL, '2020-06-11 04:09:35'),
	(28, 'Aikido', 1, NULL, NULL, 0, 'akido.jpg', NULL, 'b', NULL, '2020-06-11 10:10:17'),
	(30, 'Judo', 1, NULL, NULL, 0, 'judo.jpg', NULL, 'c', NULL, '2020-06-11 10:10:17'),
	(31, 'Karate', 1, NULL, NULL, 0, 'karate.jpg', NULL, 'd', NULL, '2020-06-11 10:10:18'),
	(32, 'Kickboxing', 1, NULL, NULL, 0, 'kickboxing.jpg', NULL, 'e', NULL, '2020-06-11 10:10:19'),
	(33, 'Muay', 1, NULL, NULL, 1, 'muay.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(34, 'Pencak silat', 1, NULL, NULL, 1, 'pencak_silat.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(35, 'Taekwondo', 1, NULL, NULL, 1, 'taekwondo.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(36, 'Võ cổ truyền', 1, NULL, NULL, 1, 'vocotruyen.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(37, 'Vovinam', 1, NULL, NULL, 1, 'vovinam.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(38, 'Wushu', 1, NULL, NULL, 1, 'wushu.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(39, 'Bơi lội', 1, NULL, NULL, 1, 'boiloi.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(40, 'Bóng bàn', 1, NULL, NULL, 1, 'bongban.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(41, 'Bóng chuyền', 1, NULL, NULL, 1, 'bongchuyen.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(42, 'Bóng Đá', 1, NULL, NULL, 1, 'bongda.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(43, 'Bóng Rổ', 1, NULL, NULL, 1, 'bongro.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(44, 'Cầu Lông', 1, NULL, NULL, 1, 'caulong.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(45, 'Cầu mây', 1, NULL, NULL, 1, 'caumay.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(46, 'Cờ tướng', 1, NULL, NULL, 1, 'cotuong.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(47, 'Cờ Vua', 1, NULL, NULL, 1, 'covua.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(48, 'Đẩy gậy', 1, NULL, NULL, 1, 'daygay.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(49, 'Điền Kinh', 1, NULL, NULL, 1, 'dienkinh.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(50, 'Kéo co', 1, NULL, NULL, 1, 'keoco.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(51, 'Quần vợt', 1, NULL, NULL, 1, 'quanvot.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(52, 'TD nhịp điệu', 1, NULL, NULL, 1, 'theducnhipdieu.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(53, 'TD thẩm mỹ', 1, NULL, NULL, 1, 'theducthammy.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09'),
	(54, 'Xe đạp', 1, NULL, NULL, 1, 'xedap.jpg', NULL, NULL, NULL, '2020-06-11 10:10:09');
/*!40000 ALTER TABLE `sport` ENABLE KEYS */;

-- Dumping structure for table thethao.sport_detail
CREATE TABLE IF NOT EXISTS `sport_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_sport` int(11) DEFAULT NULL,
  `short_desc` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_showed` tinyint(4) NOT NULL DEFAULT 0,
  `date_show` date DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.sport_detail: 4 rows
/*!40000 ALTER TABLE `sport_detail` DISABLE KEYS */;
INSERT INTO `sport_detail` (`id`, `title`, `id_sport`, `short_desc`, `content`, `keywords`, `description`, `is_showed`, `date_show`, `link`, `caption`, `order`, `img`, `created_at`, `updated_at`) VALUES
	(23, 'Đội bơi trung cao tuổi hạng nhất toàn đoàn giải Thành phố năm 2019', 29, 'Đội bơi trung cao tuổi hạng nhất toàn đoàn giải Thành phố năm 2019', '<p><img alt="" src="/public/files/image-20200112152241-1.png" style="width: 505px; height: 431px;" /></p>', 'a', 'a', 1, NULL, 'akido-news', 'a', 0, '5ee1ac66310b61591848038.png', NULL, '2020-06-11 04:07:41'),
	(24, 'Giải Quần vợt kỷ niệm ngày Doanh nhân Việt Nam 13/10', 29, 'Giải Quần vợt kỷ niệm ngày Doanh nhân Việt Nam 13/10', '<p>1&aacute;daaa</p>', 'b', 'b', 1, '2020-05-25', 'judo-news', 'b', 1, 'judo.jpg', '2020-05-25 00:00:00', '2020-06-10 05:59:39'),
	(25, 'Giải bóng đá mini cúp Doanh nghiệp quận 7 chào mừng ngày Doanh nhân Việt Nam 13/10 năm 2019', 29, 'Sau gần ba tuần tranh tài sôi nổi, Giải bóng đá mini cúp Doanh nghiệp quận 7 chào mừng ngày Doanh nhân Việt Nam đã chính thức khép lại vào tối ngày 03/10/2019 tại câu lạc bộ Bóng đá quận 7.', '<p>asdasd</p>', 'c', 'c', 1, '2020-05-27', 'boxing-news', 'c', 2, 'boxing.jpg', '2020-05-27 00:00:00', '2020-06-10 05:59:14'),
	(26, 'Đội bóng đá tp. hcm (q. quận 7) vô địch vòng loại hội khỏe phù đổng học sinh tiểu học 4', 29, 'aa', '<p>asd</p>', 'd', 'd', 1, '2020-05-27', 'co-tuong-news', 'd', 3, 'cotuong.jpg', '2020-05-27 00:00:00', NULL);
/*!40000 ALTER TABLE `sport_detail` ENABLE KEYS */;

-- Dumping structure for table thethao.union
CREATE TABLE IF NOT EXISTS `union` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_showed` tinyint(4) NOT NULL DEFAULT 0,
  `is_show_navi` tinyint(4) NOT NULL DEFAULT 0,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.union: 1 rows
/*!40000 ALTER TABLE `union` DISABLE KEYS */;
INSERT INTO `union` (`id`, `title`, `keywords`, `description`, `is_showed`, `is_show_navi`, `link`, `order`, `created_at`, `updated_at`) VALUES
	(29, 'Chi bộ', NULL, NULL, 1, 1, 'chi-bo', 0, '2020-06-09 00:00:00', '2020-06-09 16:17:09');
/*!40000 ALTER TABLE `union` ENABLE KEYS */;

-- Dumping structure for table thethao.union_detail
CREATE TABLE IF NOT EXISTS `union_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_union` int(11) DEFAULT NULL,
  `short_desc` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_showed` tinyint(4) NOT NULL DEFAULT 0,
  `is_new` tinyint(4) NOT NULL DEFAULT 0,
  `date_show` date DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.union_detail: 1 rows
/*!40000 ALTER TABLE `union_detail` DISABLE KEYS */;
INSERT INTO `union_detail` (`id`, `title`, `id_union`, `short_desc`, `content`, `keywords`, `description`, `is_showed`, `is_new`, `date_show`, `link`, `order`, `img`, `caption`, `created_at`, `updated_at`) VALUES
	(29, 'Họp mặt nhân kỷ niệm 1979 năm cuộc khởi nghĩa Hai Bà Trưng và 109 năm ngày Quốc tế Phụ nữ', 29, 'Họp mặt nhân kỷ niệm 1979 năm cuộc khởi nghĩa Hai Bà Trưng và 109 năm ngày Quốc tế Phụ nữ', '<p>Họp mặt nh&acirc;n kỷ niệm 1979 năm cuộc khởi nghĩa Hai B&agrave; Trưng v&agrave; 109 năm ng&agrave;y Quốc tế Phụ nữ</p>', NULL, 'Họp mặt nhân kỷ niệm 1979 năm cuộc khởi nghĩa Hai Bà Trưng và 109 năm ngày Quốc tế Phụ nữ', 1, 1, '2020-06-10', NULL, 0, '5ee07b5d160b51591769949.jpeg', '', '2020-06-10 00:00:00', '2020-06-10 06:19:10');
/*!40000 ALTER TABLE `union_detail` ENABLE KEYS */;

-- Dumping structure for table thethao.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permission` tinyint(4) NOT NULL DEFAULT 0,
  `tel` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `block` tinyint(4) NOT NULL DEFAULT 0,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table thethao.users: 4 rows
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `email`, `password`, `fullname`, `permission`, `tel`, `block`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'abcd', 'asd@gmail.com', '$2y$10$RFx4NS.BXsEgVMjD0faq6OHnPhwGj3jJcL3lGrEOkPwBdSCZFiQHm', 'Ross Mercer1', 1, 'asdasd', 0, 'aOPyEMvCz9zKF0cf7tzKIXKBJmssSfN5IkahFrtRuyxaLMslORzqwG27KBij', '2019-11-26 00:00:00', '2020-06-08 15:46:01'),
	(2, 'hau_tran', NULL, '$2y$10$5Nhlg.tZoIc3MThCKG7Ga.PiF2VpJytk6/Qj6Y2YY/y9BaVBRuAea', 'hau', 0, NULL, 0, NULL, '2020-01-16 00:00:00', '2020-06-03 15:22:54'),
	(3, 'test', NULL, '$2y$10$8ay/tNC6PCaIXjjZzim4KOOGChnZ8bGzPwQlnOzSeIlTFmnpTR6jG', 'test', 1, NULL, 1, 'xPCNxB93vlkA9Su50TEWMQLVXSAgion8MtxQwcTNIdnSooyt9BUtjhrPfqaq', '2020-01-16 00:00:00', '2020-06-03 08:20:31'),
	(4, 'asdas', 'asda@gmail.com', '$2y$10$yNUv6p9n/onHRX7ot8bE8OezIWNQDR7Tj.9iovqa//95XeYadEIOe', 'asdas', 1, '08979879879', 0, NULL, '2020-06-03 00:00:00', '2020-06-03 16:22:59');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
