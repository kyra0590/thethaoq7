@extends('user.layout')

@section('title', $setting->title)
@section('description', $setting->description)
@section('keywords', $setting->keywords)

@section('css')
<link href="/public/css/lightgallery.css" rel="stylesheet">
<style>
   .demo-gallery {
     /*   background: #152836;
        padding: 30px 10px 0px 10px;
        border-radius: 10px;*/
    }

    .demo-gallery > ul {
      margin-bottom: 0;
    }
    .demo-gallery > ul > li {
        float: left;
        margin-bottom: 20px;
        color:#fff;
        text-align: center;
    }

    .demo-gallery > ul > li a {
      border: 3px solid #FFF;
      border-radius: 3px;
      /*display: block;*/
      overflow: hidden;
      position: relative;
      float: left;
      width: 100%;
      height: 150px;
      display: table-cell;
    }
    .demo-gallery > ul > li a > img {
      -webkit-transition: -webkit-transform 0.15s ease 0s;
      -moz-transition: -moz-transform 0.15s ease 0s;
      -o-transition: -o-transform 0.15s ease 0s;
      transition: transform 0.15s ease 0s;
      -webkit-transform: scale3d(1, 1, 1);
      transform: scale3d(1, 1, 1);
      height: auto;
      width: 100%;
    }

    .demo-gallery > ul > li a:hover > img {
      -webkit-transform: scale3d(1.1, 1.1, 1.1);
      transform: scale3d(1.1, 1.1, 1.1);
    }

    .demo-gallery > ul > li a .demo-gallery-poster {
      background-color: rgba(0, 0, 0, 0.1);
      bottom: 0;
      left: 0;
      position: absolute;
      right: 0;
      top: 0;
      -webkit-transition: background-color 0.15s ease 0s;
      -o-transition: background-color 0.15s ease 0s;
      transition: background-color 0.15s ease 0s;
    }

    .demo-gallery > ul > li a:hover .demo-gallery-poster {
      background-color: rgba(0, 0, 0, 0.5);
    }

    .demo-gallery > ul > li a .demo-gallery-poster > img {
      width:
      margin-left: -10px;
      margin-top: -10px;
      position: absolute;
      top: 35%;
      left: 35%;
      -webkit-transition: opacity 0.3s ease 0s;
      -o-transition: opacity 0.3s ease 0s;
      transition: opacity 0.3s ease 0s;
    }
</style>
@section('content')
<div class="grid-box" >
    <div class="title-section">
        <h1><span class="sport">Thư viện video</span></h1>
    </div>
     @if(!empty($gallery) && count($gallery))
     <div class="demo-gallery">
      @foreach($gallery as $item)
        @if ($item->type_video == 0)
        <div style="display:none;" id="video_{{$item->id}}">
            <video class="lg-video-object lg-html5" controls preload="none">
                <source src="/public/images/gallery/video/{{$item->video}}"  type="video/mp4">
                 Your browser does not support HTML5 video.
            </video>
        </div>
        @endif
      @endforeach

        <ul id="lightgallery" class="list-unstyled row">
            @foreach($gallery as $item)
              @if($item->type_video == 1)
                <li class="col-xs-6 col-sm-4 col-md-3" data-src="{{$item->link}}" data-sub-html="{{$item->title}}">
                   <a href="#">
                        <img class="img-responsive" src="/public/images/gallery/video/{{$item->img}}">
                    <div class="demo-gallery-poster">
                      <img src="/public/images/play-button.png" width="50px">
                    </div>
                    </a>
                    {{$item->title}}
                </li>
              @else
              <li class="col-xs-6 col-sm-4 col-md-3" data-poster="/public/images/gallery/video/{{$item->img}}" data-sub-html="{{$item->title}}" data-html="#video_{{$item->id}}">
                   <a href="#">
                        <img class="img-responsive" src="/public/images/gallery/video/{{$item->img}}">
                    <div class="demo-gallery-poster">
                      <img src="/public/images/play-button.png" width="50px">
                    </div>
                    </a>
                    {{$item->title}}
                </li>
              @endif
            @endforeach
        </ul>
    </div>
    @endif
</div>

@if(!empty($gallery) && count($gallery))
<div class="pagination-box">
    <ul class="pagination-list">
        @if ($gallery->lastPage() == 1)
            <li><a class="active" href="#">1</a></li>
        @elseif ($gallery->lastPage() > 1)
            @if($gallery->currentPage() > 1)
                <li><a href="{{ $gallery->url($gallery->currentPage() - 1) }}">Back</a></li>
            @endif

             @for ($i = 1; $i <= $gallery->lastPage(); $i++)
                <li><a class="{{ ($gallery->currentPage() == $i) ? ' active' : '' }}" href="{{ $gallery->url($i) }}">{{$i}}</a></li>
            @endfor

            @if($gallery->currentPage() < $gallery->lastPage())
            <li><a href="{{ $gallery->url($gallery->currentPage()+1) }}">Next</a></li>
            @endif
        @endif
    </ul>

    <p>Page {{$gallery->currentPage()}} of {{$gallery->lastPage()}}</p>
</div>
@else
    <p>Hiện tại không tìm thấy dữ liệu</p>
@endif
@endsection
@section('js')
    <script type="text/javascript">
    $(document).ready(function(){
        $('#lightgallery').lightGallery();
    });
    </script>
@endsection
