@extends('user.layout')

@section('title', $union->title)
@section('description', $union->description)
@section('keywords', $union->keywords)

@section('content')
<div class="grid-box">
    <div class="title-section">
        <h1><span>{{$union->title}}</span></h1>
    </div>
    @if(!empty($unionDetail) && count($unionDetail))
        @foreach($unionDetail as $item)
        <div class="news-post article-post">
            <div class="row">
                <div class="col-sm-3">
                    <div class="post-gallery">
                        @if (!empty($item->img) && file_exists($imgPath."/union/thumb/".$item->img))
                            <img src="/public/images/union/thumb/{{$item->img}}"  alt="{{$item->caption}}" width="100%">
                        @else
                            <img src="/public/images/logo_thumb.jpg" style="width: auto !important; height: 150px">
                        @endif
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="post-content">
                        <h2><a href="/dang-doan-the/{{$item->id_union}}-{{$item->link}}.html">{{$item->title}}</a></h2>
                        <p>{{$item->short_desc}}</p>
                        <a href="/dang-doan-the/{{$item->id_union}}-{{$item->link}}.html" class="read-more-button"><i class="fa fa-arrow-circle-right"></i>Đọc thêm</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    @endif
</div>
 @if(!empty($unionDetail) && count($unionDetail))
<div class="pagination-box">
    <ul class="pagination-list">
        @if ($unionDetail->lastPage() == 1)
            <li><a class="active" href="#">1</a></li>
        @elseif ($unionDetail->lastPage() > 1)
            @if($unionDetail->currentPage() > 1)
                <li><a href="{{ $unionDetail->url($unionDetail->currentPage() - 1) }}">Back</a></li>
            @endif

             @for ($i = 1; $i <= $unionDetail->lastPage(); $i++)
                <li><a class="{{ ($unionDetail->currentPage() == $i) ? ' active' : '' }}" href="{{ $unionDetail->url($i) }}">{{$i}}</a></li>
            @endfor

            @if($unionDetail->currentPage() < $unionDetail->lastPage())
            <li><a href="{{ $unionDetail->url($unionDetail->currentPage()+1) }}">Next</a></li>
            @endif
        @endif
    </ul>

    <p>Page {{$unionDetail->currentPage()}} of {{$unionDetail->lastPage()}}</p>
</div>
@else
    <p>Hiện tại không tìm thấy dữ liệu</p>
@endif

@endsection