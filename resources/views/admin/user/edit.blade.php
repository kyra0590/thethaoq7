@extends('admin.layout')
@section('title', $title)
@section('content')

<div class="row page-titles">
    <div class="col-md-12 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <button type="button" class="btn btn-info d-none d-lg-block m-l-15" onclick="javascript:location.href='/admin/union'"><i class="fa fa-undo"></i> Trở lại</button>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-success">
               <h4 class="m-b-0 text-white">{{$title}}</h4>
            </div>
            <div class="card-body">
                <form action="" method="post" enctype="multipart/form-data">
                    @csrf
                     <div class="form-body">
                        <div class="row p-t-20">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Khóa</label>
                                     <select class="custom-select form-control" data-placeholder="Lựa chọn" tabindex="1" name="block" required>
                                        <?php
                                            $block = ["Mở khóa", "Khóa"];
                                            foreach ($block as $key => $value) {
                                                $selected = ($user->block == $key) ? 'selected' : '';
                                         ?>
                                            <option value="{{ $key }}" {{$selected}}>{{ $value }}</option>
                                        <?php
                                          }
                                        ?>
                                    </select>
                                </div>
                            </div>
                             <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label">Quyền</label>
                                    <select class="custom-select form-control" data-placeholder="Choose a permission" tabindex="1" name="permission" required>
                                        <?php
                                            $permission = ["user", "admin"];
                                            foreach ($permission as $key => $value) {
                                                $selected = ($user->permission == $key) ? 'selected' : '';
                                         ?>
                                            <option value="{{ $key }}" {{$selected}}>{{ $value }}</option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                             <div class="col-md-6">
                                <div class="form-group">
                                   <label class="control-label">Tên người dùng</label>
                                    <input type="text" class="form-control" placeholder="" name="username" value="{{$user->username}}" required>
                                </div>
                            </div>
                           <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Mật khẩu</label>
                                    <input type="password" class="form-control" placeholder="" name="password" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Họ tên</label>
                                    <input type="text" class="form-control" placeholder="" name="fullname" value="{{$user->fullname}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input type="email" class="form-control" placeholder="" name="email" value="{{$user->email}}"required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Điện thoại</label>
                                    <input type="text" class="form-control" placeholder="" name="tel" value="{{$user->tel}}" >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Lưu</button>
                        <button type="button" class="btn btn-danger" onclick="javascript:location.href='/admin/user'"> <i class="fa fa-ban"></i> Hủy bỏ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
<?php if($status) { ?>
 $.toast({
    heading: "Thông báo",
    text: "Cập nhật thành công",
    position: 'bottom-right',
    loaderBg:'#ff6849',
    icon: 'success',
    hideAfter: 3500,
    stack: 6
});
<?php } ?>
</script>
@endsection
