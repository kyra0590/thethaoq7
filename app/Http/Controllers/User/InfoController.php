<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\User\BaseController;
use Illuminate\Http\Request;
use App\Models\Info;
use App\Models\InfoDetail;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;

class InfoController extends BaseController
{
    public function index(){
        $setting = Setting::where("module", "info")->first();
        if(empty($setting)) abort(404);

        $info = DB::table('info_detail')
            ->selectRaw("info_detail.id, info_detail.title, info.title as info_title,
                info_detail.short_desc, info_detail.content,
                info_detail.keywords, info_detail.is_showed,
                info_detail.date_show, info_detail.link,
                info_detail.order, info_detail.img,
                info.link as infoLink, info_detail.id_info,
                info_detail.caption")
            ->join('info', 'info.id', '=', 'info_detail.id_info')
            ->where('info.is_showed', 1)
            ->where('info_detail.is_showed', 1)
            ->orderBy('info_detail.order', 'asc')
            ->orderBy('info_detail.id', 'desc')
            ->paginate(10)
            ->withPath("/thong-tin");

        return view("user/info/index")
            ->with('setting', $setting)
            ->with('info', $info);
    }

    public function list($id = "", $link = "") {
        if(!$link) {
            abort(404);
        }

        $info = Info::where('is_showed', 1)
            ->where('id', $id)
            ->where('link', $link)->first();

        if(empty($info)) abort(404);

        $infoDetail = DB::table('info_detail')
            ->selectRaw("info_detail.id, info_detail.title, info.title as info_title,
                info_detail.short_desc, info_detail.content,
                info_detail.keywords, info_detail.is_showed,
                info_detail.date_show, info_detail.link,
                info_detail.order, info_detail.img,
                info.link as infoLink, info_detail.id_info,
                info_detail.caption")
            ->join('info', 'info.id', '=', 'info_detail.id_info')
            ->where('info.id', $id)
            ->where('info.is_showed', 1)
            ->where('info_detail.is_showed', 1)
            ->orderBy('info_detail.order', 'asc')
            ->orderBy('info_detail.id', 'desc')
            ->paginate(10)
            ->withPath("/thong-tin");

        return view("user/info/list")
            ->with('info', $info)
            ->with('infoDetail', $infoDetail);
    }

     public function detail($id = "", $link = "") {
        if(!$id && !$link) {
            abort(404);
        }

        $info = Info::where('is_showed', 1)->where('id', $id)->first();

        if(empty($info)) abort(404);

        $infoDetail = InfoDetail::where('is_showed', 1)
            ->where('id_info', $id)
            ->where('link', $link)
            ->first();

        if(empty($infoDetail)) abort(404);

        $relationInfo =  InfoDetail::where('is_showed', 1)
            ->where('id_info', $id)
            ->where('link', "!=", $link)
            ->limit(5)
            ->get();

        return view("user/info/detail")
            ->with('info', $info)
            ->with('infoDetail', $infoDetail)
            ->with('relationInfo', $relationInfo);
    }
}
