<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use App\Models\Partner;
use Image;

class PartnerController extends BaseController
{
    public function index(Request $request){
        $params  = $request->all();
        $isAjax  = (isset($params['is_ajax'])) ? $params['is_ajax'] : false;
        $start   = (!empty($params['start'])) ? $params['start'] : 0;
        $keyword = (!empty($params['keyword'])) ? $params['keyword'] : '';
        $limit   =  (!empty($params['limit'])) ? $params['limit'] : 50;
        $partner    = Partner::where('title', 'like', "%{$keyword}%")
            ->orderBy('order', 'asc')
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->offset($start)
            ->get();
        if($isAjax) {
            return response()->json(
                 [
                    "recordsFiltered" => count($partner),
                    "recordsTotal" => count($partner),
                    "data" => $partner
                 ], 200
            );
        }
        return view('admin/partner/index')
            ->with('title', "Danh sách đối tác");
    }

    public function add(Request $request){
        $params = $request->all();
        $title = "Thêm đối tác";
        $status = false;
        if(!empty($params)) {
            $params['date_show']  = (!empty($params['date_show'])) ? $params['date_show'] : date("Y-m-d");
            $params['title']      = trim($params['title']);
            $params['created_at'] = date("Y-m-d");
            $params['order']      = !empty($params['order']) ? (int) $params['order'] : 0;
            $params['is_show_navi'] = !empty($params['is_show_navi']) ? (int) $params['is_show_navi'] : 0;
            unset($params['_token']);
            if($request->hasFile('img')) {
                $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/partner/";
                $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                    $params['img'] = $fileName;

                    $imgThumb = Image::make($descriptionPath.$fileName);
                    $imgThumb->resize(375, 250, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($descriptionPath.$fileName);
                }
            }
            $status = Partner::insert($params);
        }
        return view('admin/partner/add')
            ->with('title', $title)
            ->with('status', $status);
    }

     public function edit(Request $request, $id = ""){
        $title = 'Sửa đối tác';
        $partner = Partner::find($id);
        $status = false;
        if(!$id || empty($partner)) {
           return redirect('/admin/partner');
        }
        $params = $request->all();
        if(!empty($params)) {
            $partner->title       = trim($params['title']);
            $partner->is_showed   = $params['is_showed'];
            $partner->link        = $params['link'];
            $partner->order       = !empty($params['order']) ? (int) $params['order'] : 0;
            $partner->is_show_navi = !empty($params['is_show_navi']) ? (int) $params['is_show_navi'] : 0;
            $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/partner/";
            if($request->hasFile('img')) {
                $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                    if(!empty($partner->img)) {
                        if(file_exists($descriptionPath.$partner->img)) {
                            unlink($descriptionPath.$partner->img);
                        }
                    }
                    $imgThumb = Image::make($descriptionPath.$fileName);
                    $imgThumb->resize(375, 250, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($descriptionPath.$fileName);
                    $partner->img = $fileName;
                }
            } else {
                if( $params['remove_img'] && file_exists($descriptionPath.$partner->img)) {
                    unlink($descriptionPath.$partner->img);
                    if(file_exists($descriptionPath.'thumb/'.$partner->img))
                    {
                        unlink($descriptionPath.'thumb/'.$partner->img);
                    }
                    $partner->img = "";
                }
            }
            $status = $partner->save();
        }
        return view('admin/partner/edit')
            ->with('title', $title)
            ->with('partner', $partner)
            ->with('status', $status);
    }

    public function del($id = ""){
         if($id){
            Partner::where('id', $id)->delete();
            $descriptionPath    = $_SERVER['DOCUMENT_ROOT']."/public/images/partner/";
            if(!empty($partner->img) && file_exists($descriptionPath.$partner->img)) {
                unlink($descriptionPath.$partner->img);
            }
        }
        return redirect("/admin/partner");
    }

}
