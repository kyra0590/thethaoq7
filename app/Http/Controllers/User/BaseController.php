<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\SliderTop;
use App\Models\Slider;
use App\Models\Intro;
use App\Models\Sport;
use App\Models\Info;
use App\Models\Union;
use App\Models\Notify;
use App\Models\GalleryImage;
use App\Models\GalleryVideo;
use App\Models\Relation;
use App\Models\Partner;
use App\Models\Admissions;
use View;

class BaseController extends Controller
{
	public function __construct() {

		$imgPath = $_SERVER['DOCUMENT_ROOT']."/public/images/";

		$sliderTop = SliderTop::where('is_showed', 1)
			->orderBy('order', "asc")
			->orderBy('id', 'desc')
			->limit(5)
			->get();

		$slider = Slider::where('is_showed', 1)
			->orderBy('order', "asc")
			->orderBy('id', 'desc')
			->limit(5)
			->get();

		$intros = Intro::where('is_showed', 1)
			->where('is_show_navi', 1)
			->orderBy('order', "asc")
			->orderBy('id', 'desc')
			->limit(5)
			->get();

		$sports = Sport::where('is_showed', 1)
			->orderBy('order', "asc")
			->orderBy('id', 'desc')
			->get();

		$infos = Info::where('is_showed', 1)
			->where('is_show_navi', 1)
			->orderBy('order', "asc")
			->orderBy('id', 'desc')
			->limit(5)
			->get();

		$unions = Union::where('is_showed', 1)
			->where('is_show_navi', 1)
			->orderBy('order', "asc")
			->orderBy('id', 'desc')
			->limit(5)
			->get();

		$admissionss = Admissions::where('is_showed', 1)
			->where('is_show_navi', 1)
			->orderBy('order', "asc")
			->orderBy('id', 'desc')
			->limit(5)
			->get();

		$intros = Intro::where('is_showed', 1)
			->where('is_show_navi', 1)
			->orderBy('order', "asc")
			->orderBy('id', 'desc')
			->limit(5)
			->get();

		$notifys = Notify::where('is_showed', 1)
			->orderBy('order', "asc")
			->orderBy('id', 'desc')
			->limit(5)
			->get();

		$galleryImages = GalleryImage::where('is_showed', 1)
			->where('is_show_navi', 1)
			->orderBy('order', "asc")
			->orderBy('id', 'desc')
			->limit(5)
			->get();

		$galleryVideos = GalleryVideo::where('is_showed', 1)
			->where('is_show_navi', 1)
			->orderBy('order', "asc")
			->orderBy('id', 'desc')
			->limit(1)
			->first();

		$relations = Relation::where('is_showed', 1)
			->orderBy('order', "asc")
			->orderBy('id', 'desc')
			->get();

		$partners = Partner::where('is_showed', 1)
			->orderBy('order', "asc")
			->orderBy('id', 'desc')
			->get();

		View::share('sports', $sports);
		View::share('intros', $intros);
		View::share('sliderTop', $sliderTop);
		View::share('slider', $slider);
		View::share('infos', $infos);
		View::share('unions', $unions);
		View::share('admissionss', $admissionss);
		View::share('notifys', $notifys);
		View::share('galleryImages', $galleryImages);
		View::share('galleryVideos', $galleryVideos);
		View::share('relations', $relations);
		View::share('partners', $partners);
		View::share('imgPath', $imgPath);
	}
}
