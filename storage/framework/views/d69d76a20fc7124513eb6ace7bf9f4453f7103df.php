<div class="sidebar show_pc">
     <div class="widget features-slide-widget">
        <div class="title-section">
            <h1><span>Tìm kiếm</span></h1>
        </div>
         <div class="slider_sidebar blur">
            <form id="search_news" class="navbar-form navbar-left" role="search" method="get" action="/tim-kiem">
                <input type="text" name="keyword" placeholder="Tìm kiếm tin">
                <button type="submit" id="search-submit"><i class="fa fa-search"></i></button>
            </form>
        </div>
    </div>

    <?php if(!empty($notifys) && count($notifys)): ?>
   <div class="widget post-widget">
        <div class="title-section">
            <h1><span>Thông báo mới</span></h1>
        </div>
        <div class="slider_sidebar blur">

            <marquee behavior="scroll" direction="up" style="height: 300px" onmouseover="this.stop();" onmouseout="this.start();">
                <ul class="slide_side list-posts">
                <?php $__currentLoopData = $notifys; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                   <li>
                    <?php if(!empty($item->img) && file_exists($imgPath."/notify/thumb/".$item->img)): ?>
                    <img src="/public/images/notify/thumb/<?php echo e($item->img); ?>" alt="<?php echo e($item->title); ?>">
                    <?php else: ?>
                    <img src="/public/images/logo_thumb.jpg" alt="<?php echo e($item->title); ?>">
                    <?php endif; ?>
                    <div class="post-content">
                        <h2 style="margin-top:20px"><a href="/thong-bao/<?php echo e($item->link); ?>.html"><?php echo e($item->title); ?></a></h2>
                    </div>
                  </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </marquee>
        </div>
    </div>
    <?php endif; ?>

    <?php if(!empty($galleryImages) && count($galleryImages)): ?>
    <div class="widget features-slide-widget">
        <div class="title-section">
            <h1><span>THƯ VIỆN ẢNH</span></h1>
        </div>
        <div class="image-post-slider">
            <ul class="bxslider">
                <?php $__currentLoopData = $galleryImages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if(!empty($item->img) && file_exists($imgPath."/gallery/image/".$item->img)): ?>
                <li>
                    <div class="news-post image-post2">
                        <img src="/public/images/gallery/image/<?php echo e($item->img); ?>" alt="<?php echo e($item->title); ?>">
                    </div>
                </li>
                <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    </div>
    <?php endif; ?>

    <?php if(!empty($galleryVideos)): ?>
    <div class="widget features-slide-widget">
        <div class="title-section">
            <h1><span>Video</span></h1>
        </div>
        <div>
            <?php if($galleryVideos->type_video == 1 && !empty($galleryVideos->link_video)): ?>
                <iframe width="100%" src="<?php echo e($galleryVideos->link_video); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <?php else: ?>
                <?php if(!empty($galleryVideos->video) && file_exists($imgPath."/gallery/video/".$galleryVideos->video)): ?>
                <video border='0' width="100%" controls>
                  <source src="/public/images/gallery/video/<?php echo e($galleryVideos->video); ?>" type="video/mp4">
                </video>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>
    <?php endif; ?>

    <?php if(!empty($relations) && count($relations)): ?>
    <div class="widget features-slide-widget">
        <div class="title-section">
            <h1><span>Liên kết Website</span></h1>
        </div>

        <div>
            <select class="form-control" onchange="window.open(this.value,'_Blank');" style="width:100%;height:30px;">
            <option value="">-- Lựa chọn --</option>
                <?php $__currentLoopData = $relations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($item->link); ?>"><?php echo e($item->title); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
        </div>
    </div>
    <?php endif; ?>

    <?php if(!empty($partners) && count($partners)): ?>
   <div class="widget post-widget">
        <div class="title-section">
            <h1><span>ĐỐI TÁC, CÁC NHÀ TÀI TRỢ</span></h1>
        </div>
         <marquee behavior="scroll" direction="up" style="height: 300px" onmouseover="this.stop();" onmouseout="this.start();">
            <?php $__currentLoopData = $partners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <p class="text-center">
                <a href="<?php echo e($item->link); ?>" target="_blank">
                    <?php if(!empty($item->img) && file_exists($imgPath."/notify/thumb/".$item->img)): ?>
                    <img src="/public/images/partner/<?php echo e($item->img); ?>" alt="<?php echo e($item->title); ?>" width="150px">
                    <?php else: ?>
                    <img src="/public/images/logo_thumb.jpg" alt="<?php echo e($item->title); ?>" width="150px">
                    <?php endif; ?>
                </a>
            </p>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </marquee>
    </div>
    <?php endif; ?>
    <div class="widget post-widget">
        <div class="title-section">
            <h1><span>THỐNG KÊ TRUY CẬP</span></h1>
        </div>
        <div style="text-align: center;">
            <!-- Histats.com  (div with counter) --><div id="histats_counter" style="text-align: center;"></div>
            <!-- Histats.com  START  (aync)-->
            <script type="text/javascript">var _Hasync= _Hasync|| [];
            _Hasync.push(['Histats.start', '1,4411933,4,403,118,80,00011111']);
            _Hasync.push(['Histats.fasi', '1']);
            _Hasync.push(['Histats.track_hits', '']);
            (function() {
            var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
            hs.src = ('//s10.histats.com/js15_as.js');
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
            })();</script>
            <noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?4411933&101" alt="" border="0" width="100%"></a></noscript>
            <!-- Histats.com  END  -->
        </div>
    </div>
</div>