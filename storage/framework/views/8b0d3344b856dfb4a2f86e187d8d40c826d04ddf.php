<header class="clearfix fifth-style">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="logo-advertisement">
            <div class="container">
                <?php if(!empty($sliderTop) && count($sliderTop)): ?>
                <div id="slider_top" class="show_pc">
                    <div id="slider_nivo" class="nivoSlider">
                        <?php $__currentLoopData = $sliderTop; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(!empty($item->img) && file_exists($imgPath."/slider-top/".$item->img)): ?>
                            <img src="/public/images/slider-top/<?php echo e($item->img); ?>" alt="">
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                      <div class="bg_slider_top">
                        <img src="/public/images/bg_slider_top.png" alt="">
                    </div>
                </div>
                <?php endif; ?>
                <div class="navbar-header show_sp">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="/public/images/logo_thumb.jpg" height="100px" alt="logo"></a>
                </div>
            </div>
        </div>

        <div class="nav-list-container">
            <div class="container">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left">
                        <li><a class="home" href="/">Trang chủ</a>
                        </li>
                        <li class="drop"><a class="tech" href="/gioi-thieu">Giới thiệu</a>
                            <?php if(!empty($intros) && count($intros)): ?>
                            <ul class="dropdown">
                                <?php $__currentLoopData = $intros; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><a href="/gioi-thieu/<?php echo e($item->id); ?>/<?php echo e($item->link); ?>.html"><?php echo e($item->title); ?></a></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                            <?php endif; ?>
                        </li>
                        <li><a class="travel" href="/tin-tuc">Tin tức - Sự kiện</a></li>
                        <li><a class="sport" href="/bo-mon">Các Bộ môn</a>
                            <?php if(!empty($sports) && count($sports)): ?>
                            <div id="slider_sports" class="megadropdown">
                                <div class="container">
                                    <div class="inner-megadropdown tech-dropdown">
                                        <div class="owl-wrapper">
                                            <h1>Danh sách các bộ môn</h1>
                                            <div class="owl-carousel" data-num="8">
                                                <?php $__currentLoopData = $sports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="item news-post standard-post">
                                                    <div class="post-gallery">
                                                        <?php if(!empty($item->icon) && file_exists($imgPath."/sport/".$item->icon)): ?>
                                                        <img src="/public/images/sport/<?php echo e($item->icon); ?>" alt="<?php echo e($item->title); ?>">
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="post-content">
                                                        <h2><a href="/bo-mon/<?php echo e($item->id); ?>/<?php echo e($item->link); ?>.html"><?php echo e($item->title); ?></a></h2>
                                                    </div>
                                                </div>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                        </li>
                        <li class="drop"><a class="fashion" href="#">Thư viện</a>
                            <ul class="dropdown">
                                <li><a href="/thu-vien-video">Video clip</a></li>
                                <li><a href="/thu-vien-anh">Hình ảnh</a></li>
                            </ul>
                        </li>
                        <li class="drop"><a class="video" href="/thong-tin">Thông tin</a>
                            <?php if(!empty($infos) && count($infos)): ?>
                            <ul class="dropdown">
                                <?php $__currentLoopData = $infos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><a href="/thong-tin/<?php echo e($item->id); ?>/<?php echo e($item->link); ?>.html"><?php echo e($item->title); ?></a></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                            <?php endif; ?>
                        </li>
                        <li class="drop"><a class="food" href="/dang-doan-the">Đảng - đoàn thể</a>
                            <?php if(!empty($unions) && count($unions)): ?>
                            <ul class="dropdown">
                                <?php $__currentLoopData = $unions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><a href="/dang-doan-the/<?php echo e($item->id); ?>/<?php echo e($item->link); ?>.html"><?php echo e($item->title); ?></a></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                            <?php endif; ?>
                        </li>
                         <li class="drop"><a class="travel" href="/chieu-sinh">Chiêu sinh</a>
                            <?php if(!empty($admissionss) && count($admissionss)): ?>
                            <ul class="dropdown">
                                <?php $__currentLoopData = $admissionss; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><a href="/chieu-sinh/<?php echo e($item->id); ?>/<?php echo e($item->link); ?>.html"><?php echo e($item->title); ?></a></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                            <?php endif; ?>
                        </li>
                        <li>
                            <a class="features" href="/lien-he">Liên hệ</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</header>

<?php if(!empty($slider) && count($slider)): ?>
    <section class="big-slider">
        <div class="container">
            <div class="image-slider">
                <ul class="big-bxslider">
                    <?php $__currentLoopData = $slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if(!empty($item->img) && file_exists($imgPath."/slider/".$item->img)): ?>
                            <li>
                                <div class="news-post image-post2">
                                    <div class="post-gallery">
                                        <?php if($item->link != ""): ?>
                                        <a href="<?php echo e($item->link); ?>" target="<?php echo e($item->target ? '_blank' : '_self'); ?>">
                                            <img src="/public/images/slider/<?php echo e($item->img); ?>" alt="<?php echo e($item->title); ?>">
                                        </a>
                                        <?php else: ?>
                                            <img src="/public/images/slider/<?php echo e($item->img); ?>" alt="<?php echo e($item->title); ?>">
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </li>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
        </div>
    </section>
<?php endif; ?>