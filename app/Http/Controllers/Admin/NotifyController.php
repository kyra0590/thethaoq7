<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use App\Models\Notify;
use Image;

class NotifyController extends BaseController
{
    public function index(Request $request){
        $params  = $request->all();
        $isAjax  = (isset($params['is_ajax'])) ? $params['is_ajax'] : false;
        $start   = (!empty($params['start'])) ? $params['start'] : 0;
        $keyword = (!empty($params['keyword'])) ? $params['keyword'] : '';
        $limit   =  (!empty($params['limit'])) ? $params['limit'] : 50;
        $notify    = Notify::where('title', 'like', "%{$keyword}%")
            ->orderBy('order', 'asc')
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->offset($start)
            ->get();
        if($isAjax) {
            return response()->json(
                 [
                    "recordsFiltered" => count($notify),
                    "recordsTotal" => count($notify),
                    "data" => $notify
                 ], 200
            );
        }
        return view('admin/notify/index')
            ->with('title', "Danh sách thông báo");
    }

    public function add(Request $request){
        $params = $request->all();
        $title = "Thêm thông báo";
        $status = false;
        if(!empty($params)) {
            $params['date_show'] = (!empty($params['date_show'])) ? $params['date_show'] : date("Y-m-d");
            $params['title'] = trim($params['title']);
            $params['created_at'] = date("Y-m-d");
            $params['order'] = !empty($params['order']) ? (int) $params['order'] : 0;
            unset($params['_token']);
            if($request->hasFile('img')) {
                $descriptionPath = $_SERVER['DOCUMENT_ROOT']."/public/images/notify/";
                $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                    $params['img'] = $fileName;

                    $imgThumb = Image::make($descriptionPath.$fileName);
                    $imgThumb->resize(375, 250, function ($constraint) {
                    $constraint->aspectRatio();
                    })->save($descriptionPath.'/thumb/'.$fileName);
                }
            }
            $status = Notify::insert($params);
        }
        return view('admin/notify/add')
            ->with('title', $title)
            ->with('status', $status);
    }

     public function edit(Request $request, $id){
        $title = 'Sửa thông báo';
        $notify = Notify::find($id);
        $status = false;
        if(!$id || empty($notify)) {
           return redirect('/admin/notify');
        }
        $params = $request->all();
        if(!empty($params)) {
            $params['date_show']  = (!empty($params['date_show'])) ? $params['date_show'] : date("Y-m-d");
            $notify->title        = trim($params['title']);
            $notify->is_showed    = $params['is_showed'];
            $notify->link         = $params['link'];
            $notify->keywords     = $params['keywords'];
            $notify->description  = $params['description'];
            $notify->content      = $params['content'];
            $notify->short_desc   = $params['short_desc'];
            $notify->date_show    = $params['date_show'];
            $notify->caption      = trim($params['caption']);
            $notify->order        = !empty($params['order']) ? (int) $params['order'] : 0;
            $notify->is_show_navi = !empty($params['is_show_navi']) ? (int) $params['is_show_navi'] : 0;
            $descriptionPath      = $_SERVER['DOCUMENT_ROOT']."/public/images/notify/";
            if($request->hasFile('img')) {
                $fileName = uniqid().strtotime('now').".{$request->img->extension()}";
                if(move_uploaded_file($_FILES['img']['tmp_name'], $descriptionPath.$fileName)) {
                    if(!empty($notify->img)) {
                        if(file_exists($descriptionPath.$notify->img)) {
                            unlink($descriptionPath.$notify->img);
                        }

                        if(file_exists($descriptionPath.'thumb/'.$notify->img))
                        {
                            unlink($descriptionPath.'thumb/'.$notify->img);
                        }
                    }
                    $imgThumb = Image::make($descriptionPath.$fileName);
                    $imgThumb->resize(375, 250, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($descriptionPath.'thumb/'.$fileName);
                    $notify->img = $fileName;
                }
            } else {
                if( $params['remove_img'] && file_exists($descriptionPath.$notify->img)) {
                    unlink($descriptionPath.$notify->img);
                    if(file_exists($descriptionPath.'thumb/'.$notify->img))
                    {
                        unlink($descriptionPath.'thumb/'.$notify->img);
                    }
                    $notify->img = "";
                }
            }
            $status = $notify->save();
        }
        return view('admin/notify/edit')
            ->with('title', $title)
            ->with('notify', $notify)
            ->with('status', $status);
    }

    public function del($id){
         if($id){
            Notify::where('id', $id)->delete();
            $descriptionPath    = $_SERVER['DOCUMENT_ROOT']."/public/images/notify/";
            if(!empty($notify->img) && file_exists($descriptionPath.$notify->img)) {
                unlink($descriptionPath.$notify->img);
                if(file_exists($descriptionPath.'thumb/'.$notify->img))
                {
                    unlink($descriptionPath.'thumb/'.$notify->img);
                }
            }
        }
        return redirect("/admin/notify");
    }

}
