@extends('user.layout')

@section('title', $sport->title)
@section('description', $sport->description)
@section('keywords', $sport->keywords)

@section('content')
<div class="grid-box">
     <div class="single-post-box">
        <div class="title-post">
            <h1><span>{{$sport->title}}</span></h1>
        </div>
        <div class="post-content">
            {!! $sport->content  !!}
        </div>
    </div>
</div>
<div class="col-md-12" style="margin-top:20px;">
    <div class="title-section">
        <h4><span>Tin tức môn : {{$sport->title}}</span></h4>
    </div>
    @if(!empty($sportDetail) && count($sportDetail))
        @foreach($sportDetail as $item)
        <div class="news-post article-post">
            <div class="row">
                <div class="col-sm-3">
                    <div class="post-gallery">
                        @if (!empty($item->img) && file_exists($imgPath."/sport-detail/thumb/".$item->img))
                            <img src="/public/images/sport-detail/thumb/{{$item->img}}"  alt="{{$item->caption}}" width="100%">
                        @else 
                            <img src="/public/images/logo_thumb.jpg" style="width: auto !important; height: 150px">
                        @endif
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="post-content">
                        <h2><a href="/bo-mon/{{$item->id_sport}}-{{$item->link}}.html">{{$item->title}}</a></h2>
                        <p>{{$item->short_desc}}</p>
                        <a href="/bo-mon/{{$item->id_sport}}-{{$item->link}}.html" class="read-more-button"><i class="fa fa-arrow-circle-right"></i>Đọc thêm</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    @endif
</div>
 @if(!empty($sportDetail) && count($sportDetail))
<div class="pagination-box">
    <ul class="pagination-list">
        @if ($sportDetail->lastPage() == 1)
            <li><a class="active" href="#">1</a></li>
        @elseif ($sportDetail->lastPage() > 1)
            @if($sportDetail->currentPage() > 1)
                <li><a href="{{ $sportDetail->url($sportDetail->currentPage() - 1) }}">Back</a></li>
            @endif

             @for ($i = 1; $i <= $sportDetail->lastPage(); $i++)
                <li><a class="{{ ($sportDetail->currentPage() == $i) ? ' active' : '' }}" href="{{ $sportDetail->url($i) }}">{{$i}}</a></li>
            @endfor

            @if($sportDetail->currentPage() < $sportDetail->lastPage())
            <li><a href="{{ $sportDetail->url($sportDetail->currentPage()+1) }}">Next</a></li>
            @endif
        @endif
    </ul>

    <p>Page {{$sportDetail->currentPage()}} of {{$sportDetail->lastPage()}}</p>
</div>
@else
    <p>Hiện tại không tìm thấy dữ liệu</p>
@endif

@endsection