<!doctype html>
<html lang="en" class="no-js">

<head>
    <title>Thể Thao Quận 7</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php include('css.php') ?>
</head>

<body>
    <!-- Container -->
    <div id="container">
        <!-- Header
            ================================================== -->
        <?php include('header.php') ?>
        <!-- End Header -->
        <!-- block-wrapper-section
            ================================================== -->
        <section class="block-wrapper shadow-white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="block-content">
                            <!-- grid box -->
                            <div class="grid-box">
                                <div class="title-section">
                                    <h1><span>Chi tiết tin</span></h1>
                                </div>
                                <div class="single-post-box">
                                    <div class="title-post">
                                        <h1>SMM Sport: 'Bao giờ Thái Lan vượt qua Việt Nam để lấy lại vị thế số 1 Đông Nam Á'</h1>
                                    </div>
                                    <div class="post-gallery">
                                        <img src="https://baoquocte.vn/stores/news_dataimages/nguyennga/122019/13/11/3422_edcf-5-a-946688-b-0637-e-603-bc-93-dc-1-d-82-c-1576209787964.jpg" alt="">
                                        <span class="image-caption">HLV Park Hang-seo giúp bóng đá Việt Nam tiến bộ vượt bậc. (Nguồn: Dân trí)</span>
                                    </div>
                                    <div class="post-content">
                                        <blockquote>
                                            <p>TGVN. Chứng kiến thành công của bóng đá Việt Nam dưới thời HLV Park Hang-seo, tờ SMM Sport tỏ ra tiếc nuối thời kỳ hoàng kim cùng HLV Kiatisak.</p>
                                        </blockquote>
                                        <p>Bóng đá Việt Nam đang có bước thăng tiến mạnh mẽ dưới thời HLV Park Hang-seo. Chúng ta đã thâu tóm được những danh hiệu lớn ở khu vực Đông Nam Á như AFF Cup hay SEA Games.</p>
                                        <p>Chứng kiến thành công của bóng đá Việt Nam, tờ SMM Sport (Thái Lan) đã tỏ ra tiếc nuối thời kỳ hoàng kim cùng HLV Kiatisak. Ngày hôm nay, nhật báo Thái Lan đã có bài viết: “Park Hang-seo, người thay đổi toàn diện bóng đá Việt Nam”.</p>
                                        <p>Tác giả mở đầu bài viết: “Nếu quay trở lại 6 năm trước, các đối thủ trong khu vực Đông Nam Á. Kể từ năm 2013, chúng ta đã giành tất cả danh hiệu lớn ở khu vực.</p>
                                        <p>Dưới thời HLV Kiatisak, bóng đá Thái Lan đã mang tới niềm vui tràn ngập cho những người hâm mộ. Từ chỗ bị quay lưng, đội tuyển Thái Lan đã trở thành hiện tượng, tạo ra sự hâm mộ cuồng nhiệt.</p>
                                        <p>Trong kỷ nguyên của Zico Thái, vé được bán hết chỉ trong vòng 1 phút. Thậm chí, trang web bán vé sập liên tục. Nhưng than ôi, Zico đã không tiếp tục làm HLV trưởng bởi không có sự phối hợp tốt với Chủ tịch mới của Liên đoàn bóng đá Thái Lan là Somyot Poompunmuang.</p>
                                        <p>Thất bại 0-4 trước Nhật Bản ở vòng loại World Cup 2018 là trận đấu cuối cùng của HLV Kiatisak. Sự ra đi của ông ấy đã thực sự khiến cho đội tuyển Thái Lan thay đổi”.</p>
                                        <p>Tờ báo này cũng buồn khi Thái Lan ở thời điểm này đã tụt lại so với Việt Nam: “Sau này, đội tuyển Thái Lan đã có sự dẫn dắt của nhiều HLV như Milovan Rajevac Sirisak Yodyardthai và giờ là Akira Nishino nhưng chưa HLV nào có thể mang tới thành công cho bóng đá Thái Lan.</p>
                                        <p>Ngược lại, các đội bóng Việt Nam ngày xưa cứ gặp Thái Lan là thua. Ngay cả với HLV người Nhật Bản, Miura hay sau đó là Nguyễn Hữu Thắng, tình hình cũng chẳng thể khá khẩm hơn.</p>
                                    </div>
                                    <div class="post-gallery">
                                        <img src="https://baoquocte.vn/stores/news_dataimages/nguyennga/122019/13/11/3722_process-1576209852690.jpg" alt="">
                                        <span class="image-caption">HLV Kiatisak từng mang tới thời hoàng kim cho bóng đá Thái Lan. (Nguồn: Dân trí)</span>
                                    </div>
                                    <div class="post-content">
                                        <p>Tuy nhiên, lịch sử bóng đá Việt Nam đã bước sang trang mới kể từ khi HLV Park Hang-seo tới làm việc ở Việt Nam từ năm 2017. Ở thời điểm đó, HLV Park Hang-seo không phải là HLV nổi tiếng. Ông ấy từng là trợ lý cho Guus Hiddink ở World Cup 2002 và dẫn dắt Olympic Hàn Quốc ở ASIAD 2002.</p>
                                        <p>Sau đó, ông ấy không thành công ở cấp độ CLB. Có chăng, ông ấy chỉ gặt hái được chút ít thành công ở CLB Chunnam Dragon. Ban đầu, sự có mặt của ông ấy đã khiến cho những người hâm mộ Việt Nam nghi ngờ.</p>
                                        <p>Thế nhưng, HLV Park Hang-seo đã tạo nên cuộc cách mạng bóng đá ở Việt Nam. Ông ấy đã thay máu đội tuyển, khi loại bỏ nhiều tuyển thủ quốc gia ở thời điểm đó, để tin dùng lứa U23.</p>
                                        <p>Và ngay trong giải đấu chính thức đầu tiên, HLV Park Hang-seo đã giúp U23 Việt Nam tạo nên kỳ tích. Chỉ sau 3 tháng dẫn dắt, ông thầy người Hàn Quốc đã đưa U23 Việt Nam trở thành á quân giải U23 châu Á, vượt qua nhiều đối thủ mạnh như Australia, Iraq và Qatar. Họ chỉ chịu thua U23 Uzebekistan ở phút cuối cùng hiệp phụ thứ 2.</p>
                                        <p>Từ thành công vang dội, HLV Park Hang-seo đã chiếm trọn niềm tin từ những người hâm mộ. Không ai nghi ngờ tài năng của HLV người Hàn Quốc và ông ấy còn nhận được sự hỗ trợ tối đa từ Liên đoàn bóng đá Việt Nam.</p>
                                        <p>Và tất cả đã tạo nên Việt Nam mạnh mẽ. Họ đã vô địch các giải đấu lớn ở Đông Nam Á như AFF Cup, SEA Games hay giải đấu châu Á như ASIAD hay Asian Cup. Đó chưa kể tới đội tuyển Việt Nam đang đứng đầu bảng G ở vòng loại World Cup.</p>
                                        <p>Việt Nam giờ đây đã vượt qua Thái Lan để thống trị bóng đá Đông Nam Á và thực sự thách thức các đối thủ lớn ở châu Á. Không biết bao giờ, Thái Lan có thể vượt qua Việt Nam để lấy lại vị thế số 1 Đông Nam Á một lần nữa”.</p>
                                    </div>
                                    <!--  <div class="article-inpost">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="image-content">
                                                    <div class="image-place">
                                                        <img src="https://media1.nguoiduatin.vn/thumb_x600x600/media/ha-thi-linh/2019/12/10/u22-viet-nam-vo-dich-seagame-30-20.jpg" alt="">
                                                        <div class="hover-image">
                                                            <a class="zoom" href="https://media1.nguoiduatin.vn/thumb_x600x600/media/ha-thi-linh/2019/12/10/u22-viet-nam-vo-dich-seagame-30-20.jpg">
                                                                <i class="fa fa-arrows-alt"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <span class="image-caption">Cras eget sem nec dui volutpat ultrices.</span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="text-content">
                                                    <h2>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. </h2>
                                                    <p>Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscing risus a sem. Nullam quis massa sit amet nibh viverra malesuada. </p>
                                                    <p>Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu. Ut scelerisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. </p>
                                                    <p>Nunc iaculis mi in ante. Vivamus nibh feugiat est.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="col-md-12">
                                        <div class="title-section">
                                            <h1><span class="world">TIN LIÊN QUAN</span></h1>
                                        </div>
                                        <ul class="list-posts">
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» Bế mạc giải bóng đá HKPĐ khối THCS 6&7 năm 2019: quận 7 đăng quang ngôi vô địch</a></h2>
                                                </div>
                                            </li>
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» Gần 4.000 người tham gia cuộc đi bộ đồng hành vì người nghèo</a></h2>
                                                </div>
                                            </li>
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» ĐẠI HỘI ĐOÀN VIÊN CHI ĐOÀN TRUNG TÂM TDTT NHIỆM KỲ 2019 - 2022</a></h2>
                                                </div>
                                            </li>
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» Về Cao Lãnh - Đồng Tháp thăm mộ cụ Phó bảng Nguyễn Sinh Sắc</a></h2>
                                                </div>
                                            </li>
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» GIẢI KARATE HỘI KHỎE PHÙ ĐỔNG quận 7 NĂM HỌC 2019 - 2020</a></h2>
                                                </div>
                                            </li>
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» ĐỘI BÓNG U13 quận 7 vô địch Giải bóng đá thiếu niên U13 Yamaha Cup 2019</a></h2>
                                                </div>
                                            </li>
                                            <li style="border:none">
                                                <div class="post-content">
                                                    <h2><a href="chi-tiet-tin.php">»» quận 7 KHAI MẠC HỘI KHỎE PHÙ ĐỔNG NĂM HỌC 2019 – 2020</a></h2>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <!-- sidebar -->
                        <?php include('sidebar.php') ?>
                        <!-- End sidebar -->
                    </div>
                </div>
            </div>
        </section>
        <!-- End block-wrapper-section -->
        <!-- footer
            ================================================== -->
        <?php include('footer.php') ?>
        <!-- End footer -->
    </div>
    <!-- End Container -->
    <?php include('script.php') ?>
</body>

</html>