@extends('admin.layout')
@section('title', $title)
@section('content')

<div class="row page-titles">
    <div class="col-md-12 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <button type="button" class="btn btn-info d-none d-lg-block m-l-15" onclick="javascript:location.href='/admin/slider-top'"><i class="fa fa-undo"></i> Trở lại</button>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-success">
                <h4 class="m-b-0 text-white">{{$title}}</h4>
            </div>
            <div class="card-body">
                <form action="" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                        <div class="row p-t-20">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Hiển thị</label>
                                    <select class="form-control custom-select" name="is_showed">
                                        <option value="0">Ẩn</option>
                                        <option value="1">Hiện</option>
                                    </select>
                                </div>
                            </div>
                             <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Tiêu đề</label>
                                    <input type="text" id="lastName" class="form-control" placeholder="" name="title" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Image (1000x200)</label>
                                    <input type="file" id="input-file-now" name='img' class="dropify" data-allowed-file-extensions="jpeg jpg png gif"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Lưu</button>
                        <button type="button" class="btn btn-danger" onclick="javascript:location.href='/admin/slider-top'"> <i class="fa fa-ban"></i> Hủy bỏ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
<?php if($status) { ?>
 $.toast({
    heading: "Thông báo",
    text: "Tạo thành công",
    position: 'bottom-right',
    loaderBg:'#ff6849',
    icon: 'success',
    hideAfter: 3500,
    stack: 6
});
<?php } ?>
</script>
@endsection
