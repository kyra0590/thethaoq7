@extends('user.layout')

@section('title', $setting->title)
@section('description', $setting->description)
@section('keywords', $setting->keywords)

@section('content')
<div class="grid-box">
    <div class="title-section">
        <h1><span>Chiêu sinh</span></h1>
    </div>
    @if(!empty($admissions) && count($admissions))
        @foreach($admissions as $item)
        <div class="news-post article-post">
            <div class="row">
                <div class="col-sm-3">
                    <div class="post-gallery">
                        @if (!empty($item->img) && file_exists($imgPath."/admissions/thumb/".$item->img))
                            <img src="/public/images/admissions/thumb/{{$item->img}}"  alt="{{$item->caption}}" width="100%">
                        @else
                            <img src="/public/images/logo_thumb.jpg" style="width: auto !important; height: 150px">
                        @endif
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="post-content">
                        <h2><a href="/chieu-sinh/{{$item->id_admissions}}-{{$item->link}}.html">{{$item->title}}</a></h2>
                        <p>{{$item->short_desc}}</p>
                        <a href="/chieu-sinh/{{$item->id_admissions}}-{{$item->link}}.html" class="read-more-button"><i class="fa fa-arrow-circle-right"></i>Đọc thêm</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    @endif
</div>
@if(!empty($admissions) && count($admissions))
<div class="pagination-box">
    <ul class="pagination-list">
        @if ($admissions->lastPage() == 1)
            <li><a class="active" href="#">1</a></li>
        @elseif ($admissions->lastPage() > 1)
            @if($admissions->currentPage() > 1)
                <li><a href="{{ $admissions->url($admissions->currentPage() - 1) }}">Back</a></li>
            @endif

             @for ($i = 1; $i <= $admissions->lastPage(); $i++)
                <li><a class="{{ ($admissions->currentPage() == $i) ? ' active' : '' }}" href="{{ $admissions->url($i) }}">{{$i}}</a></li>
            @endfor

            @if($admissions->currentPage() < $admissions->lastPage())
            <li><a href="{{ $admissions->url($admissions->currentPage()+1) }}">Next</a></li>
            @endif
        @endif
    </ul>

    <p>Page {{$admissions->currentPage()}} of {{$admissions->lastPage()}}</p>
</div>
@else
    <p>Hiện tại không tìm thấy dữ liệu</p>
@endif

@endsection